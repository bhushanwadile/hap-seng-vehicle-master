﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace HapSengVehicleMaster
{
    class _BE1SVM : UIDesign
    {
        #region Declaration
        private SAPbouiCOM.Form oForm;
        private SAPbouiCOM.Matrix oMatrix;
        public SAPbouiCOM.DataTable oDataTable;
        #endregion

        #region Events
        public void EventHandler(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_CLICK)
            {
                ItemClickEvent(ref pVal, ref BubbleEvent);
            }
            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_VALIDATE)
            {
                ItemValidateEvent(ref pVal, ref BubbleEvent);
            }
            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_RESIZE)
            {
                FormResizeEvent(ref pVal, ref BubbleEvent);
            }
            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
            {
                FormActivateEvent(ref pVal, ref BubbleEvent);
            }
            if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED)
            {
                LinkPressEvent(ref pVal, ref BubbleEvent);
            }
            //if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
            //{
            //    FormActivateEvent(ref pVal, ref BubbleEvent);
            //}
        }

        public void FormDataLoadEvent(ref SAPbouiCOM.BusinessObjectInfo pVal, ref bool BubbleEvent)
        {
            //try
            //{
            //    if (pVal.ActionSuccess == true)
            //    {
            //        //oForm = SboApp.Forms.GetForm("BE1SVM", -1);
            //        SboApp.ActivateMenuItem("1288");
            //    }
            //}
            //catch (Exception ex)
            //{
              
            //}
        }

        public void LinkPressEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            try
            {
                oForm = SboApp.Forms.GetForm("BE1SVM", -1);
                oMatrix = oForm.Items.Item("29").Specific;
                SAPbouiCOM.LinkedButton oLink;
                if (pVal.ItemUID == "29" && pVal.ColUID == "V_4" )
                {
                  
                    if (pVal.BeforeAction == true)
                    {
                        oForm.Freeze(true);
                        oLink = oMatrix.Columns.Item("V_4").ExtendedObject;
                        oForm.Items.Item("test").Specific.value = oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.Value;
                        oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.Value = oMatrix.Columns.Item("V_12").Cells.Item(pVal.Row).Specific.Value;
                       
                        if (oMatrix.Columns.Item("V_11").Cells.Item(pVal.Row).Specific.Value != "")
                        {
                            oLink.LinkedObjectType = oMatrix.Columns.Item("V_11").Cells.Item(pVal.Row).Specific.Value;

                        }
                       
                    }
                    else
                    {
                       
                        oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.Value = oForm.Items.Item("test").Specific.value;
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
                        oForm.Freeze(false);
                    }
                 
                }



            //    oForm = obe1.SboApp.Forms.GetForm("FrmBOM", -1)
            //oMatrix = oForm.Items.Item("0_U_G").Specific
            //If pVal.ItemUID = "0_U_G" And pVal.ColUID = "C_0_2" And pVal.BeforeAction = True Then
            //    oCombo = oMatrix.Columns.Item("C_0_1").Cells.Item(pVal.Row).Specific
            //    oLink = oMatrix.Columns.Item("C_0_2").ExtendedObject
            //    If oCombo.Value = "4" Then
            //        oLink.LinkedObjectType = "4"
            //    Else
            //        oLink.LinkedObjectType = "290"
            //    End If

            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Link Press Event : " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        private void FormActivateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            try
            {
                if (pVal.BeforeAction == true)
                {

                    oForm = SboApp.Forms.GetForm("BE1SVM", -1);

                    SboApp.Forms.Item(oForm.UniqueID).Select();
                    //oForm.Settings.Enabled = true;
                    //oForm.Refresh();
                }
            }
            catch (Exception)
            {}
        }


      

        public void FormResizeEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            try
            {
                if (pVal.BeforeAction == false)
                {
                    oForm = SboApp.Forms.GetForm("BE1SVM", -1);
                    //SAPbouiCOM.Item oItemMatrix = oForm.Items.Item("29");
                    SAPbouiCOM.Item oItemButton = oForm.Items.Item("1");
                    //oItemMatrix.Height = oItemButton.Top - 10;

                }
            }
            catch (Exception ex)
            {
                //SboApp.StatusBar.SetText("Form Resize Event : " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        public void MenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent)
        {
            try
            {
                if (pVal.MenuUID == "1304" && pVal.BeforeAction == false)
                {
                    try
                    {
                        oForm = SboApp.Forms.GetForm("BE1SVM", -1);
                        if (oForm.Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE)
                        {
                            FillMatrix(oForm);
                        }

                    }
                    catch (Exception ex)
                    {
                        SboApp.StatusBar.SetText("Form refresh event : " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                    }
                }

                // next
                if (pVal.MenuUID == "1288" && pVal.BeforeAction == false)
                {
                    
                    oForm = SboApp.Forms.GetForm("BE1SVM", -1);
                    oForm.Freeze(true);
                    SAPbouiCOM.EditText oEdit = oForm.Items.Item("18").Specific;
                    oForm.Items.Item("18").Enabled = true;
                    //string str = "select case when VMEntry + 1 > (select Max(VMentry) from B1SVMHEADER) then (select min(VMentry) from B1SVMHEADER) else VMEntry + 1 end from B1SVMHEADER WHERE VMEntry = '"+ oEdit.Value +"'";
                    string str = "select case when (select top 1 t0.vmentry from B1SVMHEADER t0 where t0.VMEntry > '" + oEdit.Value + "' order by VMEntry) > (select Max(VMentry) from B1SVMHEADER) then (select min(VMentry) from B1SVMHEADER) else (select top 1 t0.vmentry from B1SVMHEADER t0 where t0.VMEntry > '" + oEdit.Value + "' order by VMEntry) end from B1SVMHEADER WHERE VMEntry = '" + oEdit.Value + "'";
                    int result = 0;
                    int a   = GetScalar(str,ref result);
                    if (a != 0)
                    {
                        oEdit.Value = a.ToString();
                    }
                    else
                    {
                        str = "select min(VMentry) from B1SVMHEADER";
                        a =  GetScalar(str,ref result);
                        if (a != 0)
                        {
                            oEdit.Value = a.ToString();
                        }
                    }
                    oForm.Freeze(false);
                }
                // previous
                if (pVal.MenuUID == "1289" && pVal.BeforeAction == false)
                {
                    oForm = SboApp.Forms.GetForm("BE1SVM", -1);
                    oForm.Freeze(true);
                    SAPbouiCOM.EditText oEdit = oForm.Items.Item("18").Specific;
                    oForm.Items.Item("18").Enabled = true;
                    //string str = "select case when VMEntry - 1 < (select min(VMentry) from B1SVMHEADER) then (select Max(VMentry) from B1SVMHEADER)   else VMEntry - 1 end from B1SVMHEADER WHERE VMEntry ='" + oEdit.Value + "'";
                    string str = "select case when (select top 1 t0.vmentry from B1SVMHEADER t0 where t0.VMEntry < '" + oEdit.Value + "' order by VMEntry desc) < (select min(VMentry) from B1SVMHEADER) then (select Max(VMentry) from B1SVMHEADER)   else (select top 1 t0.vmentry from B1SVMHEADER t0 where t0.VMEntry < '" + oEdit.Value + "' order by VMEntry desc) end from B1SVMHEADER WHERE VMEntry ='" + oEdit.Value + "'";
                    int result = 0;
                    int a = GetScalar(str, ref result);
                    if (a != 0)
                    {
                        oEdit.Value = a.ToString();
                    }
                    else
                    {
                        str = "select max(VMentry) from B1SVMHEADER";
                        a = GetScalar(str, ref result);
                        if (a != 0)
                        {
                            oEdit.Value = a.ToString();
                        }
                    }
                    oForm.Freeze(false);
                }
                // first
                if (pVal.MenuUID == "1290" && pVal.BeforeAction == false)
                {
                    oForm = SboApp.Forms.GetForm("BE1SVM", -1);
                    oForm.Freeze(true);
                    SAPbouiCOM.EditText oEdit = oForm.Items.Item("18").Specific;
                    
                    string str = "(select min(VMentry) from B1SVMHEADER)";
                    int result = 0;
                    string currentval = oEdit.Value;
                    int docentry;
                    if (currentval == "")
                    { docentry = 0; }
                    else { docentry = Convert.ToInt32(currentval); }
                    int a = GetScalar(str, ref result);
                    if (a != 0 && a != docentry)
                    {
                        oForm.Items.Item("18").Enabled = true;
                        oEdit.Value = a.ToString();
                    }
                    else
                    {
                        str = "select min(VMentry) from B1SVMHEADER";
                        a = GetScalar(str, ref result);
                        if (a != 0)
                        {
                            oEdit.Value = a.ToString();
                        }
                    }
                    oForm.Freeze(false);
                }
                // last
                if (pVal.MenuUID == "1291" && pVal.BeforeAction == false)
                {
                    oForm = SboApp.Forms.GetForm("BE1SVM", -1);
                    oForm.Freeze(true);
                    SAPbouiCOM.EditText oEdit = oForm.Items.Item("18").Specific;
                    
                    string str = "(select max(VMentry) from B1SVMHEADER)";
                    int result = 0;
                    string currentval =  oEdit.Value;
                    int docentry;
                    if (currentval == "")
                    { docentry = 0; }
                    else { docentry = Convert.ToInt32(currentval); }
                    int a = GetScalar(str, ref result);
                    if (a != 0 && a != docentry)
                    {
                        oForm.Items.Item("18").Enabled = true;
                        oEdit.Value = a.ToString();
                    }
                    else
                    {
                        str = "select max(VMentry) from B1SVMHEADER";
                        a = GetScalar(str, ref result);
                        if (a != 0)
                        {
                            oEdit.Value = a.ToString();
                        }
                    }
                    oForm.Freeze(false);
                }

                if (pVal.MenuUID == "1281" && pVal.BeforeAction == false)
                {
                    oForm = SboApp.Forms.GetForm("BE1SVM", -1);
                    oForm.Freeze(true);
                    oForm.EnableMenu("1281", false);
                    oForm.EnableMenu("1304", false);
                    //oForm.EnableMenu("1288", false);
                    //oForm.EnableMenu("1289", false);
                    //oForm.EnableMenu("1290", false);
                    //oForm.EnableMenu("1291", false);
                    SAPbouiCOM.Matrix oMatrix = (SAPbouiCOM.Matrix)oForm.Items.Item("29").Specific;
                    if (oMatrix.RowCount == 0)
                    {
                        oForm.Items.Item("6").Specific.Value = "";
                        oForm.Items.Item("8").Specific.Value = "";
                        oForm.Items.Item("10").Specific.Value = "";
                        oForm.Items.Item("12").Specific.Value = "";
                        oForm.Items.Item("14").Specific.Value = "";
                        oForm.Items.Item("16").Specific.Value = "";
                        oForm.Items.Item("18").Specific.Value = "";
                        oForm.Items.Item("20").Specific.Value = "";
                        oForm.Items.Item("22").Specific.Value = "";
                        oForm.Items.Item("24").Specific.Value = "";
                        oForm.Items.Item("26").Specific.Value = "";
                        oForm.Items.Item("28").Specific.Value = "";
                        oForm.Items.Item("6").Enabled = true;
                        oForm.Items.Item("10").Enabled = true;
                        oForm.Items.Item("12").Enabled = true;
                        oForm.Items.Item("14").Enabled = true;
                        oForm.Items.Item("24").Enabled = true;
                        oForm.Items.Item("3").Enabled = false;
                        oForm.Items.Item("4").Enabled = false;
                        oForm.Items.Item("31").Enabled = false;
                        oForm.Items.Item("32").Enabled = false;
                        // new added fields for find
                        oForm.Items.Item("16").Enabled = true;
                        oForm.Items.Item("20").Enabled = true;
                        oForm.Items.Item("22").Enabled = true;

                        SAPbouiCOM.EditText oEdit = oForm.Items.Item("16").Specific;
                        oEdit.DataBind.SetBound(true, "", "pdate");
                        oEdit = oForm.Items.Item("20").Specific;
                        oEdit.DataBind.SetBound(true, "", "docdate");


                        oForm.Items.Item("6").Click();
                    }
                    oForm.Freeze(false);
                }
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Menu Event : " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        private void ItemValidateEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            try
            {
                oForm = SboApp.Forms.GetForm("BE1SVM", -1);


                if (pVal.ItemUID == "18" && pVal.BeforeAction == false)
                {

                   
                    if (pVal.ItemChanged == true)
                    {
                        FillMatrix(oForm);
                        //FillMatrix(oForm);
                        oForm.EnableMenu("1304", true);
                        
                    }

                   
                }
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Validate Event : " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        private void FillMatrix(SAPbouiCOM.Form oForm)
        {
            oForm.Freeze(true);
            SAPbouiCOM.EditText oEdit = (SAPbouiCOM.EditText)oForm.Items.Item("18").Specific;
            if (oEdit.Value != "")
            {
                //oForm.Items.Item("18").Enabled = false;
                SAPbouiCOM.Matrix oMatrix = (SAPbouiCOM.Matrix)oForm.Items.Item("29").Specific;
                string sql = "exec BE1S_SelectVehicleHeader " + oEdit.Value + "";
                oForm.DataSources.DataTables.Item(0).ExecuteQuery(sql);
                oDataTable = oForm.DataSources.DataTables.Item(0);
                //Header fields.

                oForm.Items.Item("test").Click();
                oForm.Items.Item("6").Enabled = false;
                oForm.Items.Item("10").Enabled = false;
                oForm.Items.Item("12").Enabled = false;
                oForm.Items.Item("14").Enabled = false;
                oForm.Items.Item("18").Enabled = false;
                oForm.Items.Item("24").Enabled = false;
                oForm.Items.Item("16").Enabled = false;
                oForm.Items.Item("20").Enabled = false;
                oForm.Items.Item("22").Enabled = false;
                oForm.Items.Item("18").Specific.Value = "";

                //SAPbouiCOM.EditText oEdit1 = oForm.Items.Item("16").Specific;
                //oEdit1.Value = oDataTable.Columns.Item("ProdDate").Cells.Item(0).Value;
                //oEdit1 = oForm.Items.Item("20").Specific;
                //oEdit1.Value = oDataTable.Columns.Item("DocDate").Cells.Item(0).Value;

                oForm.Items.Item("6").Specific.Value = oDataTable.Columns.Item("Model").Cells.Item(0).Value;
                oForm.Items.Item("8").Specific.Value = oDataTable.Columns.Item("Canceled").Cells.Item(0).Value;
                oForm.Items.Item("10").Specific.Value = oDataTable.Columns.Item("Descriptiion").Cells.Item(0).Value;
                oForm.Items.Item("12").Specific.Value = oDataTable.Columns.Item("CommNo").Cells.Item(0).Value;
                oForm.Items.Item("14").Specific.Value = oDataTable.Columns.Item("EngineNo").Cells.Item(0).Value;

                oForm.Items.Item("18").Specific.Value = oDataTable.Columns.Item("VMEntry").Cells.Item(0).Value;
                oForm.Items.Item("20").Specific.Value = oDataTable.Columns.Item("DocDate").Cells.Item(0).Value;
                oForm.Items.Item("22").Specific.Value = oDataTable.Columns.Item("MnfSerial").Cells.Item(0).Value;
                oForm.Items.Item("24").Specific.Value = oDataTable.Columns.Item("LotNumber").Cells.Item(0).Value;
                oForm.Items.Item("26").Specific.Value = oDataTable.Columns.Item("Type").Cells.Item(0).Value;
                oForm.Items.Item("28").Specific.Value = oDataTable.Columns.Item("Colour").Cells.Item(0).Value;
                oForm.Items.Item("16").Specific.Value = oDataTable.Columns.Item("ProdDate").Cells.Item(0).Value;



                var columns = oMatrix.Columns;

                //columns.Item("V_0").DataBind.SetBound(true, "@B1S_VM_LOCATION", "Code");



                columns.Item("V_0").DataBind.Bind("Dt", "Location");
                columns.Item("V_1").DataBind.Bind("Dt", "ActualWarehouse");
                columns.Item("V_2").DataBind.Bind("Dt", "CustomerName");
                columns.Item("V_3").DataBind.Bind("Dt", "UserName");
                columns.Item("V_4").DataBind.Bind("Dt", "DocumentNo");
                columns.Item("V_5").DataBind.Bind("Dt", "Documentstatus");
                columns.Item("V_6").DataBind.Bind("Dt", "PostingDate");
                columns.Item("V_7").DataBind.Bind("Dt", "DocumentDate");
                columns.Item("V_8").DataBind.Bind("Dt", "SystemTime");
                columns.Item("V_9").DataBind.Bind("Dt", "SystemDate");
                columns.Item("V_10").DataBind.Bind("Dt", "Step");
                columns.Item("V_11").DataBind.Bind("Dt", "objtype");
                columns.Item("V_12").DataBind.Bind("Dt", "DocEntry");
                columns.Item("V_11").Visible = false;
                columns.Item("V_12").Visible = false;
                //columns.Item("V_-1").DataBind.Bind("testdt", "RowNo");


                oMatrix.LoadFromDataSource();
                oMatrix.AutoResizeColumns();
                SAPbouiCOM.ComboBox oComboPriceList = oMatrix.Columns.Item("V_0").Cells.Item(1).Specific;
                FillCombo(oComboPriceList, "select Code, Name from [@B1S_VM_LOCATION]", "Code", "Name", false);

                //oForm.Items.Item("6").Click();
                //oForm.Items.Item("16").Enabled = true;
                //oForm.Items.Item("20").Enabled = true;

                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
                
                //oForm.Refresh();
                try
                {
                    oForm.EnableMenu("1282", false);
                    oForm.EnableMenu("1281", true);
                    oForm.EnableMenu("1289", true);
                    oForm.EnableMenu("1290", true);
                    oForm.EnableMenu("1291", true);
                    oForm.EnableMenu("1288", true);

                }
                catch (Exception)
                {
                }

                // check enable PDI start button or not

                string str = "exec BE1S_EnablePDIStart " + oEdit.Value + "";
                int stepref = 0;
                int step = GetScalar(str, ref stepref);
                if (step > 0)
                { oForm.Items.Item("3").Enabled = true; }
                else
                { oForm.Items.Item("3").Enabled = false; }

                // check enable PDI end button or not

                str = "exec BE1S_EnablePDIEnd " + oEdit.Value + "";
                stepref = 0;
                step = GetScalar(str, ref stepref);
                if (step > 0)
                { oForm.Items.Item("4").Enabled = true; }
                else
                { oForm.Items.Item("4").Enabled = false; }

                // check enable PRE start button or not

                 str = "exec BE1S_EnablePREStart " + oEdit.Value + "";
                 stepref = 0;
                 step = GetScalar(str, ref stepref);
                if (step > 0)
                { oForm.Items.Item("31").Enabled = true; }
                else
                { oForm.Items.Item("31").Enabled = false; }

                // check enable PRE end button or not

                str = "exec BE1S_EnablePREEnd " + oEdit.Value + "";
                stepref = 0;
                step = GetScalar(str, ref stepref);
                if (step > 0)
                { oForm.Items.Item("32").Enabled = true; }
                else
                { oForm.Items.Item("32").Enabled = false; }

                // this is added for the date issue on hap seng server.

                oForm.Items.Item("16").Enabled = true;
                oForm.Items.Item("20").Enabled = true;
                oForm.Items.Item("16").Specific.Value = oDataTable.Columns.Item("ProdDate").Cells.Item(0).Value;
                oForm.Items.Item("20").Specific.Value = oDataTable.Columns.Item("DocDate").Cells.Item(0).Value;
                oForm.Items.Item("test").Click();
                oForm.Items.Item("16").Enabled = false;
                oForm.Items.Item("20").Enabled = false;
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE;
                oForm.Freeze(false);
            }
        }

        private void ItemClickEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {

            try
            {
                int VMentry, user;
                string step, location, str;
                string model, desc, commno, engine, lotno, chassisno, proddate, createdate;
                oForm = SboApp.Forms.GetForm("BE1SVM", -1);

               

                if (pVal.ItemUID == "3" && pVal.BeforeAction == false)
                {
                    SAPbouiCOM.Item oItem = oForm.Items.Item("3");
                    if (oItem.Enabled == true)
                    {
                        SAPbouiCOM.EditText oEdit = oForm.Items.Item("18").Specific;
                        SAPbouiCOM.Matrix oMatrix = (SAPbouiCOM.Matrix)oForm.Items.Item("29").Specific;
                        VMentry = Convert.ToInt32(oEdit.Value);
                        user = GetUserSign(SboApp.Company.UserName);
                        str = "exec BE1S_GetStepToUpdatePDIStart " + VMentry + ", " + user + "";
                        ADONonExeSql(str);
                        FillMatrix(oForm);
                    }
                }

                if (pVal.ItemUID == "4" && pVal.BeforeAction == false)
                {
                       SAPbouiCOM.Item oItem = oForm.Items.Item("4");
                       if (oItem.Enabled == true)
                       {
                           SAPbouiCOM.EditText oEdit = oForm.Items.Item("18").Specific;
                           SAPbouiCOM.Matrix oMatrix = (SAPbouiCOM.Matrix)oForm.Items.Item("29").Specific;
                           VMentry = Convert.ToInt32(oEdit.Value);
                           user = GetUserSign(SboApp.Company.UserName);
                           str = "exec BE1S_GetStepToUpdatePDIStop " + VMentry + ", " + user + "";
                           ADONonExeSql(str);
                           FillMatrix(oForm);
                       }
                }

                if (pVal.ItemUID == "31" && pVal.BeforeAction == false)
                {
                       SAPbouiCOM.Item oItem = oForm.Items.Item("31");
                       if (oItem.Enabled == true)
                       {
                           SAPbouiCOM.EditText oEdit = oForm.Items.Item("18").Specific;
                           SAPbouiCOM.Matrix oMatrix = (SAPbouiCOM.Matrix)oForm.Items.Item("29").Specific;
                           VMentry = Convert.ToInt32(oEdit.Value);
                           user = GetUserSign(SboApp.Company.UserName);
                           str = "exec BE1S_GetStepToUpdatePREStart " + VMentry + ", " + user + "";
                           ADONonExeSql(str);
                           FillMatrix(oForm);
                       }
                }

                if (pVal.ItemUID == "32" && pVal.BeforeAction == false)
                {
                       SAPbouiCOM.Item oItem = oForm.Items.Item("32");
                       if (oItem.Enabled == true)
                       {
                           SAPbouiCOM.EditText oEdit = oForm.Items.Item("18").Specific;
                           SAPbouiCOM.Matrix oMatrix = (SAPbouiCOM.Matrix)oForm.Items.Item("29").Specific;
                           VMentry = Convert.ToInt32(oEdit.Value);
                           user = GetUserSign(SboApp.Company.UserName);
                           str = "exec BE1S_GetStepToUpdatePREStop " + VMentry + ", " + user + "";
                           ADONonExeSql(str);
                           FillMatrix(oForm);
                       }
                }

                if (pVal.ItemUID == "1" && pVal.ActionSuccess == true)
                {
                    //oForm.EnableMenu("1288", true);
                    //SboApp.ActivateMenuItem("1288");
                }

                if (pVal.ItemUID == "1" && pVal.BeforeAction == false)
                {

                    //string str1 = SboApp.Company.UserName;

                    if (oForm.Mode == SAPbouiCOM.BoFormMode.fm_FIND_MODE)
                    {
                        model = oForm.Items.Item("6").Specific.Value;
                        desc = oForm.Items.Item("10").Specific.Value;
                        commno = oForm.Items.Item("12").Specific.Value;
                        engine = oForm.Items.Item("14").Specific.Value;
                        lotno = oForm.Items.Item("24").Specific.Value;

                        // new fields added for find
                        proddate =   oForm.Items.Item("16").Specific.Value;
                        createdate = oForm.Items.Item("20").Specific.Value;
                        chassisno =  oForm.Items.Item("22").Specific.Value;

                        SAPbouiCOM.EditText oEdit = (SAPbouiCOM.EditText)oForm.Items.Item("18").Specific;
                        SAPbouiCOM.Item oItem = oForm.Items.Item("18");
                        oItem.Enabled = true;
                        Sql = "exec BE1S_VehicleMaster '" + model + "', '" + desc + "', '" + commno + "', '" + engine + "', '" + lotno + "', '" + chassisno + "', '" + createdate + "', '" + proddate + "'";

                        SAPbobsCOM.Recordset oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        oRs.DoQuery(Sql);

                        if (oRs.RecordCount == 1)
                        {
                            oEdit.Value = Convert.ToString(oRs.Fields.Item(0).Value);
                        }
                        else
                        {
                            _ShowDetails oshow = new _ShowDetails(oForm, oEdit, FormOpenType.WithGridEditBox, false, Sql, "Select Commision No. ", SAPbouiCOM.BoMatrixSelect.ms_Single);
                        }

                    }
                    if (oForm.Mode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE)
                    {
                        //int VMentry;
                        //string step, location, str;
                        SAPbouiCOM.EditText oEdit = oForm.Items.Item("18").Specific;
                        SAPbouiCOM.Matrix oMatrix = (SAPbouiCOM.Matrix)oForm.Items.Item("29").Specific;

                        VMentry = Convert.ToInt32(oEdit.Value);

                        for (int i = 1; i <= oMatrix.RowCount; i++)
                        {
                            step = oMatrix.Columns.Item("V_10").Cells.Item(i).Specific.Value;
                            location = oMatrix.Columns.Item("V_0").Cells.Item(i).Specific.Value;

                            str = "exec BE1S_UpdateLocation " + VMentry + ", " + step + ", '" + location + "'";

                            ADONonExeSql(str);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Item Click Event : " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }

        }
        #endregion

        #region Method
        public void ShowForm()
        {
            try
            {

                loadform("_BE1SVM.srf");
                CreateForm();

            }
            catch (Exception ex)
            {
                oMsgShow.Messsage("Show Form : " + ex.Message.ToString(), MsgShow.MsgType.Statusbar, MsgShow.MsgCat.Errors);
            }

        }
        public void CreateForm()
        {


            try
            {

                oForm = SboApp.Forms.GetForm("BE1SVM", -1);
                oForm.EnableMenu("1282", false);
                SAPbouiCOM.Item oItem;
                SAPbouiCOM.Grid oGrid;
                //oForm.Freeze(true);
                oForm.DataSources.DataTables.Add("Dt");
                oForm.DataSources.UserDataSources.Add("pdate", SAPbouiCOM.BoDataType.dt_DATE, 20);
                oForm.DataSources.UserDataSources.Add("docdate", SAPbouiCOM.BoDataType.dt_DATE, 20);
                oForm.DataSources.UserDataSources.Add("pdt", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20);
                oForm.DataSources.UserDataSources.Add("docdt", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 20);
                //SAPbouiCOM.EditText oEditprodDate = oForm.Items.Item("16").Specific;
                //oEditprodDate.DataBind.SetBound(true, "", "pdate");
                ////oEditprodDate.Value = "19000101";
                //SAPbouiCOM.EditText oEditDate = oForm.Items.Item("20").Specific;
                //oEditDate.DataBind.SetBound(true, "", "docdate"); 
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;

                oForm.Items.Item("18").Enabled = false;

                oForm.Items.Item("6").Enabled = true;
                oForm.Items.Item("10").Enabled = true;
                oForm.Items.Item("12").Enabled = true;
                oForm.Items.Item("14").Enabled = true;
                oForm.Items.Item("24").Enabled = true;

                // new added fields for find
                oForm.Items.Item("16").Enabled = true;
                oForm.Items.Item("20").Enabled = true;
                oForm.Items.Item("22").Enabled = true;

                SAPbouiCOM.EditText oEdit = oForm.Items.Item("16").Specific;
                oEdit.DataBind.SetBound(true, "", "pdate");
                oEdit = oForm.Items.Item("20").Specific;
                oEdit.DataBind.SetBound(true, "", "docdate");
                oForm.DefButton = "1";
                
                oForm.EnableMenu("1289", true);
                oForm.EnableMenu("1290", true);
                oForm.EnableMenu("1291", true);
                oForm.EnableMenu("1288", true);
                
            }

            catch (Exception ex)
            {
                oMsgShow.Messsage("Create Form : " + ex.Message.ToString(), MsgShow.MsgType.Statusbar, MsgShow.MsgCat.Errors);
            }
            finally
            {
                oForm.Freeze(false);

            }

        }
        #endregion
    }
}