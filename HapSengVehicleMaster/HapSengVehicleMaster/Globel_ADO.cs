﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Xml;
using System.Xml.XmlConfiguration;
using SAPbouiCOM;
using SAPbobsCOM;
using System.Data.Sql;
using System.Data.SqlClient;
namespace HapSengVehicleMaster
{
    public class Global_ADO
    {
        #region Global Veriable Declaration

        #region SBO Releted Veriable
        public static SAPbouiCOM.Application SboApp;
        public static SAPbouiCOM.EventFilters oFilters;
        public static SAPbouiCOM.EventFilter oFilter;
        public static SAPbobsCOM.Company oCompany;
        #endregion

        #region Class veriable
        public static be1_UIConnection obe1sUi;
        public static MsgShow oMsgShow;
        //public _169 oMainMenuForm;
        //public _672 oBomForm;
        public static Menu oMenu;
       
        #endregion

       // public SqlConnection connobj;

        public static string Sql;
        
        public static string AddOn_Name;
        public bool costbywhsflag;
        public static string ChildBaseForm;
        public static int ChildOpen;
        public static string AltSrcFG;
        public static string AltSrcitm, AltSrcwhs;
        public static string AltSrcItmNm;
        public static SAPbouiCOM.Form BOMform;
        public static string SepDec, MainCurr;
        public static int Pricedec;
      
        public static string[] FormArray;
        #endregion
        SqlConnection connobj;
        #region "Global Method"
        public long MaxCode(string TableName)
        {
            string strSQL = null;
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);

            //oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            strSQL = "SELECT TOP(1) CODE FROM [" + TableName + "] ORDER BY CAST(CODE AS BIGINT) DESC";

            //oRecordset.DoQuery(strSQL);
            //oRecordset.MoveFirst();
            oDt = GetDataTable(strSQL);

            if (oDt.Rows.Count == 0)
            {
                return 1;
            }
            else
            {
                return (long)oDt.Rows[0][0]; // oRecordset.Fields.Item("Code").Value;
            }
        }
        public long MaxLine(string TableName, string Code)
        {
            string strSQL = null;
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);

            //oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            strSQL = "SELECT TOP(1) LineId FROM [" + TableName + "] " + " WHERE Code = '" + Code + "'" + " ORDER BY LineId DESC";

            //oRecordset.DoQuery(strSQL);
            //oRecordset.MoveFirst();
            oDt = GetDataTable(strSQL);

            return (long)oDt.Rows[0][0];  //oRecordset.Fields.Item(0).Value;
        }
        public string MonthName(int Month)
        {
            switch (Month)
            {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
            }

            return "";
        }
        public int MonthDays(int Month)
        {
            switch (Month)
            {
                case 1:
                    return 31;
                case 2:
                    return 28;
                case 3:
                    return 31;
                case 4:
                    return 30;
                case 5:
                    return 31;
                case 6:
                    return 30;
                case 7:
                    return 31;
                case 8:
                    return 31;
                case 9:
                    return 30;
                case 10:
                    return 31;
                case 11:
                    return 30;
                case 12:
                    return 31;
            }

            return 0;
        }
        public long NextDocNum(string ObjectCode, int Series)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string Query = null;

            try
            {
//                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                Query = "SELECT NextNumber FROM NNM1 WHERE ObjectCode = '" + ObjectCode + "' AND Series = " + Series;
  //              oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);

                return (long)oDt.Rows[0][0]; //oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public long NextDocEntry(string ObjectCode)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string Query = null;

            try
            {
                //oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                Query = "SELECT AutoKey FROM ONNM WHERE ObjectCode = '" + ObjectCode + "'";
                //oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);
                return (long) oDt.Rows[0][0]; //oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public string GetEmpName(int EmpId)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string Query = null;

            try
            {
             //   oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                Query = "SELECT IsNull(LastName, '') + ', ' + IsNull(FirstName, '') + ' ' + IsNull(MiddleName, '') " + " FROM OHEM " + " WHERE EmpId = " + EmpId;

//                oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);

                return oDt.Rows[0][0].ToString (); //oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                SboApp.SetStatusBarMessage("GetEmpName: " + ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                return "";
            }
        }
        public string GetScalar(string Query, ref string myObject)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            try
            {
//                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

  //              oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);
                //if (oRecordset.RecordCount <= 0)
                if (oDt.Rows.Count == 0)

                {
                    myObject = "";
                    return "No Records";
                }
                else
                {
                    myObject = oDt.Rows[0][0].ToString (); //oRecordset.Fields.Item(0).Value;
                    return "OK";
                }
            }
            catch (Exception ex)
            {
                myObject = "";
                return ("Error, " + ex.Message);
            }
        }
        public string GetNonQuery(string Query)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            try
            {
         //       oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
           //     oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);
                if (oDt.Rows.Count > 0)
                {
                    return "Ok";
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                return "Error, " + ex.Message;
            }
        }
        public string GetItemRouting(string ItemCode)
        {
            string Query = null;
            string Routing = null;

            Routing = "N";
            Query = "SELECT IsNull(U_Routing, 'N') FROM OITM WHERE ItemCode = '" + ItemCode + "'";

            GetScalar(Query, ref  Routing);
            return Routing;
        }
        public string GetItemDefaultRouting(string ItemCode)
        {
            string Query = null;
            string Routing = null;

            Routing = "";
            Query = "SELECT IsNull(U_ProcessCode, '') FROM OITM WHERE ItemCode = '" + ItemCode + "'";

            GetScalar(Query, ref Routing);
            return Routing;
        }
        public string GetItemName(string ItemCode)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string Query = null;

            try
            {
//                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Query = "SELECT ItemName FROM OITM WHERE ItemCode = '" + ItemCode + "'";

  //              oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);

                return oDt.Rows[0][0].ToString(); //oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string GetItemDfltWh(string ItemCode)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string Query = null;

            try
            {
//                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Query = "SELECT DfltWh FROM OITM WHERE ItemCode = '" + ItemCode + "' and isnull(DfltWh,'')<>''";

  //              oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);

//                if (oRecordset.RecordCount > 0)
                if (oDt.Rows.Count > 0)
                {
                    return oDt.Rows[0][0].ToString ();//oRecordset.Fields.Item(0).Value;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string GetItmsGrpCod(string ItemCode)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string Query = null;

            try
            {
            //    oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                Query = "SELECT ItmsGrpCod FROM OITM WHERE ItemCode = '" + ItemCode + "'";

                //oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);


                return oDt.Rows[0][0].ToString () ;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string GetCardName(string CardCode)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string Query = null;

            try
            {
//                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Query = "SELECT CardName FROM OCRD WHERE CardCode = '" + CardCode + "'";

  //              oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);

                return oDt.Rows[0][0].ToString ();//oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public float GetAvailableQty(string ItemCode, string WhsCode)
        {
            string Query = null;
            string Quantity = "0";

            Query = "SELECT OnHand FROM OITW " + " WHERE ItemCode = '" + ItemCode + "'" + " AND WhsCode = '" + WhsCode + "'";

            if (GetScalar(Query, ref Quantity) == "OK")
            {
                return float.Parse(Quantity);
            }
            else
            {
                return 0;
            }
        }
        public float GetBatchQty(string ItemCode, string WhsCode, string BatchNum)
        {
            string Query = null;
            string Quantity = "0";

            Query = "SELECT T0.Quantity " + " FROM OBTQ T0 " + " INNER JOIN OBTN T1 ON T0.ItemCode = T1.ItemCode AND T0.SysNumber = T1.SysNumber " + " WHERE T0.ItemCode = '" + ItemCode + "'" + " AND T0.WhsCode = '" + WhsCode + "'" + " AND T1.DistNumber = '" + BatchNum + "'";

            if (GetScalar(Query, ref Quantity) == "OK")
            {
                return float.Parse(Quantity);
            }
            else
            {
                return 0;
            }
        }
        public float FirstLoadQty(string ItemCode)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string Query = null;

            try
            {
                //oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                
                Query = "SELECT U_FirstLoad FROM OITM WHERE ItemCode = '" + ItemCode + "'";
                //oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);

                //if (oRecordset.RecordCount <= 0)
                if (oDt.Rows.Count <= 0)

                {
                    return -1;
                }
                else
                {
                    return (float)oDt.Rows[0][0]; //oRecordset.Fields.Item(0).Value;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public string IsBatchManaged(string ItemCode)
        {
            string Query = null;
            string Result = "";

            Query = "SELECT ManBtchNum FROM OITM WHERE ItemCode = '" + ItemCode + "' and ManBtchNum='Y'";
            if (GetScalar(Query, ref  Result) == "OK")
            {
                return Result;
            }
            else
            {
                return "-1";
            }
        }
        public object IsSerialManaged(string ItemCode)
        {
            string Query = null;
            string Result = null;

            Result = "N";
            Query = "SELECT ManSerNum FROM OITM WHERE ItemCode = '" + ItemCode + "'";
            if (GetScalar(Query, ref Result) == "OK")
            {
                return Result;
            }
            else
            {
                return "N";
            }
        }
        public string IsExcisable(string ItemCode)
        {
            string Query = null;
            string Result = "";

            Query = "SELECT Excisable FROM OITM WHERE ItemCode = '" + ItemCode + "'";
            if (GetScalar(Query, ref Result) == "OK")
            {
                return Result;
            }
            else
            {
                return "-1";
            }
        }
        public object GetSeries(SAPbobsCOM.BoObjectTypes ObjType, string WhsPrefix, System.DateTime DocDate)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string Query = null;

            try
            {
//                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                if (WhsPrefix == "DTA")
                {
                    WhsPrefix = "DBS";
                }

                Query = "SELECT T0.Series " + " FROM NNM1 T0 " + " INNER JOIN OFPR T1 ON T0.Indicator = T1.Indicator " + " WHERE ObjectCode = '" + ObjType + "'" + " AND U_SegCode = '" + WhsPrefix + "'" + " AND '" + DocDate.ToString("MM-dd-yyyy") + "' >= T1.F_RefDate " + " AND '" + DocDate.ToString("MM-dd-yyyy") + "' <= T1.T_RefDate ";

  //              oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);
                return oDt.Rows[0][0].ToString (); //oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public object GetSeriesName(SAPbobsCOM.BoObjectTypes ObjType, string Series)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string Query = null;

            try
            {
//                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                Query = "SELECT SeriesName " + " FROM NNM1 " + " WHERE ObjectCode = '" + ObjType + "'" + " AND Series = '" + Series + "'";

  //              oRecordset.DoQuery(Query);.
                oDt = GetDataTable(Query);
                return oDt.Rows[0][0].ToString ();//oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public string GetAcctCode(string Segment_0, string Segment_1)
        {
            string Query = null;
            string Result = "";

            Query = "SELECT AcctCode " + " FROM OACT " + " WHERE Segment_0 = '" + Segment_0 + "'" + " AND Segment_1 = '" + Segment_1 + "'";
            if (GetScalar(Query, ref Result) == "OK")
            {
                return Result;
            }
            else
            {
                return "-1";
            }
        }
        public long LastBatchNum()
        {
            string Query = null;
            string Result = "";

            Query = "SELECT TOP 1 U_LastBatchNum " + " FROM [@PSET] ";

            if (GetScalar(Query, ref Result) == "OK")
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
        public void UpdateLastBatch(long BatchNum)
        {
            string Query = null;
            string Result = "";

            Query = "UPDATE [@PSET] " + " SET U_LastBatchNum = " + BatchNum;

            if (GetNonQuery(Query) == "Ok")
            {
                return;
            }
        }
        public void ShowCrystalReport(string ReportId, string[] Parameters)
        {
            string Query = null;
            string MenuId = "";

            SAPbouiCOM.Form oForm = default(SAPbouiCOM.Form);

            try
            {
                Query = "SELECT MenuUid " + " FROM OCMN " + " WHERE ObjectKey = '" + ReportId + "'" + " AND ObjectType = '232'";

                if (GetScalar(Query, ref MenuId) != "OK")
                {
                    throw new Exception("Menu Id not found for report.");
                }

                SboApp.ActivateMenuItem(MenuId);
                oForm = SboApp.Forms.ActiveForm;

                int PFlag = 0;
                for (int flag = 0; flag <= oForm.Items.Count - 1; flag++)
                {
                    if (oForm.Items.Item(flag).Type == BoFormItemTypes.it_EDIT & oForm.Items.Item(flag).Visible == true)
                    {
                        oForm.Items.Item(flag).Specific.Value = Parameters[PFlag];
                        PFlag += 1;
                    }
                }

                oForm.Items.Item(0).Click(BoCellClickType.ct_Regular);
            }
            catch (Exception ex)
            {
                SboApp.SetStatusBarMessage("ShCry: " + ex.Message, BoMessageTime.bmt_Short, true);
            }
        }
        public float GetPackFactor(string PackingCode)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string Query = null;

            try
            {
//                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                Query = "SELECT IsNull(U_Factor, 1) " + " FROM OITT " + " WHERE Code = '" + PackingCode + "'";

  //              oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);
                return (float)oDt.Rows[0][0];//oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return 1;
            }
        }
        public float GetProcessTime(SAPbobsCOM.BoObjectTypes DocType, long DocEntry, int LineNum, string TimeFormula)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string TimeQuery = "";

            try
            {
//                oRecordset = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                if (DocType == BoObjectTypes.oQuotations)
                {
                    TimeQuery = "SELECT " + TimeFormula + " FROM QUT1 WHERE DocEntry = " + DocEntry + " AND LineNum = " + LineNum;
                }
                else if (DocType == BoObjectTypes.oOrders)
                {
                    TimeQuery = "SELECT " + TimeFormula + " FROM RDR1 WHERE DocEntry = " + DocEntry + " AND LineNum = " + LineNum;
                }

                //              oRecordset.DoQuery(TimeQuery);
                oDt = GetDataTable(TimeQuery);
                return (float)oDt.Rows[0][0];//(oRecordset.Fields.Item(0).Value);
            }
            catch (Exception Ex)
            {
                return 0;
            }
        }
        public string GetUserCode(int UserSign)
        {
            string Query = null;
            string Name = null;
            string Result = null;

            Query = "SELECT User_Code " + " FROM OUSR " + " WHERE Internal_K = " + UserSign;
            Result = GetScalar(Query, ref Name);
            if (Result == "OK")
            {
                return Name;
            }
            else
            {
                return "";
            }
        }
        public int GetUserSign(string UserCode)
        {
            string Query = null;
            string Result = null;
            string Name = "0";

            Query = "SELECT UserID " + " FROM OUSR " + " WHERE User_Code = '" + UserCode + "'";
            Result = GetScalar(Query, ref Name);

            if (Result == "OK")
            {
                return int.Parse(Name);
            }
            else
            {
                return 1;
            }
        }
        public string IsSuperUser(int UserSign)
        {
            string Query = null;
            string Result = null;

            Query = "SELECT SuperUser FROM OUSR WHERE Internal_K = " + UserSign;
            GetScalar(Query, ref  Result);

            return Result;
        }
        public string ContractorWhs(string CardCode)
        {
            string Query = null;
            string WhsCode = null;

            Query = "SELECT U_ContractorWhs FROM OCRD WHERE CardCode = '" + CardCode + "'";

            GetScalar(Query, ref WhsCode);
            return WhsCode;
        }
        public string TreeType(string ItemCode)
        {
            string Query = null;
            string ItemTree = null;

            Query = "SELECT TreeType FROM OITM WHERE ItemCode = '" + ItemCode + "'";

            GetScalar(Query, ref ItemTree);
            return ItemTree;
        }
        public object LastPurPrice(string TableName, string ItemCode, string MatchCol, string MatchValue)
        {
            string Query = null;
            string Price = null;

            Query = "SELECT Price " + " FROM " + TableName + " WHERE ItemCode = '" + ItemCode + "'" + " AND " + MatchCol + " = '" + MatchValue + "'";

            Price = "0";
            GetScalar(Query, ref Price);

            return Price;
        }
        public string GetDimDesc(int DimCode)
        {
            string Query = null;
            string Result = null;

            Result = "";
            Query = "SELECT DimDesc FROM ODIM WHERE DimCode = " + DimCode;
            GetScalar(Query, ref Result);

            return Result;
        }
        public string GetDimActive(int DimCode)
        {
            string Query = null;
            string Result = null;

            Result = "";
            Query = "SELECT DimActive FROM ODIM WHERE DimCode = " + DimCode;
            GetScalar(Query, ref Result);

            return Result;
        }
        public int GetEmpId(int UserSign)
        {
            string Query = null;
            string Result = null;

            Result = "-1";
            Query = "SELECT EmpId FROM OHEM WHERE UserId = " + UserSign;
            GetScalar(Query, ref Result);

            return int.Parse(Result);
        }
        public int GetManager(int EmpId)
        {
            string Query = null;
            string Result = null;

            Result = "-1";
            Query = "SELECT Manager FROM OHEM WHERE EmpId = " + EmpId;
            GetScalar(Query, ref Result);

            return int.Parse(Result);
        }
        public int GetUserId(int EmpId)
        {
            string Query = null;
            string Result = null;

            Result = "-1";
            Query = "SELECT UserId FROM OHEM WHERE EmpId = " + EmpId;
            GetScalar(Query, ref Result);

            return int.Parse(Result);
        }
        public float GetPrice(string ItemCode)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string Query = null;

            float Price = 0;

            try
            {
                //oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                //first check last purchase price
                Query = "SELECT LastPurPrc FROM OITM WHERE ItemCode = '" + ItemCode + "'";
                //oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);
                Price =(float) oDt.Rows[0][0];//oRecordset.Fields.Item(0).Value;
                if (Price != 0)
                {
                    return Price;
                }

                Query = "SELECT LstEvlPric FROM OITM WHERE ItemCode = '" + ItemCode + "'";
                //oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);


                Price = (float)oDt.Rows[0][0];// oRecordset.Fields.Item(0).Value;
                if (Price != 0)
                {
                    return Price;
                }

                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public float GetPrice(string Father, string Child)
        {
            //SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            System.Data.DataTable oDt = new System.Data.DataTable();

            string Query = null;

            float Price = 0;
            int PriceList = 0;

            try
            {
//                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Query = "SELECT * FROM ITT1 " + " WHERE Father = '" + Father + "'" + " AND Code = '" + Child + "'";

                //oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);
                //if (oRecordset.RecordCount > 0)
                if (oDt.Rows.Count > 0)

                {
                    PriceList = (int)oDt.Rows[0]["PriceList"];//oRecordset.Fields.Item("PriceList").Value;
                }
                else
                {
                    PriceList = 0;
                }

                Price = (float)oDt.Rows[0]["Price"];// oRecordset.Fields.Item("Price").Value;
                if (Price != 0)
                {
                    return Price;// oRecordset.Fields.Item("Price").Value;
                }

                if (PriceList == 0)
                {
                    return 0;
                }
                else if (PriceList == -1)
                {
                    Query = "SELECT LastPurPrc FROM OITM WHERE ItemCode = '" + Child + "'";
                }
                else if (PriceList == -2)
                {
                    Query = "SELECT LstEvlPric FROM OITM WHERE ItemCode = '" + Child + "'";
                }
                else
                {
                    Query = "SELECT Price FROM ITM1 WHERE ItemCode = '" + Child + "' AND PriceList = " + PriceList;
                }

                //oRecordset.DoQuery(Query);
                oDt = GetDataTable(Query);

                return (float) oDt.Rows[0][0]; //oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public float GetBomFactor(string ItemCode)
        {
            string Query = null;
            string Result = "1";

            try
            {
                Query = "SELECT IsNull(Qauntity, 1) FROM OITT WHERE Code = '" + ItemCode + "'";
                GetScalar(Query, ref Result);

                return float.Parse(Result);
            }
            catch (Exception Ex)
            {
                return (1);
            }
        }
        public string ImagePath()
        {
            string Query = null;
            string Result = null;

            Result = "";
            Query = "SELECT TOP 1 BitmapPath FROM OADP";
            GetScalar(Query, ref Result);

            return Result;
        }
        public string GetItemImage(string ItemCode)
        {
            string Query = null;
            string Result = null;

            Result = "";
            Query = "SELECT IsNull(PicturName, '') FROM OITM WHERE ItemCode = '" + ItemCode + "'";
            GetScalar(Query, ref Result);

            if (string.IsNullOrEmpty(Result))
            {
                return "";
            }
            else
            {
                return ImagePath() + "\\" + Result;
            }
        }
        public object GetBatchQtyBySys(string ItemCode, string WhsCode, string SysNumber)
        {
            string Query = null;
            string Result = "0";

            Query = "SELECT Quantity " + " FROM OBTQ " + " WHERE ItemCode = '" + ItemCode + "'" + " AND WhsCode = '" + WhsCode + "'" + " AND SysNumber = '" + SysNumber + "'";

            GetScalar(Query, ref Result);
            return Result;
        }
        public void loadform(string filename)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(System.Windows.Forms.Application.StartupPath + "\\" + filename);
                SboApp.LoadBatchActions(xmldoc.InnerXml);
            }
            catch (Exception ex)
            {
            }

        }
        public void FillCombo11(SAPbouiCOM.ComboBox Ocombo, string SqlQuery, string ValFld, string DescFld)
        {
            //SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
            //oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            //oRs.DoQuery(SqlQuery);
            System.Data.DataTable oDt = new System.Data.DataTable();
            oDt = GetDataTable(SqlQuery );

            for (int i = 0; i <= oDt .Rows .Count  - 1; i++)
            {
                //Ocombo.ValidValues.Add(oRs.Fields.Item(ValFld).Value, oRs.Fields.Item(DescFld).Value);
                Ocombo.ValidValues.Add(oDt.Rows[i][ValFld].ToString (), oDt.Rows[i][DescFld].ToString ());
                //oRs.MoveNext();
            }
        }
        public bool WhsWithBin(string WhsCode)
        {
            //SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
            Sql = "select * from OWHS where BinActivat='Y' and WhsCode ='" + WhsCode + "'";
            //oRs = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            //oRs.DoQuery(Sql);
            System.Data.DataTable oDt = new System.Data.DataTable();
            oDt = GetDataTable(Sql);

            //if (oRs.RecordCount > 0)
            if (oDt.Rows.Count > 0)

            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public double RemoveThousandseparator(string Val1)
        {

            //SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
            Sql = "select ThousSep  from OADM";
            //oRs = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            //oRs.DoQuery(Sql);
            System.Data.DataTable oDt = new System.Data.DataTable();
            oDt = GetDataTable(Sql);

            //if ((oRs.Fields.Item(0).Value.tostring()) == false)
            if (oDt.Rows [0][0].ToString ()!="")
            {
                char a =(char) oDt.Rows[0][0];
                //Val1 = System.Convert.ToDouble(Val1.Replace (a, ""), CultureInfo.InvariantCulture);
            }
            else
            {
                Val1 = System.Convert.ToString(double.Parse(Val1), CultureInfo.InvariantCulture);
            }
            return double.Parse(Val1);
        }
        public string GetAvgPrice(string ItemCode, string WhsCode)
        {
            SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
            if (costbywhsflag == false)
            {
                Sql = "select avgprice from oitm where itemcode = '" + ItemCode + "' ";
            }
            else
            {
                Sql = "select b.AvgPrice from OITM a left outer join OITW b on a.Itemcode=b.itemcode where a.ItemCode='" + ItemCode + "' and b.WhsCode='" + WhsCode + "'";
            }

            oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRs.DoQuery(Sql);
            if (oRs.RecordCount > 0)
            {
                return (oRs.Fields.Item(0).Value);
            }
            else
            {
                return "";
            }
        }
        public double GetAvgPrice2(string ItemCode, string WhsCode)
        {
            //SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);

            if (costbywhsflag == false)
            {
                Sql = "select avgprice from oitm where itemcode = '" + ItemCode + "' ";
            }
            else
            {
                Sql = "select b.AvgPrice from OITM a left outer join OITW b on a.Itemcode=b.itemcode where a.ItemCode='" + ItemCode + "' and b.WhsCode='" + WhsCode + "'";
            }

            //oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            //oRs.DoQuery(Sql);
            System.Data.DataTable oDt = new System.Data.DataTable();
            oDt = GetDataTable(Sql);

            if (oDt.Rows.Count > 0)

            {
                return System.Convert.ToDouble(oDt.Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }
        public bool UserGroupAuthorization(string UserId, string AuthId)
        {
            //SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
            Sql = "select PERMISSION from UGR1 a inner join USr7 b on a.GroupLink =b.GroupId where a.PermId ='" + AuthId + "' and b.UserId ='" + UserId + "'";
            //oRs = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            //oRs.DoQuery(Sql);
            System.Data.DataTable oDt = new System.Data.DataTable();
            oDt = GetDataTable(Sql);

            if (oDt.Rows.Count > 0)
            {
                if (oDt.Rows[0][0].ToString () == "F")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
        public double tryDoubleParse(string inputno)
        {
            try
            {
                double qty = System.Convert.ToDouble(inputno, CultureInfo.InvariantCulture);
                //Double.TryParse(inputno, NumberStyles.Number, CultureInfo.CurrentCulture, qty)
                return qty;
            }
            catch (Exception ex)
            {
                return 0;
            }

        }
        public string decimalreturn(string inputno)
        {
            if ((inputno.Contains(",") == true))
            {
                return inputno.Replace(",", SepDec);
            }
            else if ((inputno.Contains(".") == true))
            {
                return inputno.Replace(".", SepDec);
            }
            else
            {
                return inputno;
            }
        }
        public string decimalreturn(string inputno, bool userfield)
        {
            //Dim output As String = inputno.ToString(CultureInfo.InvariantCulture)

            if ((inputno.Contains(",") == true))
            {
                return inputno.Replace(",", ".");
            }
            else
            {
                return inputno.ToString();
            }
        }
        public string decimalreturn1(double inputno)
        {

            return inputno.ToString(CultureInfo.InvariantCulture);
        }
        public void linktobatch(string batchid)
        {
            int LineNo = 0;

            try
            {
                SboApp.ActivateMenuItem("12290");
                LineNo = 1;
                //Dim batchform As SAPbouiCOM.Form = obe1.SboApp.Forms.ActiveForm
                SAPbouiCOM.Form batchform = SboApp.Forms.GetForm("65053", -1);
                LineNo = 2;

                if (batchform.Mode != SAPbouiCOM.BoFormMode.fm_FIND_MODE)
                {
                    LineNo = 3;
                    batchform.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;
                    LineNo = 4;
                }
                SAPbouiCOM.EditText editext = batchform.Items.Item("62").Specific;
                LineNo = 5;
                editext.Value = batchid.ToString();
                LineNo = 6;
                batchform.Items.Item("37").Click();
                LineNo = 7;

            }
            catch (Exception ex)
            {
                oMsgShow.Messsage("Line No: " + (LineNo.ToString().Trim()) + " Active Batch Exception : " + ex.Message.ToString(), MsgShow.MsgType.Statusbar, MsgShow.MsgCat.Errors);

            }
            finally
            {
            }
        }
        public void FillCombo(SAPbouiCOM.ComboBox fillcombo, string sqltext, string field1, string field2, bool norefresh)
        {
            try
            {
                // BPN 07/23/2014
                // fill combo info by SQL statement. Combo will always have a empty empty as the first select to allow user select null value 
                if ((norefresh == true & fillcombo.ValidValues.Count > 0))
                {
                    return;
                }

                //SAPbobsCOM.Recordset orecord = default(SAPbobsCOM.Recordset);

                //orecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                //orecord.DoQuery(sqltext);
                System.Data.DataTable oDt = new System.Data.DataTable();
                oDt = GetDataTable(sqltext);

                while ((fillcombo.ValidValues.Count > 0))
                {
                    fillcombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
                }
                fillcombo.ValidValues.Add("", "");
                for (int i = 0; i <= oDt.Rows.Count  - 1; i++)
                {
                    fillcombo.ValidValues.Add(oDt.Rows[i][field1].ToString(), oDt.Rows[i][field2].ToString());
                    //orecord.MoveNext();
                }


            }
            catch (Exception ex)
            {
            }
        }

        public System.Data.DataTable GetDataTable(string sql)
        {


            SqlCommand cmdMain;// = default(System.Data.SqlClient.SqlCommand);
            SqlDataAdapter dataAdapt;

            try
            {
                try
                {
                    if (Global.connobj.State == ConnectionState.Closed)
                    {
                        //ADO_Connection();
                    }
                }
                catch (Exception ex)
                {
                    //ADO_Connection();
                }


                System.Data.DataTable odtMain = new System.Data.DataTable();
                dataAdapt = new SqlDataAdapter();
                cmdMain = new SqlCommand(sql, Global.connobj);
                dataAdapt.SelectCommand = cmdMain;
                dataAdapt.Fill(odtMain);
                return odtMain;

            }
            catch (Exception ex)
            {

                return null;
            }

        }
        public int ADONonExeSql(string sql)
        {
            System.Data.SqlClient.SqlCommand cmdMain = default(System.Data.SqlClient.SqlCommand);
            try
            {
                try
                {
                    if (Global.connobj.State == ConnectionState.Closed)
                    {
                        //ADO_Connection();
                    }
                }
                catch (Exception ex)
                {
                    //ADO_Connection();
                }

                cmdMain = new SqlCommand(sql, Global.connobj);
                return cmdMain.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
            }
        }

        //public static void ADO_Connection()
        //{
        //    SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
        //    // Dim oDtTbl As New DataTable
        //    try
        //    {
        //        connobj.Close();

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
        //    Sql = "Select * from be1s_ConnInfo";
        //    oRs.DoQuery(Sql);
        //    //oDtTbl = CCUtility.GetDataTable(Sql)
        //    if (oRs.RecordCount > 0)
        //    {
        //        connobj = new SqlConnection();
        //        connobj.ConnectionString = "server=" + oCompany.Server.ToString() + ";uid=" + oRs.Fields.Item(0).Value.ToString + ";pwd=" + (oRs.Fields.Item(1).Value.ToString) + ";database=" + oCompany.CompanyDB.ToString();
        //        connobj.Open();

        //        if ((connobj.State != ConnectionState.Open))
        //        {
        //            oMsgShow.Messsage("[87 |Using ADO Cannot Connect to Database]: " + oCompany.CompanyDB.ToString(), MsgShow.MsgType.PopUp);
        //            return;
        //        }
        //    }
        //}



        #endregion
    }
}


