﻿Imports System.Data.SqlClient

Module SQLCreateData
    Public Sub CreateSQLDATADO()
        Try
            Dim TablesData As System.Data.DataSet = New System.Data.DataSet("SQLCREATE")
            TablesData.ReadXml(Application.StartupPath & "\" & "SQLCreate.xml")
            Dim TableCount As Integer
            Dim Name, sqltext, sqltext1, Type As String
            Dim runflag As Boolean = False
            Dim cmdobj As SqlClient.SqlCommand
            Dim readobj As SqlClient.SqlDataReader
            Dim connobj As SqlClient.SqlConnection
            Dim Finalmsg As String = ""


            connobj = New SqlConnection()
            connobj.ConnectionString = "server=" + oCompany.Server.ToString() + ";uid=" + sqluser + ";pwd=" + sqlpwd.ToString() + ";database=" + oCompany.CompanyDB.ToString()
            connobj.Open()
            If (connobj.State <> ConnectionState.Open) Then
                MsgLang.Messsage("Cannot Connect to Database: " + oCompany.CompanyDB.ToString(), MsgType.PopUp)
                Return
            End If

            TableCount = TablesData.Tables(0).Rows.Count
            If (TableCount) = 0 Then
                MsgLang.Messsage("No SQL object needed to be loaded !", MsgType.PopUp)
                Return
            End If


            For Flag As Integer = 0 To TableCount - 1
                Name = TablesData.Tables(0).Rows(Flag).Item(0)
                Type = TablesData.Tables(0).Rows(Flag).Item(1)
                sqltext1 = TablesData.Tables(0).Rows(Flag).Item(2)
                runflag = False
                MsgLang.Messsage("Creating SQL " + Type.ToString() + " " + Name, MsgType.Statusbar, MsgCat.Warning)
                Try
                    Try
                        If Type = "Table" Then
                            cmdobj = New SqlCommand("select count(*) as count from " + Name, connobj)
                            readobj = cmdobj.ExecuteReader
                            While readobj.Read
                                If (System.Convert.ToInt32(readobj("count").ToString()) = 0) Then
                                    runflag = True
                                Else
                                    runflag = False
                                    Finalmsg = Finalmsg + Type + " : " + Name + " Already exist and have value, please change table structure manually" + vbCrLf
                                End If
                            End While
                        Else
                            runflag = True
                        End If
                        If (readobj.IsClosed = False) Then
                            readobj.Close()
                        End If
                    Catch
                        runflag = True
                    End Try
                    cmdobj = Nothing
                    readobj = Nothing
                    GC.Collect()
                    If (runflag = True) Then
                        Try

                            If Type.ToUpper() = "SQLPROC" Then
                                sqltext = "IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = '" + Name + "') DROP PROCEDURE " + Name
                            ElseIf Type.ToUpper() = "TABLE" Then
                                sqltext = "IF EXISTS (SELECT * FROM sys.tables WHERE name = '" + Name + "') DROP Table " + Name
                            ElseIf Type.ToUpper() = "FUNCTION" Then
                                sqltext = "IF EXISTS (SELECT * FROM sys.objects WHERE type in ('FN', 'IF') AND name = '" + Name + "') DROP Function " + Name
                            Else
                                sqltext = ""
                            End If
                            If (sqltext <> "") Then
                                cmdobj = New SqlCommand(sqltext, connobj)
                                cmdobj.ExecuteNonQuery()
                            End If
                            If (sqltext1 <> "") Then

                                cmdobj = New SqlCommand(sqltext1, connobj)
                                cmdobj.ExecuteNonQuery()
                            End If

                            Finalmsg = Finalmsg + Type + " : " + Name + " Created Successfully" + vbCrLf
                        Catch ex1 As SqlException
                            Finalmsg = Finalmsg + Type + " : " + Name + " Created Exception :" + ex1.Message() + vbCrLf
                        End Try

                    End If

                Catch ex As SqlException
                    MsgLang.Messsage("Import SQL Object " + Type.ToString + " " + Name.ToString() + " Exception : " + ex.Message.ToString(), MsgType.PopUp)
                End Try
            Next
            connobj.Close()
            MsgLang.Messsage(Finalmsg.ToString(), MsgType.PopUp)
        Catch ex As Exception
            MsgLang.Messsage("Import SQL Object Main Exception : " + ex.Message.ToString(), MsgType.PopUp)
        End Try
    End Sub

End Module
