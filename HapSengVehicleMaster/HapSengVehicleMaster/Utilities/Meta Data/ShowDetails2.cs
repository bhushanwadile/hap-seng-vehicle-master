﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Collections;
using System.Data;
using System.Diagnostics;

namespace HapSengVehicleMaster
{
public class _ShowDetails2 : UIDesign 
{

    private SAPbouiCOM.Form oForm;
    //private SAPbouiCOM.Form oBForm;
    //private SAPbouiCOM.EditTextColumn oBTxt;
    //private FormOpenType FormType;
    //private SAPbouiCOM.Cell oBCell;
    //private SAPbouiCOM.EditText oBEditext;
    private SAPbouiCOM.Button oBtn;
    private SAPbouiCOM.EditText oTxt;
    


    
    #region "Form Definitions"
    //public enum FormOpenType
    //{
    //    WithMatrixCell,
    //    WithLargTextCell,
    //    WithGridCell,
    //    WithMatrixEditBox,
    //    WithLargTextEditBox,
    //    WithGridEditBox
    //}
    public _ShowDetails2():base()
    {
    }

    public _ShowDetails2(SAPbouiCOM.Form BaseForm, SAPbouiCOM.Cell BaseTextBox, FormOpenType FormOpenType1, bool Editable = true, string SQL = "", string FormTital = "", SAPbouiCOM.BoMatrixSelect SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single,string SpecificFormDesign="")
        : base()
    {
        try
        {
            SAPbouiCOM.Form oForm = default(SAPbouiCOM.Form);
            oForm = SboApp.Forms.GetForm("ShowDetails2", 0);
            oForm.Close();

        }
        catch (Exception ex)
        {
        }

        ChildBaseForm = BaseForm.TypeEx ;

        MainSql = SQL;
        oBForm = BaseForm;
        FormType = FormOpenType1;
        ChildOpen = 1;
        if (FormType == Global.FormOpenType.WithLargTextCell | FormType == Global.FormOpenType.WithGridCell | FormType == Global.FormOpenType.WithMatrixCell)
        {
            oBCell = BaseTextBox;
        }
        else
        {
            oBEditext = (SAPbouiCOM.EditText )  BaseTextBox;
        }

        if (FormOpenType1 == Global.FormOpenType.WithLargTextCell)
        {
            CreateFormWithLargText(SQL, FormTital, Editable);
        }
        if (FormOpenType1 == Global.FormOpenType.WithMatrixCell)
        {
            CreateFormWithMatrix(SQL, FormTital, Editable);
        }
        if (FormOpenType1 == Global.FormOpenType.WithGridCell)
        {
            if (SpecificFormDesign == "B1SLINMASSQ")
            {
                CreateFormWithGrid_SQ(SQL, FormTital, Editable, SelectionMode);
            }
            else
            {
                CreateFormWithGrid(SQL, FormTital, Editable, SelectionMode);
            }
        }
    }
    public _ShowDetails2(SAPbouiCOM.Form BaseForm, SAPbouiCOM.EditText BaseTextBox, FormOpenType FormOpenType1, bool Editable = true, string SQL = "", string FormTital = "", SAPbouiCOM.BoMatrixSelect SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single)
        : base()
    {
        try
        {
            SAPbouiCOM.Form oForm = default(SAPbouiCOM.Form);
            oForm = SboApp.Forms.GetForm("ShowDetails2", 0);
            oForm.Close();
        }
        catch (Exception ex)
        {
        }
        MainSql = SQL;
        oBForm = BaseForm;
        FormType = FormOpenType1;

        if (FormType == Global.FormOpenType.WithLargTextCell | FormType == Global.FormOpenType.WithGridCell | FormType == Global.FormOpenType.WithMatrixCell)
        {
            BaseTextBox.Item.Enabled = true;
            oBCell =(SAPbouiCOM.Cell) BaseTextBox;

        }
        else
        { 
            oBEditext = BaseTextBox;
        }

        if (FormOpenType1 == Global.FormOpenType.WithLargTextCell | FormOpenType1 == Global.FormOpenType.WithLargTextEditBox)
        {
            CreateFormWithLargText(SQL, FormTital, Editable);
        }
        if (FormOpenType1 == Global.FormOpenType.WithMatrixCell | FormOpenType1 == Global.FormOpenType.WithMatrixEditBox)
        {
            CreateFormWithMatrix(SQL, FormTital, Editable);
        }
        if (FormOpenType1 == Global.FormOpenType.WithGridCell | FormOpenType1 == Global.FormOpenType.WithGridEditBox)
        {
            CreateFormWithGrid(SQL, FormTital, Editable, SelectionMode);
        }
    }
    public void CreateFormWithLargText(string sql, string FormTital, bool Editable = true)
	{
		SAPbouiCOM.FormCreationParams oCreationParams = default(SAPbouiCOM.FormCreationParams);
		SAPbouiCOM.EditText oTxt = default(SAPbouiCOM.EditText);
		oCreationParams = SboApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams);

		oCreationParams.FormType = "ShowDetails2";
		oCreationParams.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed;


		try {
			oForm = SboApp.Forms.AddEx(oCreationParams);

			oForm.Title = "Detail Info";
			oForm.Height = 296;
			oForm.Width = 453;

			if (!string.IsNullOrEmpty(FormTital)) {
				oForm.Title = FormTital;
			} else {
				oForm.Title = "Detail View";
			}

			oForm.Left = SboApp.Desktop.Width / 2 - oForm.Width / 2;
			oForm.Top = SboApp.Desktop.Height / 2 - oForm.Height / 2 - 80;
            SAPbouiCOM.Button oBtn;
            
			oBtn = (SAPbouiCOM .Button)  (AddFormItem(ref oForm , "B1S_Btn1", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 5, 231, 65, 19, "", "Ok","","",true,true,true ,0 ,false));
			
            oBtn=(SAPbouiCOM .Button) (AddFormItem(ref oForm, "2", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 75, 231, 65, 19));
			oForm.DefButton = "B1S_Btn1";

			oForm.DataSources.UserDataSources.Add("DtlText", SAPbouiCOM.BoDataType.dt_LONG_TEXT);
			oTxt =(SAPbouiCOM .EditText ) AddFormItem(ref oForm, "B1S_Txt1", SAPbouiCOM.BoFormItemTypes.it_EXTEDIT, 5, 5, 425, 215,"" ,"" , "","DtlText", true );

			if (FormType == Global.FormOpenType.WithLargTextCell) {
				if (!string.IsNullOrEmpty(oBCell.Specific.Value)) {
					oForm.DataSources.UserDataSources.Item("DtlText").Value = oBCell.Specific.Value;
				}
			} else {
				if (!string.IsNullOrEmpty(oBEditext.Value)) {
					oForm.DataSources.UserDataSources.Item("DtlText").Value = oBEditext.Value;
				}
			}
			if (!string.IsNullOrEmpty(FormTital)) {
				oForm.Title = FormTital;
			} else {
				oForm.Title = "Detail View";
			}



			oForm.Visible = true;
		} catch (Exception ex) {
			SboApp.SetStatusBarMessage(ex.Message);
		}
	}
    public void CreateFormWithMatrix(string sql, string FormTital, bool Editable = true)
	{
		SAPbouiCOM.FormCreationParams oCreationParams = default(SAPbouiCOM.FormCreationParams);

		SAPbouiCOM.Matrix oMatrix = default(SAPbouiCOM.Matrix);
		string[] selecstring = null;
		string OrgVal = null;
		oCreationParams = SboApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams);

		oCreationParams.FormType = "ShowDetails2";
		oCreationParams.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed;


		try {
			oForm = SboApp.Forms.AddEx(oCreationParams);

			oForm.Title = "Detail Info";
			//oForm.Left = 300
			oForm.Height = 296;
			oForm.Width = 453;
			oForm.EnableMenu("1292", true);
			oForm.EnableMenu("1293", true);
			if (!string.IsNullOrEmpty(FormTital)) {
				oForm.Title = FormTital;
			} else {
				oForm.Title = "Detail View";
			}

			oForm.Left = SboApp.Desktop.Width / 2 - oForm.Width / 2;
			oForm.Top = SboApp.Desktop.Height / 2 - oForm.Height / 2 - 80;


			 AddFormItem(ref oForm, "B1S_Btn1", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 5, 231, 65, 19, "", "Ok");
			AddFormItem(ref oForm, "2", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 75, 231, 65, 19);
			oForm.DefButton = "B1S_Btn1";

			//oForm.DataSources.UserDataSources.Add("DtlText", SAPbouiCOM.BoDataType.dt_SHORT_TEXT)
			//AddFormItem(ref oForm, "txtSearch", SAPbouiCOM.BoFormItemTypes.it_EDIT, 78, 5, 161, 15, "", "", "", "DtlText", True)
			//AddFormItem(ref oForm, "lblSearch", SAPbouiCOM.BoFormItemTypes.it_STATIC, 5, 5, 72, 15, "txtSearch", "Find", "", "")

			oForm.DataSources.UserDataSources.Add("DtlText", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
			oForm.DataSources.UserDataSources.Add("SrNo", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER);
			oMatrix = (SAPbouiCOM .Matrix )(AddFormItem(ref oForm, "B1S_Matrix", SAPbouiCOM.BoFormItemTypes.it_MATRIX, 5, 5, 425, 215));
			SAPbouiCOM.Column oCol=(SAPbouiCOM .Column) AddColumn(ref oMatrix, "#", SAPbouiCOM.BoFormItemTypes.it_EDIT, 20, "#" ,"" , "SrNo", false);
            oCol = (SAPbouiCOM.Column) AddColumn(ref oMatrix, "Data1", SAPbouiCOM.BoFormItemTypes.it_EDIT, 400, FormTital, "", "DtlText", Editable, true);

			oMatrix.AddRow(50);

			if (FormType == Global.FormOpenType.WithLargTextCell | FormType == Global.FormOpenType.WithGridCell | FormType == Global.FormOpenType.WithMatrixCell) {
				if (!string.IsNullOrEmpty(oBCell.Specific.Value)) {
					OrgVal = oBCell.Specific.Value;
				} else {
					OrgVal = "";
				}
			} else {
				if (!string.IsNullOrEmpty(oBEditext.Value)) {
					OrgVal = oBEditext.Value;
				} else {
					OrgVal = "";
				}
			}


			if (!string.IsNullOrEmpty(OrgVal)) {
				selecstring = OrgVal.Split(new char[] { ',' });
				for (int i1 = 0; i1 <= (selecstring.Count() - 1); i1++) 
                {
					if (!string.IsNullOrEmpty(selecstring[i1])) 
                    {
						//oMatrix.AddRow()
						oMatrix.Columns.Item("#").Cells.Item(i1 + 1).Specific.value = i1 + 1;
						oMatrix.Columns.Item("Data1").Cells.Item(i1 + 1).Specific.value = selecstring[i1].ToString();
					}
				}
			}

			oForm.Visible = true;
		} catch (Exception ex) {
			SboApp.SetStatusBarMessage(ex.Message);
		}
	}
    public void CreateFormWithGrid(string sql, string FormTital, bool Editable = true, SAPbouiCOM.BoMatrixSelect SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single)
    {
        SAPbouiCOM.FormCreationParams oCreationParams = default(SAPbouiCOM.FormCreationParams);

        SAPbouiCOM.Grid oGrid = default(SAPbouiCOM.Grid);
        string[] selecstring = null;
        string OrgVal = null;
        oCreationParams = SboApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams);

        oCreationParams.FormType = "ShowDetails2";
        oCreationParams.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed;
        oCreationParams.Modality = SAPbouiCOM.BoFormModality.fm_Modal;

        try
        {
            oForm = SboApp.Forms.AddEx(oCreationParams);

            oForm.Title = "Detail Info";
            //oForm.Left = 300
            oForm.Height = 320;
            oForm.Width = 453;
            oForm.EnableMenu("1292", true);
            oForm.EnableMenu("1293", true);
            if (!string.IsNullOrEmpty(FormTital))
            {
                oForm.Title = FormTital;
            }
            else
            {
                oForm.Title = "Detail View";
            }

            oForm.Left = SboApp.Desktop.Width / 2 - oForm.Width / 2;
            oForm.Top = SboApp.Desktop.Height / 2 - oForm.Height / 2 - 80;

            AddFormItem(ref oForm, "B1S_Btn1", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 5, 255, 65, 19, "", "Choose");
            AddFormItem(ref oForm, "2", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 75, 255, 65, 19);
            oForm.DefButton = "B1S_Btn1";

            oForm.DataSources.UserDataSources.Add("DtlText", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            AddFormItem(ref oForm, "txtSearch", SAPbouiCOM.BoFormItemTypes.it_EDIT, 78, 5, 161, 15, "", "", "",
            "DtlText", true);
            AddFormItem(ref oForm, "lblSearch", SAPbouiCOM.BoFormItemTypes.it_STATIC, 5, 5, 72, 15, "txtSearch", "Find", "",
            "");

            oGrid = (SAPbouiCOM .Grid)  AddFormItem(ref oForm, "B1S_Grid", SAPbouiCOM.BoFormItemTypes.it_GRID, 5, 27, 425, 215, "", "", "","", Editable);


            if (oForm.DataSources.DataTables.Count != 0)
            {
                oForm.DataSources.DataTables.Item("MydataTable").Clear();
            }
            else
            {
                oForm.DataSources.DataTables.Add("MydataTable");
            }


            oGrid.DataTable = oForm.DataSources.DataTables.Item("MydataTable");

            oForm.DataSources.DataTables.Item(0).ExecuteQuery(sql);
            oGrid.SelectionMode = SelectionMode;

            if ((oGrid.DataTable.Rows.Count < 100))
            {
                for (int i = 0; i <= oGrid.DataTable.Rows.Count - 1; i++)
                {
                    oGrid.RowHeaders.SetText(i, (i + 1).ToString());
                }
            }

            for (int i = 0; i <= oGrid.DataTable.Columns.Count - 1; i++)
            {
                if ((i != 0))
                {
                    oGrid.Columns.Item(i).TitleObject.Sortable = true;
                }
                if (string.IsNullOrEmpty((oGrid.DataTable.Columns.Item(i).Name.Trim ())))
                {
                    oGrid.Columns.Item(i).Width = 0;
                }
            }

            SelectedCol = oGrid.DataTable.Columns.Item(0).Name;

            if (FormType == Global.FormOpenType.WithLargTextCell | FormType == Global.FormOpenType.WithGridCell | FormType == Global.FormOpenType.WithMatrixCell)
            {
                if (!string.IsNullOrEmpty(oBCell.Specific.Value))
                {
                    OrgVal = oBCell.Specific.Value;
                }
                else
                {
                    OrgVal = "";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(oBEditext.Value))
                {
                    OrgVal = oBEditext.Value;
                }
                else
                {
                    OrgVal = "";
                }
            }


            if (!string.IsNullOrEmpty(OrgVal))
            {
                selecstring = OrgVal.Split(new char[] { ',' });
                if ((oGrid.DataTable.Rows.Count < 100))
                {
                    for (int i1 = 0; i1 <= oGrid.DataTable.Rows.Count - 1; i1++)
                    {
                        if (Array.BinarySearch(selecstring, oGrid.DataTable.Columns.Item(0).Cells.Item(i1).Value.ToString()) >= 0)
                        {
                            oGrid.Rows.SelectedRows.Add(i1);
                        }
                    }
                }
            }

            oForm.Visible = true;
        }
        catch (Exception ex)
        {
            SboApp.SetStatusBarMessage(ex.Message);
        }
    }

    public void CreateFormWithGrid_SQ(string sql, string FormTital, bool Editable = true, SAPbouiCOM.BoMatrixSelect SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single)
    {
        SAPbouiCOM.FormCreationParams oCreationParams = default(SAPbouiCOM.FormCreationParams);

        SAPbouiCOM.Grid oGrid = default(SAPbouiCOM.Grid);
        string[] selecstring = null;
        string OrgVal = null;
        oCreationParams = SboApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams);

        oCreationParams.FormType = "ShowDetails2";
        oCreationParams.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed;
        oCreationParams.Modality = SAPbouiCOM.BoFormModality.fm_Modal;

        try
        {
            oForm = SboApp.Forms.AddEx(oCreationParams);

            oForm.Title = "Detail Info";
            //oForm.Left = 300
            oForm.Height = 450;
            oForm.Width = 653;
            oForm.EnableMenu("1292", true);
            oForm.EnableMenu("1293", true);
            if (!string.IsNullOrEmpty(FormTital))
            {
                oForm.Title = FormTital;
            }
            else
            {
                oForm.Title = "Detail View";
            }

            oForm.Left = SboApp.Desktop.Width / 2 - oForm.Width / 2;
            oForm.Top = SboApp.Desktop.Height / 2 - oForm.Height / 2 - 80;

            //AddFormItem(ref oForm, "B1S_BtnSQ", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 5, 390, 65, 19, "", "Choose");
            //AddFormItem(ref oForm, "2", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 75, 390, 65, 19);
            //oForm.DefButton = "B1S_Btn1";

            oForm.DataSources.UserDataSources.Add("DtlText", SAPbouiCOM.BoDataType.dt_SHORT_TEXT);
            AddFormItem(ref oForm, "txtSearch", SAPbouiCOM.BoFormItemTypes.it_EDIT, 78, 5, 161, 15, "", "", "",
            "DtlText", true);
            AddFormItem(ref oForm, "lblSearch", SAPbouiCOM.BoFormItemTypes.it_STATIC, 5, 5, 72, 15, "txtSearch", "Find", "",
            "");

            oGrid = (SAPbouiCOM.Grid)AddFormItem(ref oForm, "B1S_GridSQ", SAPbouiCOM.BoFormItemTypes.it_GRID, 5, 27, 625, 350, "", "", "",
            "", Editable);
            

            if (oForm.DataSources.DataTables.Count != 0)
            {
                oForm.DataSources.DataTables.Item("MydataTable").Clear();
            }
            else
            {
                oForm.DataSources.DataTables.Add("MydataTable");
            }


            oGrid.DataTable = oForm.DataSources.DataTables.Item("MydataTable");

            oForm.DataSources.DataTables.Item(0).ExecuteQuery(sql);
            oGrid.SelectionMode = SelectionMode;

            if ((oGrid.DataTable.Rows.Count < 100))
            {
                for (int i = 0; i <= oGrid.DataTable.Rows.Count - 1; i++)
                {
                    oGrid.RowHeaders.SetText(i, (i + 1).ToString());
                }
            }

            for (int i = 0; i <= oGrid.DataTable.Columns.Count - 1; i++)
            {
                if ((i != 0))
                {
                    oGrid.Columns.Item(i).TitleObject.Sortable = true;
                }
                if (string.IsNullOrEmpty((oGrid.DataTable.Columns.Item(i).Name.Trim())))
                {
                    oGrid.Columns.Item(i).Width = 0;
                }
            }

            SelectedCol = oGrid.DataTable.Columns.Item(0).Name;

            if (FormType == Global.FormOpenType.WithLargTextCell | FormType == Global.FormOpenType.WithGridCell | FormType == Global.FormOpenType.WithMatrixCell)
            {
                if (!string.IsNullOrEmpty(oBCell.Specific.Value))
                {
                    OrgVal = oBCell.Specific.Value;
                }
                else
                {
                    OrgVal = "";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(oBEditext.Value))
                {
                    OrgVal = oBEditext.Value;
                }
                else
                {
                    OrgVal = "";
                }
            }


            if (!string.IsNullOrEmpty(OrgVal))
            {
                selecstring = OrgVal.Split(new char[] { ',' });
                if ((oGrid.DataTable.Rows.Count < 100))
                {
                    for (int i1 = 0; i1 <= oGrid.DataTable.Rows.Count - 1; i1++)
                    {
                        if (Array.BinarySearch(selecstring, oGrid.DataTable.Columns.Item(0).Cells.Item(i1).Value.ToString()) >= 0)
                        {
                            oGrid.Rows.SelectedRows.Add(i1);
                        }
                    }
                }
            }

            oForm.Visible = true;
        }
        catch (Exception ex)
        {
            SboApp.SetStatusBarMessage(ex.Message);
        }
    }
    #endregion
    #region "Events Handling"
    public void EventHandler(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
    {
        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_CLICK)
        {
            ItemClickEvent(ref pVal, ref BubbleEvent);
        }
        else if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
        {
            DoubleClickEvent(ref pVal, ref BubbleEvent);
        }
        else if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_KEY_DOWN)
        {
            KeyDownEvent(ref pVal, ref BubbleEvent);
        }
        else if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
        {
            FormUnloadEvent(ref pVal, ref BubbleEvent);
        }
        else if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
        {
            ItemPressEvent(ref pVal, ref BubbleEvent);
        }
    }

    public void ItemPressEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
    {
        if (pVal.ItemUID == "B1S_Grid" & pVal.BeforeAction == true & pVal.Row <= 0 & pVal.ColUID != "RowsHeader")
        {
            BubbleEvent = false;
        }
    }
    public void ItemClickEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
    {
        try
        {
            
            string OutPut = "";

            if (pVal.ItemUID == "2" && pVal.BeforeAction == false)
            {

                //oBEditext.Value = "---";
            }

            if (pVal.ItemUID == "B1S_Btn1" && pVal.Before_Action == false)
            {
                ChildOpen = 0;
                oForm = SboApp.Forms.GetForm(pVal.FormTypeEx, pVal.FormTypeCount);
                if (FormType == Global.FormOpenType.WithLargTextCell)
                {
                    oBCell.Specific.value = oForm.Items.Item("B1S_Txt1").Specific.value;
                }
                if (FormType == Global.FormOpenType.WithMatrixCell)
                {
                    SAPbouiCOM.Matrix oMatrix = default(SAPbouiCOM.Matrix);
                    oMatrix = oForm.Items.Item("B1S_Matrix").Specific;
                    for (int i = 0; i <= oMatrix.RowCount - 1; i++)
                    {
                        if (!string.IsNullOrEmpty(oMatrix.Columns.Item("Data1").Cells.Item(i + 1).Specific.value))
                        {
                            OutPut = OutPut + "," + oMatrix.Columns.Item("Data1").Cells.Item(i + 1).Specific.value;
                        }
                    }
                    oBCell.Specific.value = OutPut.Substring (2);
                }
                if (FormType == Global.FormOpenType.WithGridCell)
                {
                    SAPbouiCOM.Grid oGrid = default(SAPbouiCOM.Grid);
                    int rowno = 0;                       


                    oGrid = oForm.Items.Item("B1S_Grid").Specific;

                    for (int i2 = 0; i2 <= oGrid.Rows.SelectedRows.Count - 1; i2++)
                    {
                        rowno = oGrid.Rows.SelectedRows.Item(i2, SAPbouiCOM.BoOrderType.ot_RowOrder);
                        if (!string.IsNullOrEmpty(oGrid.DataTable.Columns.Item(0).Cells.Item(rowno).Value.ToString()))
                        {
                            if (!string.IsNullOrEmpty(OutPut))
                            {
                                OutPut = OutPut + "," + oGrid.DataTable.Columns.Item(0).Cells.Item(rowno).Value.ToString();
                            }
                            else
                            {
                                OutPut = oGrid.DataTable.Columns.Item(0).Cells.Item(rowno).Value.ToString();
                            }
                        }

                    }
                    oBCell.Specific.value = OutPut;
                }

                if (FormType == Global.FormOpenType.WithLargTextEditBox)
                {
                    oBEditext.Value = oForm.Items.Item("B1S_Txt1").Specific.value;
                }
                if (FormType == Global.FormOpenType.WithMatrixEditBox)
                {
                    SAPbouiCOM.Matrix oMatrix = default(SAPbouiCOM.Matrix);

                    oMatrix = oForm.Items.Item("B1S_Matrix").Specific;
                    for (int i = 0; i <= oMatrix.RowCount - 1; i++)
                    {
                        if (!string.IsNullOrEmpty(oMatrix.Columns.Item("Data1").Cells.Item(i + 1).Specific.value))
                        {
                            OutPut = OutPut + "," + oMatrix.Columns.Item("Data1").Cells.Item(i + 1).Specific.value;
                        }
                    }
                    oBEditext.Value = OutPut.Substring ( 2);
                }
                if (FormType == Global.FormOpenType.WithGridEditBox)
                {
                    SAPbouiCOM.Grid oGrid = default(SAPbouiCOM.Grid);
                    int rowno = 0;
                    oGrid = oForm.Items.Item("B1S_Grid").Specific;
                    for (int i2 = 0; i2 <= oGrid.Rows.SelectedRows.Count - 1; i2++)
                    {
                        rowno = oGrid.Rows.SelectedRows.Item(i2, SAPbouiCOM.BoOrderType.ot_RowOrder);
                        if (!string.IsNullOrEmpty(oGrid.DataTable.Columns.Item(0).Cells.Item(rowno).Value.ToString()))
                        {
                            if (!string.IsNullOrEmpty(OutPut))
                            {
                                OutPut = OutPut + "," + oGrid.DataTable.Columns.Item(0).Cells.Item(rowno).Value.ToString();
                            }
                            else
                            {
                                OutPut = oGrid.DataTable.Columns.Item(0).Cells.Item(rowno).Value.ToString();
                            }
                        }

                    }
                    //if (listChassis.Contains(OutPut))
                    //{
                    //    SboApp.StatusBar.SetText("Chassis No already selected please choose a different one.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                    //}
                    //else
                    //{
                    //    listChassis.Add(OutPut);
                        oBEditext.Value = OutPut;
                    //}

                }

                oForm.Close();
            }
            if (pVal.ItemUID == "B1S_Grid" & pVal.BeforeAction == false)
            {
                SAPbouiCOM.Grid oGrid = default(SAPbouiCOM.Grid);
                oGrid = oForm.Items.Item("B1S_Grid").Specific;
                SelectedCol = pVal.ColUID;
                oGrid.Rows.SelectedRows.Clear();
                oGrid.Rows.SelectedRows.Add(pVal.Row);
            }
            if (pVal.ItemUID == "B1S_Grid" & pVal.BeforeAction == true & pVal.Row < 0 & pVal.ColUID != "RowsHeader")
            {
                BubbleEvent = false;
            }


        }
        catch (Exception ex)
        {
        }
    }
    public void FormUnloadEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
    {
        ChildOpen = 0;
        ChildBaseForm = "";
    }
    public void DoubleClickEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
    {
        if (pVal.ItemUID == "B1S_Grid" & pVal.BeforeAction == true & pVal.Row >= 0)
        {
            BubbleEvent = false;
            oForm = SboApp.Forms.GetForm("ShowDetails2", 0);
            SAPbouiCOM.Grid oGrid = default(SAPbouiCOM.Grid);
            oGrid = oForm.Items.Item("B1S_Grid").Specific;
            oGrid.Rows.SelectedRows.Add(pVal.Row);
            oForm.Items.Item("B1S_Btn1").Click(SAPbouiCOM.BoCellClickType.ct_Regular);
        }

        if (pVal.ItemUID == "B1S_GridSQ" & pVal.BeforeAction == true & pVal.Row >= 0)
        {
            BubbleEvent = false;
            oForm = SboApp.Forms.GetForm("ShowDetails2", 0);
            SAPbouiCOM.Grid oGrid = default(SAPbouiCOM.Grid);
            oGrid = oForm.Items.Item("B1S_GridSQ").Specific;
            oGrid.Rows.SelectedRows.Add(pVal.Row);             
            string OutPut = oGrid.DataTable.Columns.Item(pVal.ColUID).Name.ToString() + "(" + oGrid.DataTable.Columns.Item(0).Cells.Item(pVal.Row).Value.ToString() + ")";
            oBCell.Specific.value = OutPut;
            oForm.Close();
        }
    }
    public void KeyDownEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
    {
        
        if (pVal.ItemUID == "txtSearch" & pVal.BeforeAction == false)
        {
            oForm = SboApp.Forms.GetForm("ShowDetails2", 0);
            SAPbouiCOM.EditText oEditText = default(SAPbouiCOM.EditText);
            SAPbouiCOM.Grid oGrid = default(SAPbouiCOM.Grid);
            int SelectCol = 0;

            oEditText = oForm.Items.Item("txtSearch").Specific;

            if (FormType == Global.FormOpenType.WithGridCell | FormType == Global.FormOpenType.WithGridEditBox)
            {
                oGrid = oForm.Items.Item("B1S_Grid").Specific;

                if ((string.IsNullOrEmpty(oEditText.Value.Replace(" ", ""))))
                {
                    Sql = MainSql;
                }
                else
                {
                    if (MainSql.ToUpper().Contains ("WHERE") == true)
                    {
                        Sql = MainSql + " and " + oGrid.DataTable.Columns.Item(SelectedCol).Name + " like '" + oEditText.Value + "%'";
                    }
                    else
                    {
                        Sql = MainSql + " Where " + oGrid.DataTable.Columns.Item(SelectedCol).Name + " like '" + oEditText.Value + "%'";
                    }
                }
                if (oForm.DataSources.DataTables.Count != 0)
                {
                    oForm.DataSources.DataTables.Item("MydataTable").Clear();
                }
                else
                {
                    oForm.DataSources.DataTables.Add("MydataTable");
                }

                oGrid.DataTable = oForm.DataSources.DataTables.Item("MydataTable");
                oForm.DataSources.DataTables.Item(0).ExecuteQuery(Sql);
                //oGrid.SetCellFocus(0, int .Parse(SelectedCol));
            }

            if (FormType == Global.FormOpenType.WithMatrixCell | FormType == Global.FormOpenType.WithMatrixEditBox)
            {
            }

        }
    }
    #endregion
}

}