﻿Public Class CreateData
    Private oForm As SAPbouiCOM.Form
    Public Sub New()
        MyBase.New()
        
    End Sub
#Region "Form Definition"
    
    Public Sub CreateForm()
        Dim oCreationParams As SAPbouiCOM.FormCreationParams

        oCreationParams = obe1.SboApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)

        oCreationParams.FormType = "B1S_GF01"
        oCreationParams.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed

        Try
            oForm = obe1.SboApp.Forms.AddEx(oCreationParams)

            oForm.Title = "Create Tables & Fields - be1"
            oForm.Left = 300
            oForm.ClientHeight = 400
            oForm.ClientWidth = 600

            AddItemsToForm(oForm)
            oForm.Visible = True
        Catch ex As Exception
            obe1.SboApp.SetStatusBarMessage(ex.Message)
        End Try
    End Sub
    Private Sub AddItemsToForm(ByRef oForm As SAPbouiCOM.Form)
        Try


            Dim oItem As SAPbouiCOM.Item

            Dim oButton As SAPbouiCOM.Button


            Dim oMatrix As SAPbouiCOM.Matrix
            Dim oColumns As SAPbouiCOM.Columns
            Dim oColumn As SAPbouiCOM.Column

            oForm.DataSources.UserDataSources.Add("Index", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER)
            oForm.DataSources.UserDataSources.Add("Table", SAPbouiCOM.BoDataType.dt_SHORT_TEXT)
            oForm.DataSources.UserDataSources.Add("Field", SAPbouiCOM.BoDataType.dt_SHORT_TEXT)
            oForm.DataSources.UserDataSources.Add("Message", SAPbouiCOM.BoDataType.dt_LONG_TEXT)

            'add the first button
            oItem = oForm.Items.Add("BT1", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 5
            oItem.Width = 65
            oItem.Height = 19
            oItem.Top = 10

            'set the button specific properties
            oButton = oItem.Specific
            oButton.Caption = "Start"

            'add the second button
            oItem = oForm.Items.Add("BT2", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 75
            oItem.Width = 65
            oItem.Height = 19
            oItem.Top = 10

            'set the button specific properties
            oButton = oItem.Specific
            oButton.Caption = "Cancel"

            'add the Third button
            oItem = oForm.Items.Add("SQLBTN", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 500
            oItem.Width = 85
            oItem.Height = 19
            oItem.Top = 10

            'set the button specific properties
            oButton = oItem.Specific
            oButton.Caption = "SQL SP Create"

            'add matrix for messages
            oItem = oForm.Items.Add("B1S_MX01", SAPbouiCOM.BoFormItemTypes.it_MATRIX)
            oItem.Top = 40
            oItem.Left = 5
            oItem.Width = 590
            oItem.Height = 325

            oMatrix = oItem.Specific
            oColumns = oMatrix.Columns

            oColumn = oColumns.Add("#", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oColumn.Width = 20
            oColumn.TitleObject.Caption = "#"
            oColumn.Editable = False
            oColumn.DataBind.SetBound(True, "", "Index")

            oColumn = oColumns.Add("B1S_MC00", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oColumn.Width = 100
            oColumn.TitleObject.Caption = "Table"
            oColumn.Editable = True
            oColumn.DataBind.SetBound(True, "", "Table")

            oColumn = oColumns.Add("B1S_MC01", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oColumn.Width = 100
            oColumn.TitleObject.Caption = "Field"
            oColumn.Editable = True
            oColumn.DataBind.SetBound(True, "", "Field")

            oColumn = oColumns.Add("B1S_MC02", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oColumn.Width = 300
            oColumn.TitleObject.Caption = "Message"
            oColumn.Editable = True
            oColumn.DataBind.SetBound(True, "", "Message")
        Catch ex As Exception
            MsgLang.Messsage("Load UDF Form Eception : " + ex.Message.ToString(), MsgType.Statusbar, MsgCat.Errors)
        End Try
    End Sub
#End Region
#Region "Public Functions"

#End Region
#Region "Events Handling"
    Public Sub ItemPressEvent(ByVal pVal As SAPbouiCOM.ItemEvent)
        Dim oForm As SAPbouiCOM.Form
        oForm = obe1.SboApp.Forms.GetForm(pVal.FormTypeEx, pVal.FormTypeCount)
        If (pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK) Then
            If pVal.ItemUID = "BT1" And pVal.BeforeAction = False Then
                StartProcess(oForm)
            End If
            If pVal.ItemUID = "BT2" And pVal.BeforeAction = True Then
                oForm.Close()
            End If
            If pVal.ItemUID = "SQLBTN" And pVal.BeforeAction = False Then
                populateEditText("Please Input SQL user, password", {"UserName", "Password"}, {"sa", ""}, 2, edittype.SQLUSR)
            End If
        End If
    End Sub

#End Region
#Region "Private Functions"
    Private Sub reconnectsbo()
        Try
            oCompany.Disconnect()
            GC.Collect()
            oCompany = Nothing
            oCompany = New SAPbobsCOM.Company
            oCompany = obe1.SboApp.Company.GetDICompany()
            If oCompany.Connect() <> 0 Then
                obe1.SboApp.StatusBar.SetText(String.Format("{Reconnecting error:{0}-{1}", oCompany.GetLastErrorCode, oCompany.GetLastErrorDescription))
                End
            End If
        Catch

        End Try
    End Sub
    Private Sub StartProcess(ByRef oForm As SAPbouiCOM.Form)
        Try
            oForm.Items.Item("B1S_MX01").Specific.Clear()
            oForm.Items.Item("BT1").Enabled = False
            oForm.Items.Item("BT2").Enabled = False

            reconnectsbo()
            CreateTables()
            reconnectsbo()
            CreateFields()
            reconnectsbo()
            CreateObjects()
            UserPermission()
            'reconnectsbo()

            oForm.Items.Item("BT1").Enabled = True
            oForm.Items.Item("BT2").Enabled = True

            obe1.SboApp.MessageBox("Process completed, restart application.")
        Catch ex As Exception
            obe1.SboApp.SetStatusBarMessage("CrData: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
    Friend Sub CreateTables()
        Dim TablesData As System.Data.DataSet = New System.Data.DataSet("Tables")
        TablesData.ReadXml(Application.StartupPath & "\" & "Tables.xml")

        Dim TableCount As Integer
        Dim Name, Description As String
        Dim Type As Integer

        TableCount = TablesData.Tables(0).Rows.Count


        For Flag As Integer = 0 To TableCount - 1
            Name = TablesData.Tables(0).Rows(Flag).Item(0)
            Description = TablesData.Tables(0).Rows(Flag).Item(1)
            Type = TablesData.Tables(0).Rows(Flag).Item(2)

            Try
                ' Extruflex specific field. 
                If (enableEXFUP = False And Name.Contains("B1SEXFUPCFG")) Then
                    Continue For
                End If
                ' Extruflex specific field end 
                Dim oUserTableMd As SAPbobsCOM.UserTablesMD
                oUserTableMd = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables)

                oUserTableMd.TableName = Name
                oUserTableMd.TableDescription = Description
                oUserTableMd.TableType = Type

                If oUserTableMd.Add <> 0 Then
                    ShowMessage(oForm, Name, "", oCompany.GetLastErrorDescription)
                    If ("You are not connected to a company" = oCompany.GetLastErrorDescription) Then
                        Return
                    End If
                Else
                    ShowMessage(oForm, Name, "", "Successfully created.")
                End If
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserTableMd)

                'Else
                '    ShowMessage(oForm, Name, "", "Already Exist.")
                'End If
                oUserTableMd = Nothing
                GC.Collect()
            Catch ex As System.Exception
                ShowMessage(oForm, Name, "", ex.Message)
            End Try
        Next
    End Sub
    Friend Sub CreateFields()
        Dim TableFlag, rowcounter As Integer
        Dim TableName, Table, Name, Desc As String
        Dim orecord As SAPbobsCOM.Recordset = Main.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

        Dim FieldsData As System.Data.DataSet = New System.Data.DataSet("Fields")
        FieldsData.ReadXml(Application.StartupPath & "\" & "Fields.xml")

        Dim TableCount As Integer
        TableCount = FieldsData.Tables.Count

        'delete existing fields
        For TableFlag = 0 To TableCount - 1

            For rowcounter = FieldsData.Tables(TableFlag).Rows.Count - 1 To 0 Step -1
                Table = FieldsData.Tables(TableFlag).Rows(rowcounter).Item(0)
                Name = FieldsData.Tables(TableFlag).Rows(rowcounter).Item(1)
                ' Extruflex specific field. 
                If (enableEXFUP = False And (Table.Contains("B1SEXFUPCFG") Or Name.StartsWith("EXFUP"))) Then
                    Continue For
                End If
                ' Extruflex specific field end
                If Table = "OWTR" Then
                    Dim iii = 0
                End If
                orecord.DoQuery("SELECT COLUMN_NAME, TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + Table + "' and COLUMN_NAME LIKE 'U_" + Name + "'")
                If (orecord.RecordCount > 0) Then
                    FieldsData.Tables(TableFlag).Rows(rowcounter).Delete()
                End If

            Next
        Next
        System.Runtime.InteropServices.Marshal.ReleaseComObject(orecord)
        orecord = Nothing
        GC.Collect()

        TableCount = FieldsData.Tables.Count
        For TableFlag = 0 To TableCount - 1
            Dim FieldCount As Integer
            FieldCount = FieldsData.Tables(TableFlag).Rows.Count
            TableName = FieldsData.Tables(TableFlag).TableName
            Try


                Dim FieldRows As System.Data.DataRow() = FieldsData.Tables(TableFlag).Rows(0).GetChildRows(TableName & "_Field")

                For Each Row In FieldRows
                    Table = Row.Item(0)
                    Name = Row.Item(1)
                    Desc = Row.Item(2)
                    ' Extruflex specific field. 
                    If (enableEXFUP = False And (Table.Contains("B1SEXFUPCFG") Or Name.StartsWith("EXFUP"))) Then
                        Continue For
                    End If
                    ' Extruflex specific field end
                    Try
                        Dim oUserFieldMD As SAPbobsCOM.UserFieldsMD

                        oUserFieldMD = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields)

                        oUserFieldMD.TableName = Table
                        oUserFieldMD.Name = Name
                        oUserFieldMD.Description = Desc
                        oUserFieldMD.Type = Row.Item(3)
                        oUserFieldMD.SubType = Row.Item(4)
                        oUserFieldMD.EditSize = Row.Item(5)

                        If Not IsDBNull(Row.Item(6)) And Row.Item(6).ToString() <> "" Then
                            oUserFieldMD.DefaultValue = Row.Item(6)
                        End If

                        Dim ValidCount As Integer = 0
                        Dim Rows As System.Data.DataRow() = Row.GetChildRows("Field_ValidValue")
                        For Each ValidRow In Rows

                            If ValidCount > 0 Then
                                oUserFieldMD.ValidValues.Add()
                            End If

                            oUserFieldMD.ValidValues.Value = ValidRow.Item(0)
                            oUserFieldMD.ValidValues.Description = ValidRow.Item(1)

                            ValidCount += 1
                        Next

                        If oUserFieldMD.Add <> 0 Then
                            ShowMessage(oForm, Table, Name, oCompany.GetLastErrorDescription)
                            If ("You are not connected to a company" = oCompany.GetLastErrorDescription) Then
                                Return
                            End If
                        Else
                            ShowMessage(oForm, Table, Name, "Successfully created.")
                        End If
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserFieldMD)
                        oUserFieldMD = Nothing
                        GC.Collect()

                    Catch ex As System.Exception
                        ShowMessage(oForm, Table, Name, ex.Message)
                    End Try
                Next
            Catch

            End Try
        Next
    End Sub
    Friend Sub CreateObjects()
        Dim ObjectFlag As Integer


        Dim ObjectsData As System.Data.DataSet = New System.Data.DataSet("Objects")
        ObjectsData.ReadXml(Application.StartupPath & "\" & "Objects.xml")

        Dim ObjectCount As Integer
        ObjectCount = ObjectsData.Tables(0).Rows.Count

        For ObjectFlag = 0 To ObjectCount - 1
            Dim Code As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(0)
            Dim Name As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(1)
            Dim TableName As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(2)
            Dim LogTable As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(3)
            Dim Type As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(4)
            Dim MngSeries As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(5)
            Dim CanDelete As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(6)
            Dim CanClose As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(7)
            Dim CanCancel As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(8)
            Dim ExtName As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(9)
            Dim CanFind As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(10)
            Dim CanYrTrnsf As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(11)
            Dim CanDefForm As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(12)
            Dim CanLog As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(13)
            Dim OvrWrtDll As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(14)
            Dim UIDFormat As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(15)
            Dim CanArchive As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(16)
            Dim MenuItem As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(17)
            Dim MenuCapt As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(18)
            Dim FatherMenu As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(19)
            Dim Position As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(20)
            Dim CanNewForm As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(21)
            Dim IsRebuild As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(22)
            Dim NewFormSrf As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(23)
            Dim MenuUid As String = ObjectsData.Tables(0).Rows(ObjectFlag).Item(24)

            Try
                'initialize object creation
                Dim oUserObj As SAPbobsCOM.UserObjectsMD
                oUserObj = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
                If (oUserObj.GetByKey(Code) = False) Then

                    oUserObj.Code = Code
                    oUserObj.Name = Name
                    oUserObj.TableName = TableName
                    oUserObj.ObjectType = Type

                    oUserObj.ManageSeries = IIf(MngSeries = "Y", 1, 0)
                    oUserObj.CanDelete = IIf(CanDelete = "Y", 1, 0)
                    oUserObj.CanClose = IIf(CanClose = "Y", 1, 0)
                    oUserObj.CanCancel = IIf(CanCancel = "Y", 1, 0)
                    oUserObj.CanArchive = IIf(CanArchive = "Y", 1, 0)
                    oUserObj.CanYearTransfer = IIf(CanYrTrnsf = "Y", 1, 0)
                    'oUserObj.FormSRF = NewFormSrf

                    If CanLog = "Y" Then
                        oUserObj.CanLog = SAPbobsCOM.BoYesNoEnum.tYES
                        oUserObj.LogTableName = LogTable
                    Else
                        oUserObj.CanLog = SAPbobsCOM.BoYesNoEnum.tNO
                    End If

                    If CanFind = "Y" Then
                        Dim ColCount As Integer = 0

                        oUserObj.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                        Dim FindRows As System.Data.DataRow() = ObjectsData.Tables(0).Rows(ObjectFlag).GetChildRows("Object_FindColumn")
                        For Each Row In FindRows
                            Dim ColAlias As String = Row.Item(0)
                            Dim ColDesc As String = Row.Item(1)

                            If ColCount > 0 Then
                                oUserObj.FindColumns.Add()
                            End If

                            oUserObj.FindColumns.ColumnAlias = ColAlias
                            oUserObj.FindColumns.ColumnDescription = ColDesc

                            ColCount += 1
                        Next
                    Else
                        oUserObj.CanFind = SAPbobsCOM.BoYesNoEnum.tNO
                    End If

                    Dim ChildRows As System.Data.DataRow() = ObjectsData.Tables(0).Rows(ObjectFlag).GetChildRows("Object_Child")
                    Dim ChildCount As Integer = 0
                    For Each Row In ChildRows
                        Dim ChildTable As String = Row.Item(0)
                        Dim ChildLog As String = Row.Item(1)

                        If ChildCount > 0 Then
                            oUserObj.ChildTables.Add()
                        End If

                        oUserObj.ChildTables.TableName = ChildTable

                        If ChildLog <> "" Then
                            oUserObj.ChildTables.LogTableName = ChildLog
                        End If

                        ChildCount += 1
                    Next

                    If oUserObj.Add <> 0 Then
                        ShowMessage(oForm, Name, "", oCompany.GetLastErrorDescription)
                        If ("You are not connected to a company" = oCompany.GetLastErrorDescription) Then
                            Return
                        End If
                    Else
                        ShowMessage(oForm, Name, "", "UDO successfully created.")
                    End If
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserObj)

                Else
                    ShowMessage(oForm, Name, "", "UDO Already Exist.")
                End If
                oUserObj = Nothing
                GC.Collect()
            Catch Ex As Exception
                ShowMessage(oForm, Name, "", Ex.Message)
            End Try
        Next
    End Sub
    Private Sub UserPermission()
        Dim permid, permname, permfath As String
        Dim oPerm As SAPbobsCOM.UserPermissionTree

        Try
            Dim OPermData As System.Data.DataSet = New System.Data.DataSet("UserPermissions")
            OPermData.ReadXml(Application.StartupPath & "\" & "UserPermissions.xml")

            Dim ObjectCount As Integer
            ObjectCount = OPermData.Tables(0).Rows.Count
            oPerm = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserPermissionTree)
            For i As Integer = 0 To ObjectCount - 1
                permid = OPermData.Tables(0).Rows(i).Item(0)
                permname = OPermData.Tables(0).Rows(i).Item(1)
                permfath = OPermData.Tables(0).Rows(i).Item(2)

                If (oPerm.GetByKey(permid) = False) Then
                    oPerm.PermissionID = permid
                    oPerm.Name = permname
                    oPerm.ParentID = permfath
                    oPerm.Options = SAPbobsCOM.BoUPTOptions.bou_FullNone
                    If (oPerm.Add() <> 0) Then
                        ShowMessage(oForm, "UserPermission", permname, oCompany.GetLastErrorDescription)
                    Else
                        ShowMessage(oForm, "UserPermission", permid, "Create Successfully.")
                    End If

                Else
                    ShowMessage(oForm, "UserPermission", permid, "Already Exist.")
                End If

            Next
        Catch ex As Exception
            MsgLang.Messsage("Create Permission Exception : " + ex.Message.ToString(), MsgType.Statusbar, MsgCat.Errors)
        End Try
    End Sub
    Private Sub ShowMessage(ByRef oForm As SAPbouiCOM.Form, ByVal Table As String, ByVal Field As String, ByVal MsgString As String)
        Dim RowIndex As Integer
        Dim oMatrix As SAPbouiCOM.Matrix

        oMatrix = oForm.Items.Item("B1S_MX01").Specific
        oMatrix.AddRow()

        RowIndex = oMatrix.RowCount

        oMatrix.Columns.Item("#").Cells.Item(RowIndex).Specific.Value = RowIndex
        oMatrix.Columns.Item("B1S_MC00").Cells.Item(RowIndex).Specific.Value = Table
        oMatrix.Columns.Item("B1S_MC01").Cells.Item(RowIndex).Specific.Value = Field
        oMatrix.Columns.Item("B1S_MC02").Cells.Item(RowIndex).Specific.Value = MsgString

        oMatrix.SetCellFocus(RowIndex, 2)
    End Sub
    'Friend Sub CreateData()
    '    '--------------- Create Fable & Fields 
    '    '--------------Block Reason Code
    '    addTbl("B1SLMQCRBR", "Block Reason Code", SAPbobsCOM.BoUTBTableType.bott_NoObject)

    '    '--------------Test tool table
    '    addTbl("B1SLMQCRTM", "Test Tool Table", SAPbobsCOM.BoUTBTableType.bott_NoObject)

    '    '--------------Test Template
    '    If addTbl("B1SLMQCRTT", "Test Template", SAPbobsCOM.BoUTBTableType.bott_MasterData) = 0 Then
    '        'Measurement, Attribute, Text
    '        ValidValue(0).Valide_Value = "M"
    '        ValidValue(0).Description = "Measurement"
    '        ValidValue(1).Valide_Value = "A"
    '        ValidValue(1).Description = "Attribute"
    '        ValidValue(2).Valide_Value = "T"
    '        ValidValue(2).Description = "Text"
    '        AddTblFld("B1SLMQCRTT", "TestType", "Type of Test", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
    '        AddTblFld("B1SLMQCRTT", "InstrInf", "Instruction Info", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        AddTblFld("B1SLMQCRTT", "TestValu", "Desired Value", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCRTT", "TestUOM", "Testing UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCRTT", "SucesFrm", "Success From", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCRTT", "SucesTo", "Success To", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCRTT", "SucesAry", "Success Array", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        ValidValue(0).Valide_Value = "Y"
    '        ValidValue(0).Description = "Yes"
    '        ValidValue(1).Valide_Value = "N"
    '        ValidValue(1).Description = "No"
    '        AddTblFld("B1SLMQCRTT", "Relevant", "Relevant", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "N")
    '        ValidValue(0).Valide_Value = "Y"
    '        ValidValue(0).Description = "Yes"
    '        ValidValue(1).Valide_Value = "N"
    '        ValidValue(1).Description = "No"
    '        AddTblFld("B1SLMQCRTT", "Print", "Print", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "N")
    '        AddTblFld("B1SLMQCRTT", "TestTool", "TestTool", SAPbobsCOM.BoFieldTypes.db_Alpha, 50, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_None, "B1SLMQCRTM")
    '    End If

    '    '--------------Test Plan Template Header 
    '    addTbl("B1SLMQCTP", "Test Plan Template Header", SAPbobsCOM.BoUTBTableType.bott_MasterData)

    '    '--------------Test Plan Template Line 
    '    If addTbl("B1SLMQCTP1", "Test Plan Template Line", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines) = 0 Then
    '        AddTblFld("B1SLMQCTP1", "SeqNo", "Sequence No", SAPbobsCOM.BoFieldTypes.db_Numeric, 0)
    '        AddTblFld("B1SLMQCTP1", "TTCODE", "Test Template ID", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCTP1", "TTDESC", "Test  Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        AddTblFld("B1SLMQCTP1", "TestType", "Type of Test", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCTP1", "InstrInf", "Instruction Info", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        AddTblFld("B1SLMQCTP1", "TestValu", "Desired Value", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCTP1", "TestUOM", "Testing UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCTP1", "SucesFrm", "Success From", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCTP1", "SucesTo", "Success To", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCTP1", "SucesAry", "Success Array", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        ValidValue(0).Valide_Value = "Y"
    '        ValidValue(0).Description = "Yes"
    '        ValidValue(1).Valide_Value = "N"
    '        ValidValue(1).Description = "No"
    '        AddTblFld("B1SLMQCTP1", "Relevant", "Relevant", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "N")
    '        ValidValue(0).Valide_Value = "Y"
    '        ValidValue(0).Description = "Yes"
    '        ValidValue(1).Valide_Value = "N"
    '        ValidValue(1).Description = "No"
    '        AddTblFld("B1SLMQCTP1", "Print", "Print", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "N")
    '        AddTblFld("B1SLMQCTP1", "TestTool", "TestTool", SAPbobsCOM.BoFieldTypes.db_Alpha, 50, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_None)
    '    End If

    '    '--------------QC Order Header 
    '    If addTbl("B1SLMQCORDR", "QC Order Header", SAPbobsCOM.BoUTBTableType.bott_Document) = 0 Then
    '        AddTblFld("B1SLMQCORDR", "InspPlan", "Insepection Plan ID", SAPbobsCOM.BoFieldTypes.db_Numeric, 0)
    '        AddTblFld("B1SLMQCORDR", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        AddTblFld("B1SLMQCORDR", "BatchID", "Batch ID", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR", "Qty", "Total Batch Qty", SAPbobsCOM.BoFieldTypes.db_Numeric, 0, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_Quantity)
    '        AddTblFld("B1SLMQCORDR", "WhsCode", "To Warehouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        ValidValue(0).Valide_Value = "Y"
    '        ValidValue(0).Description = "Yes"
    '        ValidValue(1).Valide_Value = "N"
    '        ValidValue(1).Description = "No"
    '        AddTblFld("B1SLMQCORDR", "SucesFrm", "Sucecss Flag", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR", "BaseType", "Trans Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR", "BaseEntr", "Trans Id", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR", "BaseRef", "Base Doc Num", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        AddTblFld("B1SLMQCORDR", "TransLin", "Trans Line", SAPbobsCOM.BoFieldTypes.db_Alpha, 5)
    '        ValidValue(0).Valide_Value = "Y"
    '        ValidValue(0).Description = "Yes"
    '        ValidValue(1).Valide_Value = "N"
    '        ValidValue(1).Description = "No"
    '        AddTblFld("B1SLMQCORDR", "Print", "Print", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "N")
    '        AddTblFld("B1SLMQCORDR", "PrintDt", "Print Date", SAPbobsCOM.BoFieldTypes.db_Date, 0)
    '        AddTblFld("B1SLMQCORDR", "Comments", "Comments", SAPbobsCOM.BoFieldTypes.db_Memo, 0)
    '    End If

    '    '--------------QC Order Test result by Sample  
    '    If addTbl("B1SLMQCORDR1", "QC Order Test result by Sample", SAPbobsCOM.BoUTBTableType.bott_DocumentLines) = 0 Then
    '        AddTblFld("B1SLMQCORDR1", "Code", "Test Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR1", "LineNum", "Line nUmber", SAPbobsCOM.BoFieldTypes.db_Numeric, 0)
    '        AddTblFld("B1SLMQCORDR1", "SmplNo", "Sample Line", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR1", "TTCODE", "Test Template ID", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR1", "TTDESC", "Test  Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        AddTblFld("B1SLMQCORDR1", "TestType", "Type of Test", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR1", "InstrInf", "Instruction Info", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        AddTblFld("B1SLMQCORDR1", "TestValu", "Desired Value", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR1", "TestUOM", "Testing UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR1", "SucesFrm", "Success From", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR1", "SucesTo", "Success To", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR1", "SucesAry", "Success Array", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        ValidValue(0).Valide_Value = "Y"
    '        ValidValue(0).Description = "Yes"
    '        ValidValue(1).Valide_Value = "N"
    '        ValidValue(1).Description = "No"
    '        AddTblFld("B1SLMQCORDR1", "Relevant", "Relevant", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "N")
    '        ValidValue(0).Valide_Value = "Y"
    '        ValidValue(0).Description = "Yes"
    '        ValidValue(1).Valide_Value = "N"
    '        ValidValue(1).Description = "No"
    '        AddTblFld("B1SLMQCORDR1", "Print", "Print", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "N")
    '        AddTblFld("B1SLMQCORDR1", "TestTool", "TestTool", SAPbobsCOM.BoFieldTypes.db_Alpha, 50, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_None)

    '        AddTblFld("B1SLMQCORDR1", "ResltVal", "Result Value", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        ValidValue(0).Valide_Value = "Y"
    '        ValidValue(0).Description = "Yes"
    '        ValidValue(1).Valide_Value = "N"
    '        ValidValue(1).Description = "No"
    '        AddTblFld("B1SLMQCORDR1", "ResltFlg", "ResultFlag", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR1", "ReasonCd", "ReasonCd", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR1", "ResltNte", "Result Note", SAPbobsCOM.BoFieldTypes.db_Memo, 0)
    '        AddTblFld("B1SLMQCORDR1", "TestDate", "Test Date", SAPbobsCOM.BoFieldTypes.db_Date, 0)
    '        AddTblFld("B1SLMQCORDR1", "TestBy", "Test By", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '    End If

    '    '--------------QC B1Stion  
    '    If addTbl("B1SLMQCORDR2", "QC Order Test result by Sample", SAPbobsCOM.BoUTBTableType.bott_DocumentLines) = 0 Then
    '        AddTblFld("B1SLMQCORDR2", "ActType", "B1AcStion Type", SAPbobsCOM.BoFieldTypes.db_Numeric, 0)
    '        AddTblFld("B1SLMQCORDR2", "Qty", "Quantity", SAPbobsCOM.BoFieldTypes.db_Numeric, 0, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_Quantity)
    '        AddTblFld("B1SLMQCORDR2", "ToWhs", "To Wharehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        AddTblFld("B1SLMQCORDR2", "ToItem", "To Item", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR2", "InvtrnNo", "Invtrn No", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        AddTblFld("B1SLMQCORDR2", "ActGINo", "Warehouse TransB1Stion DocEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        AddTblFld("B1SLMQCORDR2", "ActGRNo", "Goods Receipts", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    '        ValidValue(0).Valide_Value = "Y"
    '        ValidValue(0).Description = "Yes"
    '        ValidValue(1).Valide_Value = "N"
    '        ValidValue(1).Description = "No"
    '        AddTblFld("B1SLMQCORDR2", "DoneFlag", "Done Flag", SAPbobsCOM.BoFieldTypes.db_Alpha, 5)
    '        AddTblFld("B1SLMQCORDR2", "ActDate", "B1Stion Date", SAPbobsCOM.BoFieldTypes.db_Date, 0)
    '        AddTblFld("B1SLMQCORDR2", "ActionBy", "B1Stion By", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
    '        AddTblFld("B1SLMQCORDR2", "Comments", "Comments", SAPbobsCOM.BoFieldTypes.db_Memo, 0)
    '        ValidValue(0).Valide_Value = "Y"
    '        ValidValue(0).Description = "Yes"
    '        ValidValue(1).Valide_Value = "N"
    '        ValidValue(1).Description = "No"
    '        AddTblFld("B1SLMQCORDR2", "Print", "Print", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "N")
    '        AddTblFld("B1SLMQCORDR2", "TestTool", "TestTool", SAPbobsCOM.BoFieldTypes.db_Alpha, 50, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_None)
    '    End If
    '    '--------------OITM UDF
    '    AddTblFld("OITM", "B1SStdTP", "Inspection Plan", SAPbobsCOM.BoFieldTypes.db_Alpha, 50, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoFldSubTypes.st_None, "", "B1SLMQCRTT")
    'End Sub
    'Friend Sub CreateObject()
    '    Dim ChileTable(1) As String
    '    '--------------Test  Template UDO
    '    Call CreateUDO(SAPbobsCOM.BoUTBTableType.bott_MasterData, "B1SLMQCRTT", ChileTable, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoYesNoEnum.tYES, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoYesNoEnum.tYES, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoYesNoEnum.tYES, SAPbobsCOM.BoYesNoEnum.tYES)

    '    '--------------Test Plan Template UDO

    '    ChileTable(0) = "B1SLMQCTP1"
    '    Call CreateUDO(SAPbobsCOM.BoUTBTableType.bott_MasterData, "B1SLMQCTP", ChileTable, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoYesNoEnum.tYES, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoYesNoEnum.tYES, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoYesNoEnum.tYES, SAPbobsCOM.BoYesNoEnum.tYES)
    '    '--------------QC Order UDO
    '    'ReDim ChileTable(1)
    '    'ChileTable(0) = "B1SLMQCORDR1"
    '    'ChileTable(1) = "B1SLMQCORDR2"
    '    Call CreateUDO(SAPbobsCOM.BoUTBTableType.bott_Document, "B1SLMQCORDR", ChileTable, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoYesNoEnum.tYES, SAPbobsCOM.BoYesNoEnum.tYES, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoYesNoEnum.tNO, SAPbobsCOM.BoYesNoEnum.tYES, SAPbobsCOM.BoYesNoEnum.tYES)

    'End Sub
#End Region
End Class
