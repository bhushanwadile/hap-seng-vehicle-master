﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
namespace HapSengVehicleMaster
{
    class _B1SLogInfo : UIDesign 
    {
        #region Form Designing
            
                public void populateEditText(string Title, string[] label1, string[] dft_value, int no_of_Input)
                {
                    SAPbouiCOM.Form oForm = default(SAPbouiCOM.Form);
                    SAPbouiCOM.Item oitem = default(SAPbouiCOM.Item);
                    SAPbouiCOM.EditText oedit = default(SAPbouiCOM.EditText);
                    SAPbouiCOM.StaticText ostat = default(SAPbouiCOM.StaticText);
                    SAPbouiCOM.Button obutton = default(SAPbouiCOM.Button);
                                       
                    try
                    {
                        try
                        {
                            oForm = SboApp.Forms.Item("B1SLogInfo");
                            oForm.Close();
                            oForm = null;

                        }
                        catch (Exception ex)
                        {
                        }

                        oForm = FormCreation("B1SLogInfo", SAPbouiCOM.BoFormBorderStyle.fbs_Fixed, Title, 85 + no_of_Input * 25, 300);
                                               
                        oForm.Left = SboApp.Desktop.Width / 2 - oForm.Width / 2;
                        oForm.Top = SboApp.Desktop.Height / 2 - oForm.Height / 2 - 80;
                        //AddChooseFromList(oForm)
                        oForm.DataSources.UserDataSources.Add("SFTYPEDS", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 150);
                        oForm.DataSources.UserDataSources.Item("SFTYPEDS").Value="1";

                        for (int i = 0; i <= no_of_Input - 1; i++)
                        {

                            oedit =  (SAPbouiCOM .EditText ) AddFormItem(ref oForm, ("E_Edit" + i.ToString()), SAPbouiCOM.BoFormItemTypes.it_EDIT, 115, (10 + (i * 25)), 150, 19);

                            ostat = (SAPbouiCOM.StaticText)AddFormItem(ref oForm, ("S_Label" + i.ToString()), SAPbouiCOM.BoFormItemTypes.it_STATIC, 6, (10 + (i * 25)), 115, 15, "E_Edit" + i.ToString(), label1[i].ToString(), "", "", true, true, false, 0, false);


                            if ((label1[i].ToString().Contains("(D)") == true))
                            {
                                setitemds(oForm, "E_Edit" + i.ToString(), "Edit", SAPbouiCOM.BoDataType.dt_DATE, 200);
                            }
                            else if ((label1[i].ToString().Contains("(N)") == true))
                            {
                                setitemds(oForm, "E_Edit" + i.ToString(), "Edit", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, 20);
                            }
                            else if ((label1[i].ToString().Contains("(Q)") == true))
                            {
                                setitemds(oForm, "E_Edit" + i.ToString(), "Edit", SAPbouiCOM.BoDataType.dt_QUANTITY, 20);
                            }
                            else
                            {
                                setitemds(oForm, "E_Edit" + i.ToString(), "Edit", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200);
                            }
                            if (!string.IsNullOrEmpty(dft_value[i].ToString()))
                            {
                                try
                                {
                                    oForm.DataSources.UserDataSources.Item("E_Edit" + i.ToString() + "ds").Value = dft_value[i].ToString();

                                }
                                catch (Exception ex)
                                {
                                }
                            }
                            if ((label1[i].ToString().ToUpper().Contains("PASSWORD") == true | label1[i].ToString().ToUpper().Contains("PWD") == true))
                            {
                               // oedit = oitem.Specific;
                                oedit.IsPassword = true;
                            }
                        }


                        obutton = (SAPbouiCOM.Button)AddFormItem(ref oForm, "11", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 6, ((no_of_Input * 25) + 20), 65, 19,"","Ok");

                        obutton = (SAPbouiCOM.Button)AddFormItem(ref oForm, "2", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 155, ((no_of_Input * 25) + 20), 65, 19, "", "Cancel");

                        oForm.DefButton = "11";
                        oForm.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        oMsgShow.Messsage("Load Simple Form Exception: " + ex.Message, MsgShow.MsgType.Statusbar, MsgShow.MsgCat.Errors);
                    }
                }

#endregion

        #region "Events Handling"
                public void EventHandler(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
                {
                    try
                    {
                        
                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD & pVal.Before_Action == false)
                        {
                                                     
                        }
                        else if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_CLICK)
                        {
                            ClickEvent(ref pVal, ref BubbleEvent);
                        }                        
                    }
                    catch (Exception e)
                    {

                    }
                }
                public void ClickEvent(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
                {
                    try
                    {
                        

                        if (pVal.Before_Action == false  & pVal.ItemUID == "11")
                        {
                            
                            SAPbouiCOM .Form  oForm = SboApp.Forms.ActiveForm ;
                            String sqluser = oForm.Items.Item("E_Edit0").Specific.value;
                            String sqlpwd = oForm.Items.Item("E_Edit1").Specific.value;
                            if (sqluser.Trim() != "" & sqlpwd.Trim() != "")
                            {
                                connobj = new SqlConnection();
                                connobj.ConnectionString = "server=" + oCompany.Server.ToString() + ";uid=" + sqluser + ";pwd=" + sqlpwd + ";database=" + oCompany.CompanyDB.ToString();
                                connobj.Open();

                                SAPbobsCOM.Recordset oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                Sql = "Select * from sys.Tables where Name ='be1s_ConnInfo'";
                                  oRs.DoQuery(Sql);
                                if (oRs.RecordCount == 0)
                                {
                                    Sql = "CREATE TABLE [dbo].[be1s_ConnInfo]([SQLUser] [nvarchar](max) NOT NULL,	[SQLPwd] [nvarchar](max) NOT NULL) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";
                                    ADONonExeSql(Sql);
                                }
                                Sql = "Delete From be1s_ConnInfo";
                                ADONonExeSql(Sql);

                                Sql = "Insert into be1s_ConnInfo values('" + EncryptData(sqluser) + "','" + EncryptData(sqlpwd) + "')";
                                ADONonExeSql(Sql);

                                oForm.Close();
                            }
                            
                        }
                    }
                    catch (Exception e)
                    {
                        ADO_Connection();
                    }
                }
               
               

                #endregion
    }
}
