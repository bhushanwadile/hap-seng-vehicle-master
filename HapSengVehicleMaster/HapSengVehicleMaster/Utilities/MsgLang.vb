﻿Module MsgLang

    Enum MsgType
        PopUp
        Statusbar
    End Enum
    Enum MsgCat
        Errors
        Success
        Warning
    End Enum
    Enum Questiontype
        YN
        YNC
    End Enum
    Enum logActType
        BOMCalc
        UpdPrice
        MatRev
    End Enum

    Public Sub Messsage(msgtext As String, msgtype1 As MsgType)
        If enablePhase3 = False Then
            Select Case msgtype1
                Case MsgType.PopUp
                    obe1.SboApp.MessageBox(msgtext, 1, "OK", "", "")
                Case MsgType.Statusbar
                    obe1.SboApp.SetStatusBarMessage(msgtext, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            End Select
        Else
            Select Case msgtype1
                Case MsgType.PopUp
                    obe1.SboApp.MessageBox(LangCaption("msg", msgtext), 1, "OK", "", "")
                Case MsgType.Statusbar
                    obe1.SboApp.SetStatusBarMessage(LangCaption("msg", msgtext), SAPbouiCOM.BoMessageTime.bmt_Short, False)
            End Select
        End If
    End Sub
    Public Sub Messsage(msgtext As String, msgtype1 As MsgType, msgcat1 As MsgCat)
        Try
            Select Case msgtype1
                Case MsgType.PopUp
                    obe1.SboApp.MessageBox(msgtext, 1, "OK", "", "")
                Case MsgType.Statusbar
                    If msgcat1 = MsgCat.Errors Then
                        obe1.SboApp.StatusBar.SetText(msgtext, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    ElseIf msgcat1 = MsgCat.Success Then
                        obe1.SboApp.StatusBar.SetText(msgtext, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                    ElseIf msgcat1 = MsgCat.Warning Then
                        obe1.SboApp.StatusBar.SetText(msgtext, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    End If
            End Select
        Catch

        End Try
    End Sub
    Public Function QusMsg(msgtext As String, qtype As Questiontype) As Integer
        Select Case qtype
            Case Questiontype.YN
                Return obe1.SboApp.MessageBox(msgtext, 2, "Yes", "No", "")
            Case Questiontype.YNC
                Return obe1.SboApp.MessageBox(msgtext, 2, "Yes", "No", "Cancel")
        End Select

    End Function
    Public Function actiontypedesc(actiontype As logActType) As String
        Dim acttypedesc As String
        acttypedesc = ""
        Select Case actiontype
            Case logActType.UpdPrice
                acttypedesc = "UpdPrice"
            Case logActType.MatRev
                acttypedesc = "MatRev"
            Case logActType.BOMCalc
                acttypedesc = "BOMCalc"
        End Select
        Return acttypedesc
    End Function
    Public Sub recordlog(runid As String, Runid2 As String, itemcode As String, actiontype As logActType, result As String, resultcode As String, resultmsg As String)
        Dim orecord As SAPbobsCOM.Recordset

        Dim sqltext As String
        orecord = Main.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Try
            sqltext = "insert into b1s_calclog (RUNID, RUNID2, ItemCode, actiontype, result, resultCode, resultmsg, randate, ranuser) values (" &
                runid.ToString() & "," + Runid2.ToString() + ",'" & itemcode & "', '" & actiontypedesc(actiontype) & "','" & result.ToString() & "','" & resultcode & "','" & resultmsg.ToString() & "',getdate()," & Main.oCompany.UserSignature & ")"
            orecord.DoQuery(sqltext)
        Catch ex As Exception

        End Try
    End Sub
End Module
