﻿Public Class ShowDetails

    Private oForm As SAPbouiCOM.Form
    Private oBForm As SAPbouiCOM.Form
    Private oBTxt As SAPbouiCOM.EditTextColumn
    Private FormType As FormOpenType
    Private oBCell As SAPbouiCOM.Cell
    Private oBEditext As SAPbouiCOM.EditText
    Private MainSql As String
    Public SelectedCol As String

#Region "Form Definitions"
    Public Enum FormOpenType
        WithMatrixCell
        WithLargTextCell
        WithGridCell
        WithMatrixEditBox
        WithLargTextEditBox
        WithGridEditBox
    End Enum
    Public Sub New(BaseForm As SAPbouiCOM.Form, BaseTextBox As SAPbouiCOM.Cell, FormOpenType As FormOpenType, Optional Editable As Boolean = True, Optional SQL As String = "", Optional FormTital As String = "", Optional SelectionMode As SAPbouiCOM.BoMatrixSelect = SAPbouiCOM.BoMatrixSelect.ms_Single)
        MyBase.New()
        Try
            Dim oForm As SAPbouiCOM.Form
            oForm = obe1.SboApp.Forms.GetForm("B1SDtl", 0)
            oForm.Close()
        Catch ex As Exception

        End Try

        ChildBaseForm = BaseForm.TypeEx

        MainSql = SQL
        oBForm = BaseForm
        FormType = FormOpenType
        ChildOpen = 1
        If FormType = ShowDetails.FormOpenType.WithLargTextCell Or FormType = ShowDetails.FormOpenType.WithGridCell Or FormType = ShowDetails.FormOpenType.WithMatrixCell Then
            oBCell = BaseTextBox
        Else
            oBEditext = BaseTextBox
        End If

        If FormOpenType = ShowDetails.FormOpenType.WithLargTextCell Then
            CreateFormWithLargText(SQL, FormTital, Editable)
        End If
        If FormOpenType = ShowDetails.FormOpenType.WithMatrixCell Then
            CreateFormWithMatrix(SQL, FormTital, Editable)
        End If
        If FormOpenType = ShowDetails.FormOpenType.WithGridCell Then
            CreateFormWithGrid(SQL, FormTital, Editable, SelectionMode)
        End If
    End Sub
    Public Sub New(BaseForm As SAPbouiCOM.Form, BaseTextBox As SAPbouiCOM.EditText, FormOpenType As FormOpenType, Optional Editable As Boolean = True, Optional SQL As String = "", Optional FormTital As String = "", Optional SelectionMode As SAPbouiCOM.BoMatrixSelect = SAPbouiCOM.BoMatrixSelect.ms_Single)
        MyBase.New()
        Try
            Dim oForm As SAPbouiCOM.Form
            oForm = obe1.SboApp.Forms.GetForm("B1SDtl", 0)
            oForm.Close()
        Catch ex As Exception

        End Try
        MainSql = SQL
        oBForm = BaseForm
        FormType = FormOpenType

        If FormType = ShowDetails.FormOpenType.WithLargTextCell Or FormType = ShowDetails.FormOpenType.WithGridCell Or FormType = ShowDetails.FormOpenType.WithMatrixCell Then
            oBCell = BaseTextBox
        Else
            oBEditext = BaseTextBox
        End If

        If FormOpenType = ShowDetails.FormOpenType.WithLargTextCell Or FormOpenType = ShowDetails.FormOpenType.WithLargTextEditBox Then
            CreateFormWithLargText(SQL, FormTital, Editable)
        End If
        If FormOpenType = ShowDetails.FormOpenType.WithMatrixCell Or FormOpenType = ShowDetails.FormOpenType.WithMatrixEditBox Then
            CreateFormWithMatrix(SQL, FormTital, Editable)
        End If
        If FormOpenType = ShowDetails.FormOpenType.WithGridCell Or FormOpenType = ShowDetails.FormOpenType.WithGridEditBox Then
            CreateFormWithGrid(SQL, FormTital, Editable, SelectionMode)
        End If
    End Sub
    Public Sub CreateFormWithLargText(sql As String, FormTital As String, Optional Editable As Boolean = True)
        Dim oCreationParams As SAPbouiCOM.FormCreationParams
        Dim oTxt As SAPbouiCOM.EditText
        oCreationParams = obe1.SboApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)

        oCreationParams.FormType = "B1SDtl"
        oCreationParams.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed


        Try
            oForm = obe1.SboApp.Forms.AddEx(oCreationParams)

            oForm.Title = "Detail Info"
            oForm.Height = 296
            oForm.Width = 453

            If FormTital <> "" Then
                oForm.Title = FormTital
            Else
                oForm.Title = "Detail View"
            End If

            oForm.Left = obe1.SboApp.Desktop.Width / 2 - oForm.Width / 2
            oForm.Top = obe1.SboApp.Desktop.Height / 2 - oForm.Height / 2 - 80

            AddFormItem(oForm, "B1S_Btn1", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 5, 231, 65, 19, "", "Ok")
            AddFormItem(oForm, "2", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 75, 231, 65, 19)
            oForm.DefButton = "B1S_Btn1"

            oForm.DataSources.UserDataSources.Add("DtlText", SAPbouiCOM.BoDataType.dt_LONG_TEXT)
            oTxt = AddFormItem(oForm, "B1S_Txt1", SAPbouiCOM.BoFormItemTypes.it_EXTEDIT, 5, 5, 425, 215, , , "", "DtlText", Editable)

            If FormType = ShowDetails.FormOpenType.WithLargTextCell Then
                If oBCell.Specific.Value <> "" Then
                    oForm.DataSources.UserDataSources.Item("DtlText").Value = oBCell.Specific.Value
                End If
            Else
                If oBEditext.Value <> "" Then
                    oForm.DataSources.UserDataSources.Item("DtlText").Value = oBEditext.Value
                End If
            End If
            If FormTital <> "" Then
                oForm.Title = FormTital
            Else
                oForm.Title = "Detail View"
            End If



            oForm.Visible = True
        Catch ex As Exception
            obe1.SboApp.SetStatusBarMessage(ex.Message)
        End Try
    End Sub
    Public Sub CreateFormWithMatrix(sql As String, FormTital As String, Optional Editable As Boolean = True)
        Dim oCreationParams As SAPbouiCOM.FormCreationParams

        Dim oMatrix As SAPbouiCOM.Matrix
        Dim selecstring As String()
        Dim OrgVal As String
        oCreationParams = obe1.SboApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)

        oCreationParams.FormType = "B1SDtl"
        oCreationParams.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed


        Try
            oForm = obe1.SboApp.Forms.AddEx(oCreationParams)

            oForm.Title = "Detail Info"
            'oForm.Left = 300
            oForm.Height = 296
            oForm.Width = 453
            oForm.EnableMenu("1292", True)
            oForm.EnableMenu("1293", True)
            If FormTital <> "" Then
                oForm.Title = FormTital
            Else
                oForm.Title = "Detail View"
            End If

            oForm.Left = obe1.SboApp.Desktop.Width / 2 - oForm.Width / 2
            oForm.Top = obe1.SboApp.Desktop.Height / 2 - oForm.Height / 2 - 80


            AddFormItem(oForm, "B1S_Btn1", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 5, 231, 65, 19, "", "Ok")
            AddFormItem(oForm, "2", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 75, 231, 65, 19)
            oForm.DefButton = "B1S_Btn1"

            'oForm.DataSources.UserDataSources.Add("DtlText", SAPbouiCOM.BoDataType.dt_SHORT_TEXT)
            'AddFormItem(oForm, "txtSearch", SAPbouiCOM.BoFormItemTypes.it_EDIT, 78, 5, 161, 15, "", "", "", "DtlText", True)
            'AddFormItem(oForm, "lblSearch", SAPbouiCOM.BoFormItemTypes.it_STATIC, 5, 5, 72, 15, "txtSearch", "Find", "", "")

            oForm.DataSources.UserDataSources.Add("DtlText", SAPbouiCOM.BoDataType.dt_SHORT_TEXT)
            oForm.DataSources.UserDataSources.Add("SrNo", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER)
            oMatrix = AddFormItem(oForm, "B1S_Matrix", SAPbouiCOM.BoFormItemTypes.it_MATRIX, 5, 5, 425, 215)
            Call AddColumn(oMatrix, "#", SAPbouiCOM.BoFormItemTypes.it_EDIT, 20, "#", , "SrNo", False)
            Call AddColumn(oMatrix, "Data1", SAPbouiCOM.BoFormItemTypes.it_EDIT, 400, FormTital, , "DtlText", Editable, True)

            oMatrix.AddRow(50)

            If FormType = ShowDetails.FormOpenType.WithLargTextCell Or FormType = ShowDetails.FormOpenType.WithGridCell Or FormType = ShowDetails.FormOpenType.WithMatrixCell Then
                If oBCell.Specific.Value <> "" Then
                    OrgVal = oBCell.Specific.Value
                Else
                    OrgVal = ""
                End If
            Else
                If oBEditext.Value <> "" Then
                    OrgVal = oBEditext.Value
                Else
                    OrgVal = ""
                End If
            End If


            If OrgVal <> "" Then
                selecstring = OrgVal.Split(New Char() {","c})
                For i1 As Integer = 0 To selecstring.Count - 1
                    If selecstring(i1) <> "" Then
                        'oMatrix.AddRow()
                        oMatrix.Columns.Item("#").Cells.Item(i1 + 1).Specific.value = i1 + 1
                        oMatrix.Columns.Item("Data1").Cells.Item(i1 + 1).Specific.value = selecstring(i1).ToString()
                    End If
                Next
            End If

            oForm.Visible = True
        Catch ex As Exception
            obe1.SboApp.SetStatusBarMessage(ex.Message)
        End Try
    End Sub
    Public Sub CreateFormWithGrid(sql As String, FormTital As String, Optional Editable As Boolean = True, Optional SelectionMode As SAPbouiCOM.BoMatrixSelect = SAPbouiCOM.BoMatrixSelect.ms_Single)
        Dim oCreationParams As SAPbouiCOM.FormCreationParams

        Dim oGrid As SAPbouiCOM.Grid
        Dim selecstring As String()
        Dim OrgVal As String
        oCreationParams = obe1.SboApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)

        oCreationParams.FormType = "B1SDtl"
        oCreationParams.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed


        Try
            oForm = obe1.SboApp.Forms.AddEx(oCreationParams)

            oForm.Title = "Detail Info"
            'oForm.Left = 300
            oForm.Height = 296
            oForm.Width = 453
            oForm.EnableMenu("1292", True)
            oForm.EnableMenu("1293", True)
            If FormTital <> "" Then
                oForm.Title = FormTital
            Else
                oForm.Title = "Detail View"
            End If

            oForm.Left = obe1.SboApp.Desktop.Width / 2 - oForm.Width / 2
            oForm.Top = obe1.SboApp.Desktop.Height / 2 - oForm.Height / 2 - 80

            AddFormItem(oForm, "B1S_Btn1", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 5, 231, 65, 19, "", "Choose")
            AddFormItem(oForm, "2", SAPbouiCOM.BoFormItemTypes.it_BUTTON, 75, 231, 65, 19)
            oForm.DefButton = "B1S_Btn1"

            oForm.DataSources.UserDataSources.Add("DtlText", SAPbouiCOM.BoDataType.dt_SHORT_TEXT)
            AddFormItem(oForm, "txtSearch", SAPbouiCOM.BoFormItemTypes.it_EDIT, 78, 5, 161, 15, "", "", "", "DtlText", True)
            AddFormItem(oForm, "lblSearch", SAPbouiCOM.BoFormItemTypes.it_STATIC, 5, 5, 72, 15, "txtSearch", "Find", "", "")

            oGrid = AddFormItem(oForm, "B1S_Grid", SAPbouiCOM.BoFormItemTypes.it_GRID, 5, 27, 425, 198, "", "", "", "", Editable)


            If oForm.DataSources.DataTables.Count <> 0 Then
                oForm.DataSources.DataTables.Item("MydataTable").Clear()
            Else
                oForm.DataSources.DataTables.Add("MydataTable")
            End If


            oGrid.DataTable = oForm.DataSources.DataTables.Item("MydataTable")

            oForm.DataSources.DataTables.Item(0).ExecuteQuery(sql)
            oGrid.SelectionMode = SelectionMode

            If (oGrid.DataTable.Rows.Count < 100) Then
                For i As Integer = 0 To oGrid.DataTable.Rows.Count - 1
                    oGrid.RowHeaders.SetText(i, i + 1)
                Next
            End If

            For i As Integer = 0 To oGrid.DataTable.Columns.Count - 1
                If (i <> 0) Then
                    oGrid.Columns.Item(i).TitleObject.Sortable = True
                End If
                If Trim(oGrid.DataTable.Columns.Item(i).Name) = "" Then
                    oGrid.Columns.Item(i).Width = 0
                End If
            Next

            SelectedCol = oGrid.DataTable.Columns.Item(0).Name

            If FormType = ShowDetails.FormOpenType.WithLargTextCell Or FormType = ShowDetails.FormOpenType.WithGridCell Or FormType = ShowDetails.FormOpenType.WithMatrixCell Then
                If oBCell.Specific.Value <> "" Then
                    OrgVal = oBCell.Specific.Value
                Else
                    OrgVal = ""
                End If
            Else
                If oBEditext.Value <> "" Then
                    OrgVal = oBEditext.Value
                Else
                    OrgVal = ""
                End If
            End If


            If OrgVal <> "" Then
                selecstring = OrgVal.Split(New Char() {","c})
                If (oGrid.DataTable.Rows.Count < 100) Then
                    For i1 As Integer = 0 To oGrid.DataTable.Rows.Count - 1
                        If Array.BinarySearch(selecstring, oGrid.DataTable.Columns.Item(0).Cells.Item(i1).Value.ToString()) >= 0 Then
                            oGrid.Rows.SelectedRows.Add(i1)
                        End If
                    Next
                End If
            End If

            oForm.Visible = True
        Catch ex As Exception
            obe1.SboApp.SetStatusBarMessage(ex.Message)
        End Try
    End Sub
#End Region
#Region "Events Handling"
    Public Sub EventHandler(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.EventType = SAPbouiCOM.BoEventTypes.et_CLICK Then
            ItemClickEvent(pVal, BubbleEvent)
        ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK Then
            DoubleClickEvent(pVal, BubbleEvent)
        ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_KEY_DOWN Then
            KeyDownEvent(pVal, BubbleEvent)
        ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD Then
            FormUnloadEvent(pVal, BubbleEvent)
        ElseIf pVal.EventType = SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED Then
            ItemPressEvent(pVal, BubbleEvent)
        End If
    End Sub

    Sub ItemPressEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.ItemUID = "B1S_Grid" And pVal.BeforeAction = True And pVal.Row <= 0 And pVal.ColUID <> "RowsHeader" Then
            BubbleEvent = False
        End If
    End Sub
    Sub ItemClickEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Dim OutPut As String = ""


            If pVal.ItemUID = "B1S_Btn1" And pVal.Before_Action = False Then
                ChildOpen = 0
                oForm = obe1.SboApp.Forms.GetForm(pVal.FormTypeEx, pVal.FormTypeCount)
                If FormType = FormOpenType.WithLargTextCell Then
                    oBCell.Specific.value = oForm.Items.Item("B1S_Txt1").Specific.value
                End If
                If FormType = FormOpenType.WithMatrixCell Then
                    Dim oMatrix As SAPbouiCOM.Matrix
                    oMatrix = oForm.Items.Item("B1S_Matrix").Specific
                    For i As Integer = 0 To oMatrix.RowCount - 1
                        If oMatrix.Columns.Item("Data1").Cells.Item(i + 1).Specific.value <> "" Then
                            OutPut = OutPut & "," & oMatrix.Columns.Item("Data1").Cells.Item(i + 1).Specific.value
                        End If
                    Next
                    oBCell.Specific.value = Mid(OutPut, 2)
                End If
                If FormType = FormOpenType.WithGridCell Then
                    Dim oGrid As SAPbouiCOM.Grid
                    Dim rowno As Integer
                    oGrid = oForm.Items.Item("B1S_Grid").Specific
                    For i2 As Integer = 0 To oGrid.Rows.SelectedRows.Count - 1
                        rowno = oGrid.Rows.SelectedRows.Item(i2, SAPbouiCOM.BoOrderType.ot_RowOrder)
                        If oGrid.DataTable.Columns.Item(0).Cells.Item(rowno).Value.ToString() <> "" Then
                            If OutPut <> "" Then
                                OutPut = OutPut + "," + oGrid.DataTable.Columns.Item(0).Cells.Item(rowno).Value.ToString()
                            Else
                                OutPut = oGrid.DataTable.Columns.Item(0).Cells.Item(rowno).Value.ToString()
                            End If
                        End If

                    Next
                    oBCell.Specific.value = OutPut
                End If

                If FormType = FormOpenType.WithLargTextEditBox Then
                    oBEditext.Value = oForm.Items.Item("B1S_Txt1").Specific.value
                End If
                If FormType = FormOpenType.WithMatrixEditBox Then
                    Dim oMatrix As SAPbouiCOM.Matrix

                    oMatrix = oForm.Items.Item("B1S_Matrix").Specific
                    For i As Integer = 0 To oMatrix.RowCount - 1
                        If oMatrix.Columns.Item("Data1").Cells.Item(i + 1).Specific.value <> "" Then
                            OutPut = OutPut & "," & oMatrix.Columns.Item("Data1").Cells.Item(i + 1).Specific.value
                        End If
                    Next
                    oBEditext.Value = Mid(OutPut, 2)
                End If
                If FormType = FormOpenType.WithGridEditBox Then
                    Dim oGrid As SAPbouiCOM.Grid
                    Dim rowno As Integer
                    oGrid = oForm.Items.Item("B1S_Grid").Specific
                    For i2 As Integer = 0 To oGrid.Rows.SelectedRows.Count - 1
                        rowno = oGrid.Rows.SelectedRows.Item(i2, SAPbouiCOM.BoOrderType.ot_RowOrder)
                        If oGrid.DataTable.Columns.Item(0).Cells.Item(rowno).Value.ToString() <> "" Then
                            If OutPut <> "" Then
                                OutPut = OutPut + "," + oGrid.DataTable.Columns.Item(0).Cells.Item(rowno).Value.ToString()
                            Else
                                OutPut = oGrid.DataTable.Columns.Item(0).Cells.Item(rowno).Value.ToString()
                            End If
                        End If

                    Next
                    oBEditext.Value = OutPut
                End If

                oForm.Close()
            End If
            If pVal.ItemUID = "B1S_Grid" And pVal.BeforeAction = False Then
                Dim oGrid As SAPbouiCOM.Grid
                oGrid = oForm.Items.Item("B1S_Grid").Specific
                SelectedCol = pVal.ColUID
                oGrid.Rows.SelectedRows.Clear()
                oGrid.Rows.SelectedRows.Add(pVal.Row)
            End If
            If pVal.ItemUID = "B1S_Grid" And pVal.BeforeAction = True And pVal.Row < 0 And pVal.ColUID <> "RowsHeader" Then
                BubbleEvent = False
            End If

        Catch ex As Exception

        End Try
    End Sub
    Sub FormUnloadEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        ChildOpen = 0
        ChildBaseForm = ""
    End Sub
    Sub DoubleClickEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.ItemUID = "B1S_Grid" And pVal.BeforeAction = True And pVal.Row >= 0 Then
            BubbleEvent = False
            Dim oGrid As SAPbouiCOM.Grid
            oGrid = oForm.Items.Item("B1S_Grid").Specific
            oGrid.Rows.SelectedRows.Add(pVal.Row)
            oForm.Items.Item("B1S_Btn1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
        End If
        
    End Sub
    Sub KeyDownEvent(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.ItemUID = "txtSearch" And pVal.BeforeAction = False Then

            oForm = obe1.SboApp.Forms.ActiveForm
            Dim oEditText As SAPbouiCOM.EditText
            Dim oGrid As SAPbouiCOM.Grid
            Dim SelectCol As Integer = 0

            oEditText = oForm.Items.Item("txtSearch").Specific
            If FormType = FormOpenType.WithGridCell Or FormType = FormOpenType.WithGridEditBox Then

                oGrid = oForm.Items.Item("B1S_Grid").Specific

                If (oEditText.Value.Replace(" ", "") = "") Then
                    Sql = MainSql
                Else
                    If InStr(MainSql.ToUpper(), "WHERE") <> 0 Then
                        Sql = MainSql & " and " & oGrid.DataTable.Columns.Item(SelectedCol).Name & " like '" & oEditText.Value & "%'"
                    Else
                        Sql = MainSql & " Where " & oGrid.DataTable.Columns.Item(SelectedCol).Name & " like '" & oEditText.Value & "%'"
                    End If
                End If
                If oForm.DataSources.DataTables.Count <> 0 Then
                    oForm.DataSources.DataTables.Item("MydataTable").Clear()
                Else
                    oForm.DataSources.DataTables.Add("MydataTable")
                End If

                oGrid.DataTable = oForm.DataSources.DataTables.Item("MydataTable")
                oForm.DataSources.DataTables.Item(0).ExecuteQuery(Sql)
                oGrid.SetCellFocus(0, SelectedCol)
            End If
            If FormType = FormOpenType.WithMatrixCell Or FormType = FormOpenType.WithMatrixEditBox Then

            End If

        End If
    End Sub
#End Region
End Class
