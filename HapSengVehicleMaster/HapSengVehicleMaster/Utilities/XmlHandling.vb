﻿Module XmlHandling
    Friend Function GetFormXml(ByVal FormName As String) As Xml.XmlDocument
        Dim XmlResource As String
        Dim XmlStream As IO.Stream
        Dim XmlDoc As Xml.XmlDocument

        XmlResource = System.Reflection.Assembly.GetExecutingAssembly.GetName.Name & "." & FormName & ".xml"
        XmlStream = System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(XmlResource)

        XmlDoc = New Xml.XmlDocument
        XmlDoc.Load(XmlStream)

        Return XmlDoc
    End Function
    Friend Function GetXmlStream(ByVal FileName As String) As IO.Stream
        Dim XmlResource As String
        Dim XmlStream As IO.Stream

        XmlResource = System.Reflection.Assembly.GetExecutingAssembly.GetName.Name
        XmlResource = XmlResource & "." & FileName & ".xml"
        XmlStream = System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(XmlResource)

        Return XmlStream
    End Function
End Module
