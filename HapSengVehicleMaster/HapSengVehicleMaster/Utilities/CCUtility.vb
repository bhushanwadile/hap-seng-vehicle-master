﻿Imports System.Globalization
Imports System.Threading
Module CCUtility
    Enum gridtype
        YN
        show
        Choose
    End Enum
    Enum edittype
        SQLUSR = 1
    End Enum
    Enum selopt
        Itemprop = 1
        Dresult = 2
        ItemSelect = 3
    End Enum
    Public Function GetMenuID(TABLEID As String) As String
        Dim MenuId As String = ""
        Dim oMenus As SAPbouiCOM.Menus
        Dim oMenuItem As SAPbouiCOM.MenuItem
        oMenuItem = obe1.SboApp.Menus.Item("51200")
        oMenus = oMenuItem.SubMenus
        For i = 0 To oMenus.Count - 1
            If (oMenus.Item(i).String.Contains(TABLEID)) Then
                MenuId = oMenus.Item(i).UID
                Exit For
            End If
        Next
        Return MenuId
    End Function
    Public Sub gridcolumnlinkobj(ogrid As SAPbouiCOM.Grid, columnkey As Object, objtype As String)
        ' BPN 07/23/2014 
        'putting link type against any customized grid in the add-on
        ' Columnkey can both be integer specify column order number or the column name.
        ' you can also put a generic link button, and have a event action to pick up and execute something else.
        Try
            Dim ogridcol As SAPbouiCOM.EditTextColumn
            Dim resultkey As Integer
            If Integer.TryParse(columnkey, resultkey) Then
                ogridcol = ogrid.Columns.Item(columnkey)
            Else
                ogridcol = ogrid.Columns.Item(columnkey.ToString())
            End If
            ogridcol.LinkedObjectType = objtype
        Catch ex As Exception
            'MsgLang.Messsage("Link Arrow Gird Column " + ex.Message.ToString(), MsgType.Statusbar, MsgCat.Errors)
        End Try
    End Sub
    Public Sub FillCombo(fillcombo As SAPbouiCOM.ComboBox, sqltext As String, field1 As String, field2 As String)
        Try
            ' BPN 07/23/2014
            ' fill combo info by SQL statement. Combo will always have a empty empty as the first select to allow user select null value 
            Dim orecord As SAPbobsCOM.Recordset

            orecord = Main.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            orecord.DoQuery(sqltext)
            While (fillcombo.ValidValues.Count > 0)
                fillcombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
            End While
            fillcombo.ValidValues.Add("", "")
            For i As Integer = 0 To orecord.RecordCount - 1
                fillcombo.ValidValues.Add(orecord.Fields.Item(field1).Value.ToString(), orecord.Fields.Item(field2).Value.ToString())
                orecord.MoveNext()
            Next

        Catch ex As Exception

        End Try
    End Sub

    Public Sub FillCombo(fillcombo As SAPbouiCOM.ComboBox, sqltext As String, field1 As String, field2 As String, norefresh As Boolean)
        Try
            ' BPN 07/23/2014
            ' fill combo info by SQL statement. Combo will always have a empty empty as the first select to allow user select null value 
            If (norefresh = True And fillcombo.ValidValues.Count > 0) Then
                Exit Sub
            End If

            Dim orecord As SAPbobsCOM.Recordset

            orecord = Main.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            orecord.DoQuery(sqltext)
            While (fillcombo.ValidValues.Count > 0)
                fillcombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
            End While
            fillcombo.ValidValues.Add("", "")
            For i As Integer = 0 To orecord.RecordCount - 1
                fillcombo.ValidValues.Add(orecord.Fields.Item(field1).Value.ToString(), orecord.Fields.Item(field2).Value.ToString())
                orecord.MoveNext()
            Next

        Catch ex As Exception

        End Try
    End Sub

    Public Sub populatemulitselectbox(sqltext As String, oselopt As selopt, title As String, currsel As String, ogridtype As gridtype)
        Dim oForm1 As SAPbouiCOM.Form
        Dim oItem As SAPbouiCOM.Item
        Dim oButton As SAPbouiCOM.Button
        Dim oGrid As SAPbouiCOM.Grid
        Dim selecstring As String()
        Dim rowsel As Integer
        Try
            Dim oCreationParam As SAPbouiCOM.FormCreationParams
            oCreationParam = obe1.SboApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)
            Select Case oselopt
                Case selopt.Itemprop
                    oCreationParam.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed
                Case selopt.Dresult
                    oCreationParam.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Sizable
                Case selopt.ItemSelect
                    oCreationParam.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Sizable
            End Select
            oCreationParam.UniqueID = "BE1SGG"
            Try
                oForm1 = obe1.SboApp.Forms.Item("BE1SGG")
                oForm1.Close()
            Catch ex As Exception

            End Try

            oForm1 = obe1.SboApp.Forms.AddEx(oCreationParam)
            oForm1.DataSources.DataTables.Add("SFG_G")

            oForm1.DataSources.UserDataSources.Add("SFTYPEDS", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, 1)
            oForm1.DataSources.UserDataSources.Item("SFTYPEDS").Value = oselopt

            oForm1.Title = title

            Select Case oselopt
                Case selopt.Itemprop
                    oForm1.Height = 350
                    oForm1.Width = 425
                Case selopt.Dresult
                    oForm1.Height = 550
                    oForm1.Width = 1200
                Case selopt.ItemSelect
                    oForm1.Height = 550
                    oForm1.Width = 600
            End Select
            oForm1.Left = obe1.SboApp.Desktop.Width / 2 - oForm1.Width / 2
            oForm1.Top = obe1.SboApp.Desktop.Height / 2 - oForm1.Height / 2 - 80

            oItem = oForm1.Items.Add("B_YES", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 6
            oItem.Width = 65
            Select Case oselopt
                Case selopt.Itemprop
                    oItem.Top = 275
                Case selopt.Dresult
                    oItem.Top = 495
                Case selopt.ItemSelect
                    oItem.Top = 495
            End Select

            oItem.Height = 19

            oButton = oItem.Specific
            If (ogridtype = gridtype.YN) Then
                oButton.Caption = "Yes"
            ElseIf ogridtype = gridtype.show Then
                oButton.Caption = "Ok"
            ElseIf ogridtype = gridtype.Choose Then
                oButton.Caption = "Choose"
            End If


            oItem = oForm1.Items.Add("2", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 80
            oItem.Width = 65
            Select Case oselopt
                Case selopt.Itemprop
                    oItem.Top = 275
                Case selopt.Dresult
                    oItem.Top = 495
                Case selopt.ItemSelect
                    oItem.Top = 495
            End Select
            oItem.Height = 19

            oButton = oItem.Specific

            If (ogridtype = gridtype.YN) Then
                oButton.Caption = "No"
            ElseIf ogridtype = gridtype.show Or ogridtype = gridtype.Choose Then
                oButton.Caption = "Cancel"
            End If

            oItem = oForm1.Items.Add("G_Grid", SAPbouiCOM.BoFormItemTypes.it_GRID)
            oItem.Left = 10
            Select Case oselopt
                Case selopt.Itemprop
                    oItem.Enabled = True
                    oItem.Width = 350
                    oItem.Height = 245
                Case selopt.Dresult
                    oItem.Width = 1175
                    oItem.Height = 475
                Case selopt.ItemSelect
                    oItem.Width = 575
                    oItem.Height = 475
            End Select
            oItem.Top = 15

            oGrid = oItem.Specific

            oGrid.DataTable = oForm1.DataSources.DataTables.Item("SFG_G")
            oGrid.DataTable.ExecuteQuery(sqltext)
            oGrid.AutoResizeColumns()
            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto

            If (oselopt = selopt.Itemprop) Then
                oForm1.Items.Item("G_Grid").Enabled = True
                oGrid.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox
                oGrid.Columns.Item(1).Editable = False
                oGrid.Columns.Item(2).Editable = False
            Else
                oForm1.Items.Item("G_Grid").Enabled = False
            End If



            If oselopt = selopt.Itemprop Then

                Try
                    If currsel <> "" Then
                        selecstring = currsel.Split(New Char() {","c})
                        For i1 As Integer = 0 To selecstring.Count - 1
                            If selecstring(i1) <> "" Then
                                rowsel = -1
                                For i2 As Integer = 0 To oGrid.DataTable.Rows.Count - 1
                                    If (oGrid.DataTable.Columns.Item(1).Cells.Item(i2).Value.ToString() = selecstring(i1).ToString()) Then
                                        rowsel = i2
                                        Exit For
                                    End If
                                Next
                                If rowsel > -1 Then
                                    'oGrid.Rows.SelectedRows.Add(rowsel)
                                    oGrid.DataTable.Columns.Item("Choose").Cells.Item(rowsel).Value = "Y"
                                End If
                            End If
                        Next
                    End If
                Catch ex As Exception
                    MsgLang.Messsage("Grid Form reselect existing Exception: " + ex.Message.ToString(), MsgType.Statusbar, MsgCat.Errors)
                End Try
            End If
            If oselopt = selopt.Dresult Then
                gridcolumnlinkobj(oGrid, "FGCODE", "4")
                gridcolumnlinkobj(oGrid, "ItemCode", "4")
                gridcolumnlinkobj(oGrid, "WhsCode", "64")
                gridcolumnlinkobj(oGrid, "FatherCode", "4")
            ElseIf oselopt = selopt.ItemSelect Then
                gridcolumnlinkobj(oGrid, "ItemCode", "4")
                gridcolumnlinkobj(oGrid, "WhsCode", "64")
            End If

            'grid column right adjustify and resize grid column for itemname
            Try
                For c1 As Integer = 0 To oGrid.Columns.Count - 1
                    If oGrid.Columns.Item(c1).TitleObject.Caption.ToUpper.Contains("COST") Or oGrid.Columns.Item(c1).TitleObject.Caption.Contains("ROURCE") Or oGrid.Columns.Item(c1).TitleObject.Caption.Contains("PRICE") Then
                        oGrid.Columns.Item(c1).RightJustified = True
                    End If
                    If (oGrid.Columns.Item(c1).TitleObject.Caption.ToUpper = "ITEMNAME") Then
                        oGrid.Columns.Item(c1).Width = 100
                    End If
                    oGrid.Columns.Item(c1).TitleObject.Sortable = True
                Next
            Catch
            End Try

            oForm1.Visible = True
        Catch ex As Exception
            MsgLang.Messsage("Grid Form create Exception: " + ex.Message.ToString(), MsgType.Statusbar, MsgCat.Errors)
        End Try
    End Sub

    Public Sub populateEditText(Title As String, label1 As String(), dft_value As String(), no_of_Input As Integer, otype As edittype)
        Dim oForm As SAPbouiCOM.Form
        Dim oitem As SAPbouiCOM.Item
        Dim oedit As SAPbouiCOM.EditText
        Dim ostat As SAPbouiCOM.StaticText
        Dim obutton As SAPbouiCOM.Button

        Dim oCreationParams As SAPbouiCOM.FormCreationParams



        Try
            oCreationParams = obe1.SboApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)


            oCreationParams.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed
            oCreationParams.UniqueID = "BE1LMSF"

            Try
                oForm = obe1.SboApp.Forms.Item("BE1LMSF")
                oForm.Close()
                oForm = Nothing
            Catch ex As Exception

            End Try

            oForm = obe1.SboApp.Forms.AddEx(oCreationParams)



            oForm.Title = Title
            oForm.Height = 85 + no_of_Input * 25
            oForm.ClientWidth = 275
            oForm.Left = obe1.SboApp.Desktop.Width / 2 - oForm.Width / 2
            oForm.Top = obe1.SboApp.Desktop.Height / 2 - oForm.Height / 2 - 80
            'AddChooseFromList(oForm)
            oForm.DataSources.UserDataSources.Add("SFTYPEDS", SAPbouiCOM.BoDataType.dt_LONG_TEXT, 150)
            oForm.DataSources.UserDataSources.Item("SFTYPEDS").Value = otype.ToString()

            For i As Integer = 0 To no_of_Input - 1
                oitem = oForm.Items.Add("S_Label" + i.ToString(), SAPbouiCOM.BoFormItemTypes.it_STATIC)
                oitem.Left = 6
                oitem.Width = 115
                oitem.Top = 10 + i * 25
                oitem.Height = 15

                ostat = oitem.Specific
                ostat.Caption = label1(i).ToString()

                oitem = oForm.Items.Add("E_Edit" + i.ToString(), SAPbouiCOM.BoFormItemTypes.it_EDIT)
                oitem.Left = 115
                oitem.Width = 150
                oitem.Top = 10 + i * 25
                oitem.Height = 19

                If (label1(i).ToString().Contains("(D)") = True) Then

                    setitemds(oForm, "E_Edit" + i.ToString(), "Edit", SAPbouiCOM.BoDataType.dt_DATE, 200)

                ElseIf (label1(i).ToString().Contains("(N)") = True) Then

                    setitemds(oForm, "E_Edit" + i.ToString(), "Edit", SAPbouiCOM.BoDataType.dt_SHORT_NUMBER, 20)

                ElseIf (label1(i).ToString().Contains("(Q)") = True) Then

                    setitemds(oForm, "E_Edit" + i.ToString(), "Edit", SAPbouiCOM.BoDataType.dt_QUANTITY, 20)

                Else

                    setitemds(oForm, "E_Edit" + i.ToString(), "Edit", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 200)
                End If
                If dft_value(i).ToString() <> "" Then
                    Try
                        oForm.DataSources.UserDataSources.Item("E_Edit" + i.ToString() + "ds").Value = dft_value(i).ToString()
                    Catch ex As Exception

                    End Try
                End If
                If (label1(i).ToString().ToUpper().Contains("PASSWORD") = True Or label1(i).ToString().ToUpper().Contains("PWD") = True) Then
                    oedit = oitem.Specific
                    oedit.IsPassword = True
                End If
            Next

            oitem = oForm.Items.Add("1", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oitem.Left = 6
            oitem.Width = 65
            oitem.Top = no_of_Input * 25 + 20
            oitem.Height = 19

            obutton = ((oitem.Specific))

            obutton.Caption = "Ok"

            oitem = oForm.Items.Add("2", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oitem.Left = 155
            oitem.Width = 65
            oitem.Top = no_of_Input * 25 + 20
            oitem.Height = 19

            obutton = ((oitem.Specific))

            obutton.Caption = "Cancel"

            oForm.Visible = True
        Catch ex As Exception
            MsgLang.Messsage("Load Simple Form Exception: " + ex.Message, MsgType.Statusbar, MsgCat.Errors)
        End Try
    End Sub

    Public Sub sortmatrixandselect(rowid As Integer)
        Dim oForm As SAPbouiCOM.Form
        Dim omatrix As SAPbouiCOM.Matrix
        ' When data load sort matrix by 
        Try
            oForm = obe1.SboApp.Forms.Item("BE1SCCMn")
            omatrix = oForm.Items.Item("M_Main").Specific
            omatrix.Columns.Item("C_0_1").TitleObject.Sort(SAPbouiCOM.BoGridSortType.gst_Descending)
            If omatrix.RowCount >= rowid Then
                omatrix.Columns.Item(1).Cells.Item(1).Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                omatrix.SelectRow(rowid, True, False)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Function decimalreturn(inputno As String) As String
        If (inputno.Contains(",") = True) Then
            Return inputno.Replace(",", SepDec)
        ElseIf (inputno.Contains(".") = True) Then
            Return inputno.Replace(".", SepDec)
        Else
            Return inputno
        End If
    End Function
    Public Function decimalreturn(inputno As String, userfield As Boolean) As String
        'Dim output As String = inputno.ToString(CultureInfo.InvariantCulture)

        If (inputno.Contains(",") = True) Then
            Return inputno.Replace(",", ".")
        Else
            Return inputno.ToString()
        End If
    End Function
    Public Function decimalreturn1(inputno As Double) As String

        Return inputno.ToString(CultureInfo.InvariantCulture)
    End Function

    Public Function GetDecimalTextValue(inputno As String) As Double
        Try
            Dim qty As Double = System.Convert.ToDouble(inputno, CultureInfo.InvariantCulture)
            'Double.TryParse(inputno, NumberStyles.Number, CultureInfo.CurrentCulture, qty)
            Return qty
        Catch ex As Exception
            Return 0
        End Try

    End Function

    Public Sub setitemds(oForm As SAPbouiCOM.Form, itemcode As String, objtype As String, otype As SAPbouiCOM.BoDataType, size As Integer)

        Dim oedit As SAPbouiCOM.EditText
        Dim oOptbtn As SAPbouiCOM.OptionBtn
        Dim ocombo As SAPbouiCOM.ComboBox
        Dim ocheck As SAPbouiCOM.CheckBox
        Dim oCbutton As SAPbouiCOM.ButtonCombo
        Select Case (objtype)

            Case "Edit"
                oForm.DataSources.UserDataSources.Add(itemcode + "ds", otype, size)
                oedit = (oForm.Items.Item(itemcode).Specific)
                oedit.DataBind.SetBound(True, "", itemcode + "ds")

            Case "Optbtn"
                oForm.DataSources.UserDataSources.Add(itemcode + "ds", otype, size)
                oOptbtn = (oForm.Items.Item(itemcode).Specific)
                oOptbtn.DataBind.SetBound(True, "", itemcode + "ds")
            Case "Combo"
                oForm.DataSources.UserDataSources.Add(itemcode + "ds", otype, size)
                ocombo = (oForm.Items.Item(itemcode).Specific)
                ocombo.DataBind.SetBound(True, "", itemcode + "ds")

            Case "Check"
                oForm.DataSources.UserDataSources.Add(itemcode + "ds", otype, size)
                ocheck = (oForm.Items.Item(itemcode).Specific)
                ocheck.DataBind.SetBound(True, "", itemcode + "ds")

            Case "ComboButton"
                oForm.DataSources.UserDataSources.Add(itemcode + "ds", otype, size)
                oCbutton = (oForm.Items.Item(itemcode).Specific)
                oCbutton.DataBind.SetBound(True, "", itemcode + "ds")

        End Select

    End Sub


    Public Sub formcloseset(oForm As SAPbouiCOM.Form)
        Dim item As SAPbouiCOM.Item
        Dim ogrid As SAPbouiCOM.Grid
        Dim size, left, top, width, height As Integer
        Dim orecord As SAPbobsCOM.Recordset
        orecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Dim userid As Integer = oCompany.UserSignature
        Dim sqltext As String
        Try

            If (oForm.UniqueID = "BE1SCCMn") Or (oForm.TypeEx = "QCORDER") Or (oForm.TypeEx = "INSPTT") Or (oForm.UniqueID = "BatchQCOrdr") Then

                'Record size and position
                size = oForm.State
                If (size = 3) Then
                    width = oForm.Width
                    height = oForm.Height
                    If height > 650 Then
                        height = 650
                    End If
                Else
                    width = 0
                    height = 0
                End If
                left = oForm.Left
                top = oForm.Top

                Try
                    sqltext = "exec b1S_UpdateCloseForm 'FORMSIZE', " + userid.ToString() + ", '" + oForm.UniqueID.ToString() + "', " + size.ToString() + ", " + left.ToString() + ", " + top.ToString() + ", " + width.ToString() + ", " + height.ToString() + ", null, null, null"
                    orecord.DoQuery(sqltext)
                Catch
                End Try
                'Record Grid
                For Each item In oForm.Items
                    If item.Type = SAPbouiCOM.BoFormItemTypes.it_GRID Then
                        ogrid = item.Specific
                        For i As Integer = 0 To ogrid.Columns.Count
                            Try
                                sqltext = "exec b1S_UpdateCloseForm 'COLSIZE', " + userid.ToString() + ", '" + oForm.UniqueID.ToString() + "', null, null, null, null, null '" + item.UniqueID.ToString() + "', '" + ogrid.Columns.Item(i).UniqueID.ToString() + "', " + ogrid.Columns.Item(i).Width.ToString()
                                orecord.DoQuery(sqltext)
                            Catch

                            End Try
                        Next
                    End If
                Next
            End If
        Catch ex As Exception
            MsgLang.Messsage("Form Close Setting Exception!", MsgType.Statusbar, MsgCat.Errors)
        End Try
    End Sub

    Public Sub resetformload(oform As SAPbouiCOM.Form)
        Dim orecord As SAPbobsCOM.Recordset
        Try

            orecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim sqltext As String = "select * from b1s_LMCFG1 where userid = 1 and FORMUID = '" + oform.UniqueID.ToString() + "'"
            orecord.DoQuery(sqltext)
            If (orecord.RecordCount > 0) Then
                oform.State = System.Convert.ToInt16(orecord.Fields.Item("StartSize").Value.ToString())
                If (System.Convert.ToInt16(orecord.Fields.Item("StartSize").Value.ToString()) = 3) Then
                    oform.Left = System.Convert.ToInt16(orecord.Fields.Item("StartLeft").Value.ToString())
                    oform.Top = System.Convert.ToInt16(orecord.Fields.Item("StartTop").Value.ToString())
                    oform.Width = System.Convert.ToInt16(orecord.Fields.Item("StartWidth").Value.ToString())
                    oform.Height = System.Convert.ToInt16(orecord.Fields.Item("StartHeight").Value.ToString())
                    If oform.UniqueID = "BE1SCCMn" Then
                        oform.Items.Item("G_Result").Width = System.Convert.ToInt16(orecord.Fields.Item("StartWidth").Value.ToString()) - 45
                        oform.Items.Item("M_Main").Width = System.Convert.ToInt16(orecord.Fields.Item("StartWidth").Value.ToString()) - 45
                        oform.Items.Item("M_Main").Height = 100
                    End If
                    If oform.TypeEx = "INSPTT" Then
                        oform.Items.Item("0_U_G").Height = oform.Height - oform.Items.Item("0_U_G").Top - 90
                        oform.Items.Item("0_U_G").Width = oform.Width - oform.Items.Item("0_U_G").Left - 80
                        oform.Items.Item("1").Top = oform.Items.Item("0_U_G").Top + oform.Items.Item("0_U_G").Height + 15
                        oform.Items.Item("2").Top = oform.Items.Item("0_U_G").Top + oform.Items.Item("0_U_G").Height + 15
                        oform.Items.Item("12").Left = oform.Items.Item("0_U_G").Left + oform.Items.Item("0_U_G").Width + 5
                        oform.Items.Item("11").Left = oform.Items.Item("0_U_G").Left + oform.Items.Item("0_U_G").Width + 5
                        oform.Items.Item("12").Top = (oform.Items.Item("0_U_G").Height / 2) ' - (oform.Items.Item("12").Height) ' - 10)
                        oform.Items.Item("11").Top = (oform.Items.Item("0_U_G").Height / 2) + 40
                    End If
                    If oform.TypeEx = "QCORDER" Then
                        Dim Dist As Integer
                        oform.Items.Item("0_U_G").Height = oform.Height - oform.Items.Item("0_U_G").Top - oform.Items.Item("30_U_E").Height - 90
                        oform.Items.Item("0_U_G").Width = oform.Width - oform.Items.Item("0_U_G").Left - 40

                        oform.Items.Item("56").Height = oform.Height - oform.Items.Item("56").Top - oform.Items.Item("30_U_E").Height - 90
                        oform.Items.Item("56").Width = oform.Width - oform.Items.Item("56").Left - 40

                        oform.Items.Item("30_U_E").Top = oform.Items.Item("0_U_G").Top + oform.Items.Item("0_U_G").Height + 5
                        oform.Items.Item("30_U_S").Top = oform.Items.Item("0_U_G").Top + oform.Items.Item("0_U_G").Height + 5

                        oform.Items.Item("1").Top = oform.Items.Item("30_U_E").Top + oform.Items.Item("30_U_E").Height + 10
                        oform.Items.Item("2").Top = oform.Items.Item("30_U_E").Top + oform.Items.Item("30_U_E").Height + 10
                        oform.Items.Item("46").Top = oform.Items.Item("30_U_E").Top + oform.Items.Item("30_U_E").Height + 10
                        oform.Items.Item("45").Top = oform.Items.Item("30_U_E").Top + oform.Items.Item("30_U_E").Height + 10

                        oform.Items.Item("53").Top = oform.Items.Item("30_U_E").Top + oform.Items.Item("30_U_E").Height + 10
                        oform.Items.Item("53").Left = oform.Items.Item("0_U_G").Left + oform.Items.Item("0_U_G").Width - oform.Items.Item("53").Width

                        oform.Items.Item("1_U_E").Left = oform.Items.Item("0_U_G").Left + oform.Items.Item("0_U_G").Width - oform.Items.Item("1_U_E").Width
                        oform.Items.Item("17_U_E").Left = oform.Items.Item("0_U_G").Left + oform.Items.Item("0_U_G").Width - oform.Items.Item("17_U_E").Width
                        oform.Items.Item("29_U_E").Left = oform.Items.Item("0_U_G").Left + oform.Items.Item("0_U_G").Width - oform.Items.Item("29_U_E").Width
                        oform.Items.Item("31_U_E").Left = oform.Items.Item("0_U_G").Left + oform.Items.Item("0_U_G").Width - oform.Items.Item("31_U_E").Width
                        oform.Items.Item("32_U_Cb").Left = oform.Items.Item("0_U_G").Left + oform.Items.Item("0_U_G").Width - oform.Items.Item("32_U_Cb").Width
                        oform.Items.Item("23_U_Cb").Left = oform.Items.Item("0_U_G").Left + oform.Items.Item("0_U_G").Width - oform.Items.Item("23_U_Cb").Width

                        Dist = oform.Items.Item("54").Left + oform.Items.Item("54").Width - oform.Items.Item("55").Left
                        oform.Items.Item("1_U_S").Left = oform.Items.Item("1_U_E").Left - Dist - oform.Items.Item("1_U_S").Width
                        oform.Items.Item("17_U_S").Left = oform.Items.Item("1_U_S").Left
                        oform.Items.Item("50").Left = oform.Items.Item("17_U_E").Left - oform.Items.Item("50").Width
                        oform.Items.Item("29_U_S").Left = oform.Items.Item("29_U_E").Left - Dist - oform.Items.Item("29_U_S").Width
                        oform.Items.Item("31_U_S").Left = oform.Items.Item("31_U_E").Left - Dist - oform.Items.Item("31_U_S").Width
                        oform.Items.Item("32_U_S").Left = oform.Items.Item("32_U_Cb").Left - Dist - oform.Items.Item("32_U_S").Width
                        oform.Items.Item("23_U_S").Left = oform.Items.Item("23_U_Cb").Left - Dist - oform.Items.Item("23_U_S").Width
                        oform.Items.Item("1000002").Left = oform.Items.Item("23_U_Cb").Left - Dist - oform.Items.Item("23_U_S").Width
                    End If
                End If
            End If
        Catch ex As Exception
            MsgLang.Messsage("Restore Form State Exception: " + ex.Message(), MsgType.Statusbar, MsgCat.Errors)
            Try
                orecord.DoQuery("delete from b1s_LMCFG1 where userid = " + oCompany.UserSignature.ToString() + " and FORMUID = '" + oform.UniqueID.ToString() + "'")
            Catch

            End Try
        End Try
    End Sub

    Public Sub resetgridcolumn(oGrid As SAPbouiCOM.Grid, FormUID As String, MItem As String)
        Dim orecord As SAPbobsCOM.Recordset
        Dim sqltext As String = "select * from b1s_LMCFG2 where userid  =1 and FORMUID = '" + FormUID + "' and MITEm = '" + MItem + "'"
        orecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Try
            orecord.DoQuery(sqltext)
            If (orecord.RecordCount > 0) Then
                For i As Integer = 0 To orecord.RecordCount - 1
                    Try
                        oGrid.Columns.Item(orecord.Fields.Item("ColumnID").Value.ToString()).Width = System.Convert.ToInt16(orecord.Fields.Item("Columnsize").Value.ToString())
                    Catch
                    End Try
                Next
            End If
        Catch ex As Exception
            MsgLang.Messsage("Restore Grid column width Exception: " + ex.Message(), MsgType.Statusbar, MsgCat.Errors)
            Try
                orecord.DoQuery("delete from b1s_LMCFG2 where userid = " + oCompany.UserSignature.ToString() + " and FORMUID = '" + FormUID.ToString() + "' and MItem = '" + MItem.ToString() + "'")
            Catch

            End Try
        End Try
    End Sub
    Public Sub init_getuserpermission()
        Try
            Dim QCcount, CCcount, BOMcount As Integer
            QCcount = 0
            CCcount = 0
            BOMcount = 0
            'initlize
            enableQC = False
            enableBOMCALC = False
            enableCC = False
            enableCFG = False
            enablePOClose = False
            enableQCOrdReopen = False
            Dim oUser As SAPbobsCOM.Users = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUsers)
            oUser.GetByKey(oCompany.UserSignature)
            If (oUser.Superuser = SAPbobsCOM.BoYesNoEnum.tYES) Then
                enableQC = True
                enableBOMCALC = True
                enableCC = True
                enableCFG = True
                enablePOClose = True
                enableQCOrdReopen = True
            Else

                For i As Integer = 0 To oUser.UserPermission.Count - 1
                    oUser.UserPermission.SetCurrentLine(i)
                    If (oUser.UserPermission.PermissionID = "LMQC") Then
                        QCcount = 1
                        If (oUser.UserPermission.Permission = SAPbobsCOM.BoPermission.boper_Full Or UserGroupAuthorization(oUser.InternalKey, "LMQC") = True) Then
                            enableQC = True
                        Else
                            enableQC = False
                        End If
                    ElseIf (oUser.UserPermission.PermissionID = "LMBomCalc") Then
                        BOMcount = 1
                        If (oUser.UserPermission.Permission = SAPbobsCOM.BoPermission.boper_Full Or UserGroupAuthorization(oUser.InternalKey, "LMBomCalc") = True) Then
                            enableBOMCALC = True
                        Else
                            enableBOMCALC = False
                        End If
                    ElseIf (oUser.UserPermission.PermissionID = "LMCC") Then
                        CCcount = 1
                        If (oUser.UserPermission.Permission = SAPbobsCOM.BoPermission.boper_Full Or UserGroupAuthorization(oUser.InternalKey, "LMCC") = True) Then
                            enableCC = True
                        Else
                            enableCC = False
                        End If
                    ElseIf (oUser.UserPermission.PermissionID = "LMCfg") Then
                        CCcount = 1
                        If (oUser.UserPermission.Permission = SAPbobsCOM.BoPermission.boper_Full Or UserGroupAuthorization(oUser.InternalKey, "LMCfg") = True) Then
                            enableCFG = True
                        Else
                            enableCFG = False
                        End If
                    ElseIf (oUser.UserPermission.PermissionID = "LMQCRO") Then
                        If (oUser.UserPermission.Permission = SAPbobsCOM.BoPermission.boper_Full Or UserGroupAuthorization(oUser.InternalKey, "LMQCRO") = True) Then
                            enableQCOrdReopen = True
                        Else
                            enableQCOrdReopen = False
                        End If
                    ElseIf (oUser.UserPermission.PermissionID = "LMPOCLS") Then
                        CCcount = 1
                        If (oUser.UserPermission.Permission = SAPbobsCOM.BoPermission.boper_Full Or UserGroupAuthorization(oUser.InternalKey, "LMPOCLS") = True) Then
                            enablePOClose = True
                        Else
                            enablePOClose = False
                        End If

                    End If
                Next
            End If
        Catch Ex As Exception
            MsgLang.Messsage("Get User Permission Exception : " + Ex.Message.ToString(), MsgType.Statusbar, MsgCat.Errors)
        End Try
    End Sub
    Public Sub init_getcompanydetail()
        Dim oRecordset As SAPbobsCOM.Recordset
        Dim Query As String
        oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        'BPN '
        Try
            'Check cost by warehouse flag

            Query = "select top 1 priceSys, DecSep, PriceDec, QtyDec, SumDec, RateDec ,ThousSep, MainCurncy from OADM"

            oRecordset.DoQuery(Query)
            If oRecordset.RecordCount <= 0 Then
                Exit Sub
            End If

            If (oRecordset.Fields.Item("priceSys").Value.ToString() = "Y") Then
                costbywhsflag = True
            Else
                costbywhsflag = False
            End If

            If (oRecordset.Fields.Item("DecSep").Value.ToString() <> "") Then
                SepDec = oRecordset.Fields.Item("DecSep").Value.ToString().TrimEnd()
            End If
            If (oRecordset.Fields.Item("ThousSep").Value.ToString() <> "") Then
                ThousSep = oRecordset.Fields.Item("ThousSep").Value.ToString().TrimEnd()
            End If
            If (oRecordset.Fields.Item("PriceDec").Value.ToString() <> "") Then
                Pricedec = oRecordset.Fields.Item("PriceDec").Value.ToString().TrimEnd()
            End If
            If (oRecordset.Fields.Item("QtyDec").Value.ToString() <> "") Then
                QtyDec = oRecordset.Fields.Item("QtyDec").Value.ToString().TrimEnd()
            End If
            If (oRecordset.Fields.Item("SumDec").Value.ToString() <> "") Then
                SumDec = oRecordset.Fields.Item("SumDec").Value.ToString().TrimEnd()
            End If
            If (oRecordset.Fields.Item("RateDec").Value.ToString() <> "") Then
                RateDec = oRecordset.Fields.Item("RateDec").Value.ToString().TrimEnd()
            End If
            If (oRecordset.Fields.Item("MainCurncy").Value.ToString() <> "") Then
                MainCurr = oRecordset.Fields.Item("MainCurncy").Value.ToString().TrimEnd()
            End If
            Language = oCompany.language

            prvider = New Globalization.NumberFormatInfo
            prvider.NumberDecimalSeparator = SepDec
            prvider.NumberGroupSeparator = ThousSep


            Query = "select * from b1s_LMCFG99 where isnull(VALUE10, '') = '1' "

            oRecordset.DoQuery(Query)
            If oRecordset.RecordCount <= 0 Then
                Main.enableBOMCALCButton = False
                Main.enableProd1 = False
                Main.enableQCALL = False
                Main.enableCCALL = False
                Main.enableBCALL = False
                UseAutoPrice = False
                Main.enableExtrOprt = False
                Exit Sub
            End If

            If (oRecordset.RecordCount > 0) Then
                For i As Int16 = 1 To oRecordset.RecordCount
                    Select Case oRecordset.Fields.Item(0).Value.ToString()
                        Case "ENBCALL"
                            If (oRecordset.Fields.Item(1).Value.ToString() = "N") Then
                                Main.enableBCALL = True
                            Else
                                Main.enableBCALL = False
                            End If
                        Case "ENBOMCAL"
                            If (oRecordset.Fields.Item(1).Value.ToString() = "Y") Then
                                Main.enableBOMCALCButton = True
                            Else
                                Main.enableBOMCALCButton = False
                            End If
                        Case "ENCCALL"
                            If (oRecordset.Fields.Item(1).Value.ToString() = "N") Then
                                Main.enableCCALL = True
                            Else
                                Main.enableCCALL = False
                            End If
                        Case "ENPRODALT"
                            If (oRecordset.Fields.Item(1).Value.ToString() = "Y") Then
                                Main.enableProd1 = True
                            Else
                                Main.enableProd1 = False
                            End If
                        Case "ENQCALL"
                            If (oRecordset.Fields.Item(1).Value.ToString() = "N") Then
                                Main.enableQCALL = True
                            Else
                                Main.enableQCALL = False
                            End If
                        Case "ENSTDALL"
                            If (oRecordset.Fields.Item(1).Value.ToString() = "Y") Then
                                Main.UseAutoPrice = True
                            Else
                                Main.UseAutoPrice = False
                            End If
                        Case "ENBTCHISS"
                            If (oRecordset.Fields.Item(1).Value.ToString() = "Y") Then
                                Main.enableBtchIss = True
                            Else
                                Main.enableBtchIss = False
                            End If
                        Case "ENEXTROPRS"
                            If (oRecordset.Fields.Item(1).Value.ToString() = "Y") Then
                                Main.enableExtrOprt = True
                            Else
                                Main.enableExtrOprt = False
                            End If
                    End Select
                    oRecordset.MoveNext()
                Next
            End If
        Catch ex As Exception
            MsgLang.Messsage("Init: " & ex.Message, MsgType.Statusbar, MsgCat.Errors)
        End Try
        'Setting 99

        Try
            Query = "select * from b1s_LMCFG99 where isnull(VALUE10, '') = ''"
            oRecordset.DoQuery(Query)
            If (oRecordset.RecordCount > 0) Then
                For i As Int16 = 1 To oRecordset.RecordCount
                    Select Case oRecordset.Fields.Item(0).Value.ToString()
                        Case "ENABLEPULP"
                            If (oRecordset.Fields.Item(1).Value.ToString() = "Y") Then
                                Main.enablePulpType = True
                            Else
                                Main.enablePulpType = False
                            End If
                        Case "ENBCOSTSET"
                            If (oRecordset.Fields.Item(1).Value.ToString() = "Y") Then
                                Main.enableCostSet = True
                            Else
                                Main.enableCostSet = False
                            End If
                        Case "ENABLEXFUP"
                            If (oRecordset.Fields.Item(1).Value.ToString() = "Y") Then
                                Main.enableEXFUP = True
                            Else
                                Main.enableEXFUP = False
                            End If
                    End Select
                    oRecordset.MoveNext()
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub linktobatch(batchid As String)
        Dim LineNo As Integer
        Try

            obe1.SboApp.ActivateMenuItem("12290")
            LineNo = 1
            'Dim batchform As SAPbouiCOM.Form = obe1.SboApp.Forms.ActiveForm
            Dim batchform As SAPbouiCOM.Form = obe1.SboApp.Forms.GetForm("65053", -1)
            LineNo = 2

            If batchform.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                LineNo = 3
                batchform.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                LineNo = 4
            End If
            Dim editext As SAPbouiCOM.EditText = batchform.Items.Item("62").Specific
            LineNo = 5
            editext.Value = batchid.ToString()
            LineNo = 6
            batchform.Items.Item("37").Click()
            LineNo = 7
        Catch ex As Exception

            MsgLang.Messsage("Line No: " + Trim(LineNo) + " Active Batch Exception : " + ex.Message.ToString(), MsgType.Statusbar, MsgCat.Errors)
        Finally

        End Try
    End Sub

    Public Sub refreshUDO(ByRef newUDOkey As String, oForm As SAPbouiCOM.Form, ItemUID As String)
        Dim oedit As SAPbouiCOM.EditText
        Try
            If (newUDOkey <> "") Then
                oForm.Freeze(True)
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                oedit = oForm.Items.Item(ItemUID).Specific
                oedit.Value = newUDOkey
                oForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                oForm.Freeze(False)
                newUDOkey = ""
            End If
        Catch ex As Exception
            MsgLang.Messsage("reload new added obj failed: " + ex.Message.ToString(), MsgType.Statusbar, MsgCat.Errors)
            oForm.Freeze(False)
        End Try
    End Sub
End Module
