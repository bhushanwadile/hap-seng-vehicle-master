﻿Module FormUtilities
    Public Function AddFormItem(ByRef oForm As SAPbouiCOM.Form, ByVal ItemUid As String, ByVal ItemType As SAPbouiCOM.BoFormItemTypes, _
                        ByVal Left As Integer, ByVal Top As Integer, ByVal Width As Integer, ByVal Height As Integer, _
                        Optional ByVal LinkTo As String = "", Optional ByVal Caption As String = "", _
                        Optional ByVal TableName As String = "", Optional ByVal FieldName As String = "", _
                        Optional ByVal Enabled As Boolean = True, Optional ByVal Visible As Boolean = True, _
                        Optional ByVal DispDesc As Boolean = True, Optional ByVal PaneLevel As Integer = 0, _
                        Optional ByVal RightJust As Boolean = False) As Object
        Dim oItem As SAPbouiCOM.Item

        Try
            oItem = oForm.Items.Add(ItemUid, ItemType)
            oItem.Left = Left
            oItem.Width = Width
            oItem.Height = Height
            oItem.Top = Top

            If LinkTo <> "" Then
                oItem.LinkTo = LinkTo
            End If

            oItem.Enabled = Enabled
            oItem.Visible = Visible
            oItem.DisplayDesc = DispDesc

            If TableName <> "" Or FieldName <> "" Then
                oItem.Specific.DataBind.SetBound(True, TableName, FieldName)
            End If

            If Caption <> "" Then
                oItem.Specific.Caption = Caption
            End If

            If ItemType = SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX Then
                Dim oCheck As SAPbouiCOM.CheckBox = oItem.Specific

                oCheck.Caption = Caption
                oCheck.ValOff = "N"
                oCheck.ValOn = "Y"
            End If

            If ItemType = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                Dim oCombo As SAPbouiCOM.ComboBox = oItem.Specific

                oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
            End If

            If ItemType = SAPbouiCOM.BoFormItemTypes.it_MATRIX Then
                Dim oMatrix As SAPbouiCOM.Matrix = oItem.Specific

                oMatrix.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single
            End If

            'If PaneLevel <> 0 Then
            oItem.FromPane = PaneLevel
            oItem.ToPane = PaneLevel
            'End If

            If RightJust Then
                oItem.RightJustified = True
            End If

            Return oItem.Specific
        Catch Ex As Exception
            'obe1.SboApp.SetStatusBarMessage("AddItem: " & Ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return -1
        End Try
    End Function
    Public Function AddColumn(ByRef oMatrix As SAPbouiCOM.Matrix, ByVal ColId As String, ByVal ColType As SAPbouiCOM.BoFormItemTypes, _
                                    ByVal Width As Integer, ByVal Caption As String, _
                                    Optional ByVal Table As String = "", Optional ByVal Field As String = "", _
                                    Optional ByVal Editable As Boolean = True, Optional ByVal Visible As Boolean = True, _
                                    Optional ByVal RightJustified As Boolean = False) As Object
        Dim oColumn As SAPbouiCOM.Column

        Try
            oColumn = oMatrix.Columns.Add(ColId, ColType)
            oColumn.Width = Width
            oColumn.TitleObject.Caption = Caption

            If Field <> "" Then
                oColumn.DataBind.SetBound(True, Table, Field)
            End If

            oColumn.Editable = Editable
            oColumn.Visible = Visible

            If ColType = SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX Then
                oColumn.ValOff = "N"
                oColumn.ValOn = "Y"
            End If

            If ColType = SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX Then
                oColumn.DisplayDesc = True
                oColumn.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
            End If

            If RightJustified Then
                oColumn.RightJustified = True
            End If

            Return oColumn
        Catch ex As Exception
            Return -1
        End Try
    End Function


End Module
