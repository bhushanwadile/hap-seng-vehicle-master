﻿Imports System.Data.XmlReadMode
Imports System.Xml
Imports System.Data

Module LangConvertor
    Public oXMLDoc As XmlDocument
    Dim ItemCount As Integer
    Dim CaptionCount As Integer
    Dim X, Y As Integer
    Dim FormType, FormName As String
    Sub loadLangform(filename As String)
        oXMLDoc = New XmlDocument
        oXMLDoc.Load(System.Windows.Forms.Application.StartupPath & "\" & filename)
        UpdateXML()
        obe1.SboApp.LoadBatchActions(oXMLDoc.InnerXml)
    End Sub
    Function getcaptioncount() As Integer
        CaptionCount = CaptionCount + 1
        Return CaptionCount
    End Function
    Sub UpdateXML()
        For Each node As XmlNode In oXMLDoc.DocumentElement.SelectNodes("*")
            Dim StartNodePathe As String
            StartNodePathe = node.Name
            node = node.ParentNode
            StartNodePathe = node.Name + "/" + StartNodePathe
            ItemCount = 1
            CaptionCount = 0
            Rec(StartNodePathe)
        Next
    End Sub
    Sub Rec(nodepath As String, Optional uid As String = "")
        Try
            Dim node, node1, PNode As XmlNode
            Dim lstNodes As XmlNodeList = oXMLDoc.SelectNodes(nodepath)
            Dim ItemList() As String = {"8", "4", "121", "122", "99", "129", "127"}
            Dim CapNo As Integer
            node = oXMLDoc.SelectSingleNode(nodepath)

            If node.ChildNodes.Count > 0 Then
                For i As Integer = 0 To node.ChildNodes.Count - 1
                    node1 = node.ChildNodes.Item(i)
                    Dim ooo = node1.NodeType
                    Dim Body = oXMLDoc.OuterXml
                    If UCase(node1.Name) = "FORM" Then
                        FormType = node1.Attributes("FormType").Value
                        node1.Attributes("title").Value = LangCaption(FormType, node1.Attributes("title").Value)
                    End If
                    If UCase(node1.Name) = "SPECIFIC" Then
                        If node1.ParentNode.Name = "item" Then
                            PNode = node1.ParentNode
                            If ItemList.Contains(PNode.Attributes("type").Value) = True Then
                                If PNode.Attributes("type").Value = 127 Then
                                    Dim nods As XmlNodeList = oXMLDoc.DocumentElement.SelectNodes("/" & nodepath & "/" & node1.Name & "/columns/action/column")
                                    For Each x As XmlNode In nods
                                        x.Attributes("title").Value = LangCaption(FormType, x.Attributes("title").Value)
                                    Next
                                    ItemCount = ItemCount + 1
                                Else
                                    'MsgBox(PNode.Attributes("uid").Value & "-----------" & node1.Name & "------" & node1.Attributes("caption").Value)
                                    If PNode.Attributes("type").Value = 4 And (PNode.Attributes("uid").Value = "1" Or PNode.Attributes("uid").Value = "2") Then
                                        ItemCount = ItemCount + 1
                                    Else
                                        node1.Attributes("caption").Value = LangCaption(FormType, node1.Attributes("caption").Value)
                                        ItemCount = ItemCount + 1
                                        Exit For
                                    End If
                                End If
                            Else
                                ItemCount = ItemCount + 1
                            End If
                        End If
                    End If
                    If node1.ChildNodes.Count > 0 Then
                        If UCase(node1.Name) = "ITEM" Then
                            Rec(nodepath & "/" & node1.Name & "[" & ItemCount & "]")
                        Else
                            Rec(nodepath & "/" & node1.Name)
                        End If
                    End If
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub
    Function LangCaption(FormId As String, Caption As String)
        Dim substr As String
        Dim CapNo As String
        Dim StartIndex, EndIndex As Integer
        Dim i As Integer = 0
        'For i As Integer = 0 To Caption.Length
        Do

            StartIndex = Caption.IndexOf("[", i) + 2
            EndIndex = Caption.IndexOf("]", i) - 1
            If StartIndex > 0 Then

                CapNo = Mid(Caption, StartIndex, Caption.IndexOf("|", StartIndex) - StartIndex)
                Dim oRs As SAPbobsCOM.Recordset
                Sql = "select * from [B1SFormItemCaption] where FormId='" & FormId & "' and CaptionNo='" & CapNo & "' and LangCode='EN'"
                'Sql = "select * from [B1SFormItemCaption] where  CaptionNo='" & CapNo & "' and LangCode='EN'"
                oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                oRs.DoQuery(Sql)


                If oRs.RecordCount > 0 Then
                    substr = Trim(Mid(Caption, StartIndex - 1, EndIndex + 2 - StartIndex + 2))
                    Caption = Caption.Substring(0, StartIndex - 2) & Trim(oRs.Fields.Item("Caption").Value) & Mid(Caption, EndIndex + 3)

                    'Caption = Trim(oRs.Fields.Item("Caption").Value) & Mid(Caption, EndIndex + 3)
                    i = EndIndex + (Len(Trim(oRs.Fields.Item("Caption").Value)) - Len(substr))
                Else
                    substr = Trim(Mid(Caption, StartIndex - 1, EndIndex + 2 - StartIndex + 2))

                    Dim ReplaceStr As String = Trim(Replace(Mid(substr, substr.IndexOf("|") + 2, Len(substr) - 1), "]", ""))
                    Caption = Caption.Substring(0, StartIndex - 2) & ReplaceStr & Mid(Caption, EndIndex + 3)
                    'Caption = ReplaceStr & Mid(Caption, EndIndex + 3)
                    i = EndIndex + (Len(ReplaceStr) - Len(substr))
                End If
            End If
            Sql = Mid(Caption, EndIndex + 2)
            'MsgBox(Sql.IndexOf("["))
            'If Sql.IndexOf("[") < 0 Then
            '    Return Caption
            'End If

        Loop Until (Sql.IndexOf("[") < 0)

        Return Caption
    End Function
    Function LangCaption1(FormId As String, Caption As String)
        Dim oRs As SAPbobsCOM.Recordset
        Dim i = Mid(Caption, 1, Caption.IndexOf("|") - 1)
        Sql = "select * from [B1SFormItemCaption] where FormId='" & FormId & "' and CaptionNo='" & Mid(Caption, Caption.IndexOf("[") + 2, Caption.IndexOf("|") - 1) & "' and LangCode='EN'"
        oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRs.DoQuery(Sql)

        If oRs.RecordCount > 0 Then
            Return oRs.Fields.Item("Caption").Value
        Else
            Return Mid(Caption, Caption.IndexOf("|") + 2, Caption.IndexOf("]") - 1)
        End If
    End Function

End Module
