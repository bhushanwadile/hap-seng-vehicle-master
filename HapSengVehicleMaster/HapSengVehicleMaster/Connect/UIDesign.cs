﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HapSengVehicleMaster
{
   public class UIDesign :be1_UIConnection
    {
        #region Menu Creation
        public void AddMenu(string Parent_MenuId, SAPbouiCOM.BoMenuType MenuType, string MenuId, int Position, string Caption,  string ImgName = "", bool Enable =true)
        {
            //Dim formCmdCenter As SAPbouiCOM.Form
            //formCmdCenter = SboApp.Forms.GetFormByTypeAndCount(169, 1)

            SAPbouiCOM.Menus oMenus = default(SAPbouiCOM.Menus);

            //Dim oMenus1 As SAPbouiCOM.Menus
            SAPbouiCOM.MenuItem oMenuItem = default(SAPbouiCOM.MenuItem);
            //Dim oMenuItem1 As SAPbouiCOM.MenuItem
            oMenus = SboApp.Menus;
            if ((oMenus.Exists(MenuId.ToString()) == true))
            {
                return;
            }

            SAPbouiCOM.MenuCreationParams oCreationPB1Skage = default(SAPbouiCOM.MenuCreationParams);
            oCreationPB1Skage = SboApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
            try
            {
                oMenuItem = SboApp.Menus.Item(Parent_MenuId);
                //Modules'
                oMenus = oMenuItem.SubMenus;


                //--------------------- Tab            
                if (MenuType == SAPbouiCOM.BoMenuType.mt_SEPERATOR)
                {
                    oCreationPB1Skage.Type = SAPbouiCOM.BoMenuType.mt_SEPERATOR;
                    oCreationPB1Skage.UniqueID = MenuId;
                    oCreationPB1Skage.Position = oMenuItem.SubMenus.Count + 1;
                    //Position '
                    oCreationPB1Skage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                    oCreationPB1Skage.String = Caption;
                     
                    if (!string.IsNullOrEmpty(ImgName))
                    {
                        //oCreationPB1Skage.Image = "C:\Users\Onkar\Desktop\" & ImgName
                        oCreationPB1Skage.Image = System.Windows.Forms.Application.StartupPath + "\\" + ImgName;
                    }
                    oMenus.AddEx(oCreationPB1Skage);
                    
                }
                //---------------------------------------------------
                //--------------------------- Folder
                if (MenuType == SAPbouiCOM.BoMenuType.mt_POPUP)
                {
                    oCreationPB1Skage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                    oCreationPB1Skage.UniqueID = MenuId;
                    oCreationPB1Skage.String = Caption;
                    if (Position == -1)
                    {
                        oCreationPB1Skage.Position = oMenuItem.SubMenus.Count + 1;
                    }
                    else
                    {
                        oCreationPB1Skage.Position = Position;
                    }
                    oMenus.AddEx(oCreationPB1Skage);
                }
                //----------------------------------------
                //--------------------------- Menu
                if (MenuType == SAPbouiCOM.BoMenuType.mt_STRING)
                {
                    oCreationPB1Skage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                    oCreationPB1Skage.UniqueID = MenuId;
                    oCreationPB1Skage.String = Caption;
                    oCreationPB1Skage.Enabled = Enable;
                    if (Position == -1)
                    {
                        oCreationPB1Skage.Position = oMenuItem.SubMenus.Count + 1;
                    }
                    else
                    {
                        oCreationPB1Skage.Position = Position;
                    }
                    oMenus.AddEx(oCreationPB1Skage);
                }
                
                return;
            }
            catch (Exception ex)
            {
            }

        }
        #endregion
        #region FormCreations
        public SAPbouiCOM.Form FormCreation(string FormTypeEx,SAPbouiCOM.BoFormBorderStyle BorderStyle,string FormTitle,int FormHeight,int FormWidth)
        { 
            SAPbouiCOM.FormCreationParams oCreationParams;
            SAPbouiCOM.Form oForm ;//=new SAPbouiCOM.Form() ;
            try
            {
                oCreationParams = SboApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams);

                oCreationParams.FormType = FormTypeEx;
                oCreationParams.BorderStyle = BorderStyle;


                oForm = SboApp.Forms.AddEx(oCreationParams);

                oForm.Title = FormTitle ;
                oForm.Height = FormHeight;
                oForm.Width = FormWidth;
                
                oForm.Left = SboApp.Desktop.Width / 2 - oForm.Width / 2;
                oForm.Top = SboApp.Desktop.Height / 2 - oForm.Height / 2 - 80;
            }
            catch (Exception ex)
            {
                return null;
            }
            return oForm;
        }
        #endregion
        #region FormItemDesign
            public object AddFormItem(ref SAPbouiCOM.Form oForm, string ItemUid, SAPbouiCOM.BoFormItemTypes ItemType, int Left, int Top, int Width, int Height, string LinkTo = "", string Caption = "", string TableName = "",
            string FieldName = "", bool Enabled = true, bool Visible = true, bool DispDesc = true, int PaneLevel = 0, bool RightJust = false)
            {
                SAPbouiCOM.Item oItem ;

                try
                {
                    oItem = oForm.Items.Add(ItemUid, ItemType);
                    oItem.Left = Left;
                    oItem.Width = Width;
                    oItem.Height = Height;
                    oItem.Top = Top;

                    if (!string.IsNullOrEmpty(LinkTo))
                    {
                        oItem.LinkTo = LinkTo;
                    }

                    oItem.Enabled = Enabled;
                    oItem.Visible = Visible;
                    oItem.DisplayDesc = DispDesc;

                    if (!string.IsNullOrEmpty(TableName) | !string.IsNullOrEmpty(FieldName))
                    {
                        oItem.Specific.DataBind.SetBound(true, TableName, FieldName);
                    }

                    if (!string.IsNullOrEmpty(Caption))
                    {
                        oItem.Specific.Caption = Caption;
                    }

                    if (ItemType == SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
                    {
                        SAPbouiCOM.CheckBox oCheck = oItem.Specific;

                        oCheck.Caption = Caption;
                        oCheck.ValOff = "N";
                        oCheck.ValOn = "Y";
                    }

                    if (ItemType == SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
                    {
                        SAPbouiCOM.ComboBox oCombo = oItem.Specific;

                        oCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                    }

                    if (ItemType == SAPbouiCOM.BoFormItemTypes.it_MATRIX)
                    {
                        SAPbouiCOM.Matrix oMatrix = oItem.Specific;

                        oMatrix.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single;
                    }

                    //If PaneLevel <> 0 Then
                    oItem.FromPane = PaneLevel;
                    oItem.ToPane = PaneLevel;
                    //End If

                    if (RightJust)
                    {
                        oItem.RightJustified = true;
                    }

                    return oItem.Specific;
                }
                catch (Exception Ex)
                {
                    //obe1.SboApp.SetStatusBarMessage("AddItem: " & Ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    return -1;
                }
            }

            public SAPbouiCOM.Column   AddColumn(ref SAPbouiCOM.Matrix oMatrix, string ColId, SAPbouiCOM.BoFormItemTypes ColType, int Width, string Caption, string Table = "", string Field = "", bool Editable = true, bool Visible = true, bool RightJustified = false)
        {
            SAPbouiCOM.Column oColumn = default(SAPbouiCOM.Column);

            try
            {
                oColumn = oMatrix.Columns.Add(ColId, ColType);
                oColumn.Width = Width;
                oColumn.TitleObject.Caption = Caption;

                if (!string.IsNullOrEmpty(Field))
                {
                    oColumn.DataBind.SetBound(true, Table, Field);
                }

                oColumn.Editable = Editable;
                oColumn.Visible = Visible;

                if (ColType == SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
                {
                    oColumn.ValOff = "N";
                    oColumn.ValOn = "Y";
                }

                if (ColType == SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
                {
                    oColumn.DisplayDesc = true;
                    oColumn.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                }

                if (RightJustified)
                {
                    oColumn.RightJustified = true;
                }

                return oColumn;
            }
            catch (Exception ex)
            {
                return null ;
            }
        }
        #endregion
        #region ChooseFromList
                public SAPbouiCOM.ChooseFromList AddChooseFromList(SAPbouiCOM.Form oForm, string CflId, string ObjType)
                {
                    SAPbouiCOM.ChooseFromListCreationParams oChooseListParams = default(SAPbouiCOM.ChooseFromListCreationParams);
                    SAPbouiCOM.ChooseFromListCollection oChooseListCollection = default(SAPbouiCOM.ChooseFromListCollection);
                    SAPbouiCOM.ChooseFromList a=null;
                    //a = new SAPbouiCOM.ChooseFromList();
                    try
                    {
                        oChooseListCollection = oForm.ChooseFromLists;
                        oChooseListParams = SboApp.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams);

                        oChooseListParams.MultiSelection = false;
                        oChooseListParams.ObjectType = ObjType;
                        oChooseListParams.UniqueID = CflId;

                        return oChooseListCollection.Add(oChooseListParams);
                    }
                    catch (Exception Ex)
                    {
                        //obe1sUi.SboApp.SetStatusBarMessage("AdCfl: " & Ex.Message)
                        return a;
                    }
                }
                
                public SAPbouiCOM.Conditions BuildCond(string CondAlias, string CondVal)
                {
                    SAPbouiCOM.Conditions oConditions = default(SAPbouiCOM.Conditions);
                    SAPbouiCOM.Condition oCondition = default(SAPbouiCOM.Condition);

                    oConditions = new SAPbouiCOM.Conditions();
                    oCondition = oConditions.Add();

                    oCondition.Alias = CondAlias;
                    oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                    oCondition.CondVal = CondVal;

                    return oConditions;
                }
                
                public SAPbouiCOM.Conditions BuildCond(string CondAlias, string CondVal, string CondAlias2, string CondVal2, SAPbouiCOM.BoConditionRelationship CondRel = SAPbouiCOM.BoConditionRelationship.cr_AND)
                {
                    SAPbouiCOM.Conditions oConditions = default(SAPbouiCOM.Conditions);
                    SAPbouiCOM.Condition oCondition = default(SAPbouiCOM.Condition);

                    oConditions = new SAPbouiCOM.Conditions();
                    oCondition = oConditions.Add();

                    oCondition.Alias = CondAlias;
                    oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                    oCondition.CondVal = CondVal;
                    oCondition.Relationship = CondRel;

                    oCondition = oConditions.Add();
                    oCondition.Alias = CondAlias2;
                    oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                    oCondition.CondVal = CondVal2;

                    return oConditions;
                }
                
                public SAPbouiCOM.Conditions BuildCond(string CondAlias, string CondVal, string CondAlias2, string CondVal2, string CondAlias3, string CondVal3, SAPbouiCOM.BoConditionRelationship CondRel = SAPbouiCOM.BoConditionRelationship.cr_AND, SAPbouiCOM.BoConditionRelationship CondRel2 = SAPbouiCOM.BoConditionRelationship.cr_AND)
                {
                    SAPbouiCOM.Conditions oConditions = default(SAPbouiCOM.Conditions);
                    SAPbouiCOM.Condition oCondition = default(SAPbouiCOM.Condition);

                    oConditions = new SAPbouiCOM.Conditions();
                    oCondition = oConditions.Add();

                    oCondition.Alias = CondAlias;
                    oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                    oCondition.CondVal = CondVal;
                    oCondition.Relationship = CondRel;

                    oCondition = oConditions.Add();
                    oCondition.Alias = CondAlias2;
                    oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                    oCondition.CondVal = CondVal2;
                    oCondition.Relationship = CondRel2;

                    oCondition = oConditions.Add();
                    oCondition.Alias = CondAlias3;
                    oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                    oCondition.CondVal = CondVal3;

                    return oConditions;
                }
                
                public void AddFormatSurch(string FormID, string ColId, string Query_Name, string Query_Catgory, string Sql_Query, 
                string ItemID, SAPbobsCOM.BoFormattedSearchActionEnum Action, SAPbobsCOM.BoYesNoEnum Refresh = SAPbobsCOM.BoYesNoEnum.tNO,
                string Refsh_FldId = "", SAPbobsCOM.BoYesNoEnum ForceRefresh = SAPbobsCOM.BoYesNoEnum.tNO)
                {
                    try
                    {
                        SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
                        SAPbobsCOM.Recordset oRs1 = default(SAPbobsCOM.Recordset);
                        SAPbobsCOM.Recordset oRs2 = default(SAPbobsCOM.Recordset);
                        //Dim Qnew As SAPbobsCOM.UserQueries
                        SAPbobsCOM.FormattedSearches Fsearch = default(SAPbobsCOM.FormattedSearches);
                        oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        oRs1 = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        oRs2 = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        Fsearch = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oFormattedSearches);
                        long l = 0;
                        //-----------Formatted Search ---------------
                        //******************* Formatted search on Receipt From Production Doc.*****************************
                        Sql  = "Select * From CSHS Where FormID='" + FormID + "' AND ColID='" + ColId + "' and ItemID='" + ItemID + "'";
                        oRs.DoQuery("Select * From CSHS Where FormID='" + FormID + "' AND ColID='" + ColId + "' and ItemID='" + ItemID + "'");
                        if (oRs.RecordCount == 0)
                        {
                            oRs2.DoQuery("Select IntrnalKey From OUQR Where QName='" + Query_Name + "'");
                            if (oRs2.RecordCount == 0)
                            {
                                AddQuery(Query_Catgory, Query_Name, Sql_Query);
                            }
                            oRs1.DoQuery("Select IntrnalKey From OUQR Where QName='" + Query_Name + "'");
                            Fsearch.QueryID = oRs1.Fields.Item(0).Value;
                            Fsearch.FormID = FormID;
                            Fsearch.ItemID = ItemID;
                            Fsearch.ColumnID = ColId;
                            Fsearch.Action = Action;
                            Fsearch.Refresh = Refresh;
                            Fsearch.FieldID = Refsh_FldId;
                            Fsearch.ForceRefresh = ForceRefresh;

                            l = Fsearch.Add();
                            if (l != 0)
                            {
                                Sql  = "";
                            }

                        }
                        //******************* Formatted search on Batch No Doc.*****************************

                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.ToString);
                    }
                }
        
                public void AddQuery(string QueryCategorie, string QueryName, string Query)
                {
                    try
                    {
                        SAPbobsCOM.QueryCategories Qcat = default(SAPbobsCOM.QueryCategories);
                        SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
                        SAPbobsCOM.Recordset oRs1 = default(SAPbobsCOM.Recordset);
                        SAPbobsCOM.UserQueries Qnew = default(SAPbobsCOM.UserQueries);
                        // Dim Fsearch As SAPbobsCOM.FormattedSearches
                        oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                        oRs1 = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                        //-----------Create New Query Category ---------------
                        oRs.DoQuery("Select * From OQCN Where CatName='" + QueryCategorie + "'");
                        if (oRs.RecordCount == 0)
                        {
                            Qcat = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oQueryCategories);
                            Qcat.Name = QueryCategorie;
                            Qcat.Add();
                        }

                        //-----------Create New Query ---------------
                        //-----------Batch Qry. on Receipt from Production ---------------
                        oRs1.DoQuery("Select * From OUQR Where QName='" + QueryName + "'");
                        if (oRs1.RecordCount == 0)
                        {
                            oRs.DoQuery("Select CategoryId From OQCN Where CatName='" + QueryCategorie + "'");
                            Qnew = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserQueries);
                            Qnew.Query = Query;
                            Qnew.QueryCategory = oRs.Fields.Item(0).Value;
                            Qnew.QueryDescription = QueryName;
                            long i = 0;
                            i = Qnew.Add();
                        }

                    }
                    catch (Exception ex)
                    {
                    }
                }

        #endregion

        #region Progress Bar
                public void ProgessBarStart(string Msg, int MinVal, int MaxVal, bool Stoppable = true)
                {
                    try
                    {
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oPrg);
                        oPrg = null;
                        GC.Collect();
                    }
                    catch
                    {
                    
                    }
                    try
                    {
                        oPrg = SboApp.StatusBar.CreateProgressBar(Msg, MinVal, Stoppable);
                        oPrg.Maximum = MaxVal;
                        //return oPrg;
                    }
                    catch
                    {
                        //return null;
                    }
                }
                public void ProgressBarStatus(int Val)
                {
                    try
                    {
                        oPrg.Value = Val;
                    }
                    catch
                    { 
                    
                    }
                }
                public void ProgressBarStatus(int Val,string Msg)
                {
                    try
                    {
                        oPrg.Value = Val;
                        oPrg.Text = Msg;
                    }
                    catch
                    {

                    }
                }
                public void ProgressBarClose()
                {
                    try
                    {
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oPrg);
                        oPrg = null;                        
                        GC.Collect();   
                    }
                    catch
                    { 
                    }
                 }
        #endregion


    }
}
