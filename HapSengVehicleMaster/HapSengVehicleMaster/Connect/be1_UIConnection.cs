﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
//using System.Windows.Forms;
using System.Reflection;
using System.Windows.Forms;


namespace HapSengVehicleMaster
{
    public class be1_UIConnection : Global
    {

      
        #region SBO Application UI Event
        //public SAPbouiCOM.Company oCompany;
        SAPbouiCOM.Form oForm; 

       public void SetApplication(string AddOnName)
        {
            SAPbouiCOM.SboGuiApi SboGuiApi = default(SAPbouiCOM.SboGuiApi);
            string sConnectionString = null;

            try
            {
                AddOn_Name = AddOnName;
                SboGuiApi = new SAPbouiCOM.SboGuiApi();

                sConnectionString = Environment.GetCommandLineArgs().GetValue(1).ToString();
                SboGuiApi.Connect(sConnectionString);

                SboApp = SboGuiApi.GetApplication();

                oMsgShow = new MsgShow();
                oMsgShow.Messsage(AddOn_Name + " Add On UI Connection Successfully.", MsgShow.MsgType.Statusbar, MsgShow.MsgCat.Success);

                oCompany = new SAPbobsCOM.Company();
                oCompany = SboApp.Company.GetDICompany();

                

                SboApp.FormDataEvent += new SAPbouiCOM._IApplicationEvents_FormDataEventEventHandler(SboApp_FormDataEvent);
                SboApp.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SboApp_ItemEvent);
                SboApp.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SboApp_MenuEvent);
                SboApp.RightClickEvent += new SAPbouiCOM._IApplicationEvents_RightClickEventEventHandler(SboApp_RightClickEvent);
                SboApp.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(SboApp_AppEvent);
                SboApp.ReportDataEvent += new SAPbouiCOM._IApplicationEvents_ReportDataEventEventHandler(SboApp_ReportDataEvent);
                SboApp.StatusBarEvent += new SAPbouiCOM._IApplicationEvents_StatusBarEventEventHandler( SboApp_StatusBarEvent);
                
                // if you required the you can use this.
                //ADO_Connection();

            }
            catch (Exception Ex)
            {
                System.Windows.Forms.MessageBox.Show("Connect to Company Exception " + Ex.Message.ToString());
                System.Environment.Exit(0);
            }

        }

      

       public void SboApp_StatusBarEvent(string Text, SAPbouiCOM.BoStatusBarMessageType messageType)
       {
           //try
           //{
           //    if (_changeMsg) //As you sets as true, in the first time, your code catch here
           //    {
           //        _errorSAPAddonMessage = Text;//Sets the SAP B1 error message to the global variable
           //        Text = _newMsg; //Changes the SAP message
           //        _changeMsg = false; //Sets to false the change message property
           //      SboApp.StatusBar.SetText  (Text , SAPbouiCOM.BoMessageTime.bmt_Short, messageType); //Set your message again
           //    }
           //    if (_errorSAPAddonMessage.Equals(Text)) //The SAP B1 will be set the message of error again, then you should change it again.
           //    {
                   
           //        _newMsg = string.Empty; //clean your message
           //    }
           //}
           //catch (System.Exception)
           //{
               
           
           //}
       }

       

        public void eventFilter(String[] FormTypeEx)
        {
            // Add additional form to be filtered

            try
            {
                oFilters = new SAPbouiCOM.EventFilters();
                //CC Filter'
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_ALL_EVENTS);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_MENU_CLICK);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_CLICK);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_LOAD);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_KEY_DOWN);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_RESIZE);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE);

                for (int i  =0; i<FormTypeEx.Length ;i++)
                {
                    oFilter.AddEx(FormTypeEx [i]);
                }
                
                SboApp.SetFilter(oFilters);
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetSystemMessage("Event Filter Exception: " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        private void SboApp_ReportDataEvent(ref SAPbouiCOM.ReportDataInfo eventInfo, out bool BubbleEvent)
        {
           BubbleEvent=true;
           try
           {
               SAPbouiCOM.Form oForm = SboApp.Forms.ActiveForm;
              if (FormArray.Contains(oForm.TypeEx) == true)
               {
                    var ocls = MagicallyCreateInstance("_" + oForm.TypeEx);
                   object[] mParam = { eventInfo, BubbleEvent };
                   InvokeClassMethod(ocls, ocls.GetType(), "EventHandler", mParam);
                   BubbleEvent = (bool)mParam[1];
                   ocls = null;
                   GC.Collect();
               }
           }
           catch (Exception e)
           {

           }
        }

        private void SboApp_RightClickEvent(ref SAPbouiCOM.ContextMenuInfo eventInfo, out bool BubbleEvent)
        {

            BubbleEvent = true;
          

           try
           {
               if (eventInfo.BeforeAction == true)
               {
                   SAPbouiCOM.Form oForm1;
                   oForm1 = SboApp.Forms.ActiveForm;
                   if (MoldalFormFlag == true && ModalFormArray.Contains(oForm1.TypeEx) == false)
                   {
                       oForm1 = SboApp.Forms.GetForm(MoldalFormName, -1);
                       oForm1.Select();
                       BubbleEvent = false;
                       return;
                   }
               }
               SAPbouiCOM .Form oForm=SboApp .Forms .ActiveForm ;
               if (FormArray.Contains(oForm.TypeEx) == true)
               {
                   var ocls = MagicallyCreateInstance("_" + oForm.TypeEx);
                   object[] mParam = {eventInfo, BubbleEvent };
                   InvokeClassMethod(ocls, ocls.GetType(), "RightClickEvent", mParam);
                   BubbleEvent = (bool)mParam[1];
                   ocls = null;
                   GC.Collect();
               }
              
           }
           catch (Exception e)
           {

           }
        }

        private void SboApp_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {              
                    
                    oMenu.MenuHandler(ref pVal,ref BubbleEvent);
               

            }
            catch (Exception e)
            {

            }
        }
        
        private void SboApp_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {
         switch (EventType)
         {
             case SAPbouiCOM.BoAppEventTypes.aet_ShutDown :
                 //oMenu.Delete_All_Menu();// When Our add on is shutdown then all the menu which is created by Add on is remove by this methoed.
                 oMenu.DeleteMenuUsingXML();// When Our add on is shutdown then all the menu which is created by Add on is remove by this methoed.
                 
                 oMsgShow.Messsage(AddOn_Name + " Add On Shut Down.", MsgShow.MsgType.Statusbar, MsgShow.MsgCat.Success);
                 Application.Exit();
                 break ;


         }
         

        }

        private void SboApp_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.BeforeAction == true)
                {
                    SAPbouiCOM.Form oForm1;
                    oForm1 = SboApp.Forms.ActiveForm;
                    
                    if (oForm1.TypeEx.ToString() != "41" && oForm1.TypeEx.ToString() != "21")
                    {
                        FormTypeFlag = oForm1.TypeEx.ToString();
                    }
                    if (MoldalFormFlag == true && ModalFormArray.Contains(oForm1.TypeEx) == false && oForm1.TypeEx != "ShowDetails" && oForm1.TypeEx != "ShowDetails2"  && oForm1.TypeEx != "ShowDetails_buton")
                    {
                        oForm1 = SboApp.Forms.GetForm(MoldalFormName, -1);
                        oForm1.Select();
                        BubbleEvent = false;
                        return;
                    }
                }

                try
                {
                    if (pVal.EventType !=SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE) 
                    {
                        if (FormArray.Contains(pVal.FormTypeEx) == true)
                        {
                            var ocls = MagicallyCreateInstance("_" + pVal.FormTypeEx);
                            object[] mParam = { pVal, BubbleEvent };
                            InvokeClassMethod(ocls, ocls.GetType(), "EventHandler", mParam);
                            BubbleEvent = (bool)mParam[1];
                            ocls = null;
                            GC.Collect();
                            return;
                        }
                      
                    }
                    else if (pVal.FormTypeEx == "BE1SVM")
                    {
                        var ocls = MagicallyCreateInstance("_" + pVal.FormTypeEx);
                        object[] mParam = { pVal, BubbleEvent };
                        InvokeClassMethod(ocls, ocls.GetType(), "EventHandler", mParam);
                        BubbleEvent = (bool)mParam[1];
                        ocls = null;
                        GC.Collect();
                        return;
                    }
                   
                }
                catch (Exception e)
                {

                }

                //if (pVal.EventType ==SAPbouiCOM.BoEventTypes.et_FORM_LOAD ) 
                //{
                //    switch (pVal.FormTypeEx)
                //    {
                //        case "169":
                //            oMainMenuForm = new _169();
                //            break ;

                //        case "672":
                //            oBomForm = new _672();
                //            oBomForm.EventHandler(ref pVal, ref BubbleEvent);
                //            break;
                //        case "150":
                //            _150 oOITM = new _150();
                //            oOITM.EventHandler (ref pVal , ref BubbleEvent);
                //            break;

                //    }
                  
                //}

               

            }
            catch (Exception ex)
            {}
            
            //throw new System.NotImplementedException();
        }
              
        private void SboApp_FormDataEvent(ref SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (FormArray.Contains(BusinessObjectInfo.FormTypeEx) == true)
                {
                    var ocls = MagicallyCreateInstance("_" + BusinessObjectInfo.FormTypeEx);
                    object[] mParam = { BusinessObjectInfo, BubbleEvent };
                    InvokeClassMethod(ocls, ocls.GetType(), "FormDataLoadEvent", mParam);
                    BubbleEvent = (bool)mParam[1];
                    ocls = null;
                    GC.Collect();
                }
             }
                catch (Exception e)
                {

                }
        }
        #endregion
        #region Dynamic Class Object & Method access
        public  object MagicallyCreateInstance(string className)
        {
            try
            {
                var assembly = Assembly.GetExecutingAssembly();

                var type = assembly.GetTypes()
                    .First(t => t.Name == className);

                return Activator.CreateInstance(type);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public object InvokeClassMethod(object cls, Type clsType, string clsmMethod, object[] mParam)
        {
            try
            {
                MethodInfo myMethodInfo = clsType.GetMethod(clsmMethod);
                object i = myMethodInfo.Invoke(cls, mParam);
                return i;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        #endregion
        
    }

}
