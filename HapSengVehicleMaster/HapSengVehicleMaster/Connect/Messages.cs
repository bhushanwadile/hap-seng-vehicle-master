﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
namespace HapSengVehicleMaster
{
    public class MsgShow : be1_UIConnection 
    {

        public enum MsgType
        {
            PopUp,
            Statusbar
        }
        public enum MsgCat
        {
            Errors,
            Success,
            Warning
        }
        public enum Questiontype
        {
            YN,
            YNC
        }
        public enum logActType
        {
            BOMCalc,
            UpdPrice,
            MatRev
        }
        public MsgShow()
        { 
        
        }
        public  void Messsage(string msgtext, MsgType msgtype1)
        {
                switch (msgtype1)
                {
                    case MsgType.PopUp:
                        SboApp.MessageBox(msgtext, 1, "OK", "", "");
                        break;
                    case MsgType.Statusbar:
                        SboApp.SetStatusBarMessage(msgtext, SAPbouiCOM.BoMessageTime.bmt_Short, false);
                        break;
                }
           
        }
        public  void Messsage(string msgtext, MsgType msgtype1, MsgCat msgcat1)
        {
            try
            {
                switch (msgtype1)
                {
                    case MsgType.PopUp:
                        SboApp.MessageBox(msgtext, 1, "OK", "", "");
                        break;
                    case MsgType.Statusbar:
                        if (msgcat1 == MsgCat.Errors)
                        {
                            SboApp.StatusBar.SetText(msgtext, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                        }
                        else if (msgcat1 == MsgCat.Success)
                        {
                            SboApp.StatusBar.SetText(msgtext, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                        }
                        else if (msgcat1 == MsgCat.Warning)
                        {
                            SboApp.StatusBar.SetText(msgtext, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                        }
                        break;
                }

            }
            catch
            {
            }
        }
        public  int QusMsg(string msgtext, Questiontype qtype)
        {
            switch (qtype)
            {
                case Questiontype.YN:
                    return SboApp.MessageBox(msgtext, 2, "Yes", "No", "");
                case Questiontype.YNC:
                    return SboApp.MessageBox(msgtext, 2, "Yes", "No", "Cancel");
            }
            return -1;
        }
        
    }
}