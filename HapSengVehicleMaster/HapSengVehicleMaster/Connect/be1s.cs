﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System;
using System.Collections;
//using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
namespace HapSengVehicleMaster
{
    public class be1
    {

        public SAPbouiCOM.Application SboApp;
        public int LastFormNumber = 0;
        public SAPbouiCOM.EventFilters oFilters;
        public SAPbouiCOM.EventFilter oFilter;
        //meta data


        // Create objects


        public be1(): base()
        {

           

        }
        public  void be1_Start()
        {
            //get a connection to the currently
            //running sbo instance
            
            SetApplication();
            //Set Filter
            eventFilter();
            //initialize variables
            Initialize();
        }

        public void SetApplication()
        {
            //this function connects to the currently 
            //runing sbo instance
            SAPbouiCOM.SboGuiApi SboGuiApi = default(SAPbouiCOM.SboGuiApi);
            string sConnectionString = null;

            try
            {
                SboGuiApi = new SAPbouiCOM.SboGuiApi();

                sConnectionString = Environment.GetCommandLineArgs().GetValue(1).ToString();
                SboGuiApi.Connect(sConnectionString);

                //SboApp = SboGuiApi.GetApplication();

                //Global.oCompany = new SAPbobsCOM.Company();
                //Global.oCompany = SboApp.Company.GetDICompany();


            }
            catch (Exception Ex)
            {
                System.Windows.Forms.MessageBox.Show("Connect to Company Exception " + Ex.Message.ToString());
                System.Environment.Exit(0);
            }

        }
        public void eventFilter()
        {
            // Add additional form to be filtered

            try
            {

                oFilters = new SAPbouiCOM.EventFilters();
                //CC Filter'
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_ALL_EVENTS);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_MENU_CLICK);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_CLICK);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_LOAD);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_KEY_DOWN);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_RESIZE);
                oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE);

                oFilter.AddEx ("BE1SCCMn");
                oFilter.AddEx("BE1SGG");
                oFilter.AddEx("BE1SCFG");
                oFilter.AddEx("BE1LMSF");
                oFilter.AddEx("70001");
                oFilter.AddEx("672");
                oFilter.AddEx("65211");
                //QC filter
                oFilter.AddEx("QCORDER");
                oFilter.AddEx("150");
                oFilter.AddEx("AC_SSE02");
                oFilter.AddEx("INSPTT");
                oFilter.AddEx("InspOpenBatch");
                oFilter.AddEx("B1SDtl");
                oFilter.AddEx("BatchQCOrdr");
                oFilter.AddEx("B1SLMQCRTT");
                oFilter.AddEx("B1S_GF01");
                oFilter.AddEx("QCReceipt");
                oFilter.AddEx("41");
                oFilter.AddEx("42");
                //EXFUnit Price Update
                oFilter.AddEx("139");

                //SboApp.SetFilter(oFilters);
            }
            catch (Exception ex)
            {
                //SboApp.StatusBar.SetSystemMessage("Event Filter Exception: " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        private void Initialize()
        {
            //init_getcompanydetail();
            //init_getuserpermission();
        }



        private void SboApp_FormDataEvent(ref SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, out bool BubbleEvent)
        {
            //BPN
            BubbleEvent = true;
            try 
            {
                
            }
            catch (Exception ex)
            {
                //MsgLang.Messsage("Form Data load exception : " + ex.Message.ToString(), MsgType.Statusbar, MsgCat.Errors);
            }
            //BPN End



        }

        private void SboApp_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {

                BubbleEvent = true;

                if (pVal.ItemUID =="" )
                {
                string i="0";
                }

                //BPN 07/22/2014 END 
            }
            catch (Exception Ex)
            {
                //SboApp.SetStatusBarMessage("ItemEvent: " & Ex.Message)
                //SboApp.MessageBox("ItemEvent: " & Ex.Message & Chr(10) & Ex.StackTrace)
            }
        }
        private void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {

                string[] NavigationMenus = {"1281","1282","1288","1289","1290","1291"};
                string[] MatrixMenus = {"1292",	"1293"};
                string[] BlockAdd = { "AC_PB02" };
                
               
            }
            catch (Exception ex)
            {
                //MsgLang.Messsage("B1SLM Menu Event Exception : " + ex.Message.ToString(), MsgType.Statusbar, MsgCat.Errors);
            }
        }

        private void SBO_Application_RightClickEvent(ref SAPbouiCOM.ContextMenuInfo eventInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SAPbouiCOM.Form oForm = default(SAPbouiCOM.Form);

           // oForm = SboApp.Forms.Item(eventInfo.FormUID);


           

        }

        private void SBO_Application_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {
            if (EventType == SAPbouiCOM.BoAppEventTypes.aet_ShutDown | EventType == SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged | EventType == SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition)
            {
               

                //END
            }
        }


    }
}


