﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Xml;
namespace HapSengVehicleMaster
{
    
    public class Menu : UIDesign
    {
        
        public void AddAllMenu()

        {           

            DeleteMenuUsingXML();
            CreateMenuUsingXML("169");         
            
        }

        public void MenuHandler(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent)
        {
            try
            {
                

                Sql = pVal.MenuUID.Substring(0, 3).ToLower();
                if (Sql == "bos")
                {

                    string ClassName = pVal.MenuUID.Substring(4);
                    string MenuId = ClassName.Substring(ClassName.IndexOf("_")+1);
                    ClassName = ClassName.Substring(0, ClassName.IndexOf("_"));

                    //if (Check_Menu_Athorization (MenuId)==false)
                    //{
                    //    oMsgShow.Messsage("This User Not Authorization For This Form.", MsgShow.MsgType.PopUp, MsgShow.MsgCat.Success);
                    //    return;
                    //}

                    if (FormArray.Contains(ClassName) == true)
                    {
                        var ocls = MagicallyCreateInstance("_" + ClassName);
                        object[] mParam = { pVal, BubbleEvent };
                        InvokeClassMethod(ocls, ocls.GetType(), "MenuEvent", mParam);
                        BubbleEvent = (bool)mParam[1];
                        ocls = null;
                        GC.Collect();
                    }

                }
                else if (StdMenuArray.Contains(pVal.MenuUID) == true)
                {
                    SAPbouiCOM.Form oForm = SboApp.Forms.ActiveForm;
                    string ClassName = oForm.TypeEx;

                    if (FormArray.Contains(ClassName) == true)
                    {
                        var ocls = MagicallyCreateInstance("_" + ClassName);
                        object[] mParam = { pVal, BubbleEvent };
                        InvokeClassMethod(ocls, ocls.GetType(), "MenuEvent", mParam);
                        BubbleEvent = (bool)mParam[1];
                        ocls = null;
                        GC.Collect();
                    }
                }

                //SAPbouiCOM.Menus oMenus = new SAPbouiCOM.Menus();
                //SAPbouiCOM.MenuItem oMenuItem = SboApp.Menus.Item(pVal.MenuUID);
                //oMenus = SboApp.Menus;


                // commmented this after error for the authorization from terry on 23 march 2018
                //if (pVal.BeforeAction == true)
                //{
                //    string str = SboApp.Menus.Item(pVal.MenuUID).String;
                //    if (str.Equals(""))
                //    {
                //    }
                //    else
                //    {
                //        string str1 = str.Substring(0, str.IndexOf("-") - 1);
                //        if (Check_Menu_Athorization(str1) == false)
                //        {
                //            BubbleEvent = false;
                //            SboApp.StatusBar.SetText("You are not authorize to view.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                //        }
                //    }
                //}
                //end commmented this after error for the authorization from terry on 23 march 2018
            }
            catch (Exception e)
            {

            }
        }

        public void Delete_All_Menu()
        {
            try
            {
                SAPbouiCOM.Menus oMenus;
                SAPbouiCOM.MenuItem oMenuItem;
                for (int i = 0; i < SboApp.Menus.Count; i++)
                {
                    oMenuItem = SboApp.Menus.Item(i);
                    oMenus = oMenuItem.SubMenus;
                    for (int ii = 0; ii <= oMenus.Count - 1; ii++)
                    {
                        if (oMenus.Item(ii).UID.Length >= 3)
                        {
                        if (oMenus.Item(ii).Type != SAPbouiCOM.BoMenuType.mt_STRING)
                        {
                            Delete_SubMenu(oMenus.Item(ii).UID);
                        }
                            if (oMenus.Item(ii).UID.Substring(0, 3).ToLower() == "b1s")
                            {
                                SboApp.Menus.RemoveEx(oMenus.Item(ii).UID.ToString());
                            }
                        }

                    }
                }
            }
            catch (Exception e)
            {

            }
        }

        public void Delete_SubMenu(string MenuId)
        {
            try
            {
                SAPbouiCOM.Menus oMenus;
                SAPbouiCOM.MenuItem oMenuItem;

                oMenuItem = SboApp.Menus.Item(MenuId);
                oMenus = oMenuItem.SubMenus;
                for (int ii = 0; ii <= oMenus.Count - 1; ii++)
                {
                    if (oMenus.Item(ii).UID.Length >= 3)
                    {
                    if (oMenus.Item(ii).Type != SAPbouiCOM.BoMenuType.mt_STRING)
                    {
                        Delete_SubMenu(oMenus.Item(ii).UID);
                    }

                   
                        if (oMenus.Item(ii).UID.Substring(0, 3).ToLower() == "b1s")
                        {
                            SboApp.Menus.RemoveEx(oMenus.Item(ii).UID.ToString());
                        }
                    }

                }
            }
            catch (Exception e)
            {

            }
        }

        public void Delete_Form_Context_Menu(string FormTypeEx)
        {
            try
            {
                SAPbouiCOM.Menus oMenus;
                SAPbouiCOM.MenuItem oMenuItem;

                oMenuItem = SboApp.Menus.Item("1280");
                oMenus = oMenuItem.SubMenus;
                for (int ii = 0; ii <= oMenus.Count - 1; ii++)
                {
                    if (oMenus.Item(ii).UID.Length >= 3)
                    {
                        Sql = oMenus.Item(ii).UID.Substring(0, 3);
                        int i = oMenus.Item(ii).UID.IndexOf("_", 4);
                        if (oMenus.Item(ii).UID.Substring(0, i).ToLower() == "b1s_" + FormTypeEx.ToLower())
                        {
                            SboApp.Menus.RemoveEx(oMenus.Item(ii).UID.ToString());
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
        }

        public void CreateMenuUsingXML(string FormType)
        {
            try
            {
                DataTable DtMenu = new DataTable();
                DtMenu = ReadMenuXML(FormType);
                for (int i = 0; i < DtMenu.Rows.Count ; i++)
                {
                    //if (Check_Menu_Athorization(DtMenu.Rows[i]["Id"].ToString()) == false)
                    //{
                    //    continue;
                    //}

                    // VMBE1S_169_VM

                    string ParantId;
                    SAPbouiCOM.BoMenuType MenuType=SAPbouiCOM.BoMenuType.mt_STRING;

                    if (DtMenu.Rows[i]["StdParent"].ToString().ToUpper() == "Y")
                    {
                        ParantId =DtMenu.Rows [i]["ParentMenu"].ToString ();
                    }
                    else
                    {
                        ParantId =AddOn_Company_Id+"_"+ DtMenu.Rows [i]["Form"].ToString () +"_"+DtMenu.Rows [i]["ParentMenu"].ToString ();
                    }

                    switch (DtMenu.Rows[i]["MenuType"].ToString())
                    {
                        case "1":
                            MenuType = SAPbouiCOM.BoMenuType.mt_SEPERATOR;
                            break;
                        case "2":
                            MenuType = SAPbouiCOM.BoMenuType.mt_POPUP;
                            break;
                        case "3":
                            MenuType = SAPbouiCOM.BoMenuType.mt_STRING;
                            break;
                    }

                    AddMenu(ParantId, MenuType, AddOn_Company_Id+"_"+ DtMenu.Rows [i]["Form"].ToString () +"_"+DtMenu.Rows[i]["Id"].ToString(), Convert.ToInt16(DtMenu.Rows[i]["Pos"].ToString()), DtMenu.Rows[i]["Tital"].ToString(), DtMenu.Rows[i]["Image"].ToString(),Convert.ToBoolean ( DtMenu.Rows[i]["Enable"]));
                }
            }
            catch(Exception e)
            { 
            }
        }
        public void DeleteMenuUsingXML(string FormType = "")
        {
            // If use want delete all the menu which is create by our add on thet time user not pass any form type & sytem take is "" blank form type.
            try
            {
                DataTable DtMenu = new DataTable();
                DtMenu = ReadMenuXML(FormType);
                for (int i = 0; i < DtMenu.Rows.Count; i++)
                {
                    string MenutId;
                    MenutId = AddOn_Company_Id + "_" + DtMenu.Rows[i]["Form"].ToString() + "_" + DtMenu.Rows[i]["Id"].ToString();
                    try
                    {
                        SboApp.Menus.RemoveEx(MenutId);
                    }
                    catch
                    { 
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        public DataTable ReadMenuXML(string FormType)
        {
            try
            {
                DataSet ds = new DataSet();
                ds.ReadXml("Menu.xml");
                DataTable Dt = ds.Tables[0];
                AddOn_Company_Id = Dt.Rows[0][0].ToString();
                AddOn_Id = Dt.Rows[0][1].ToString();
                DataTable Dt1 = ds.Tables[1];

                if (FormType != "")
                {
                    string SQLCondition = "Form='" + FormType + "'";
                    //DataRow[] DtRow = Dt1.Select(SQLCondition);

                    DataTable output = new DataTable();
                    output = Dt1.Select(SQLCondition).CopyToDataTable(); 
                    //foreach (DataRow e in DtRow)
                    //{
                    //    output.ImportRow(e);
                    //}
                    return output;
                }
                else
                {
                    return Dt1;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

    }
}

