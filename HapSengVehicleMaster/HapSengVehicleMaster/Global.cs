﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using SAPbobsCOM;
using SAPbouiCOM;
using System.Globalization;
using System.Xml;
using System.Xml.XmlConfiguration;
using System.Security.Cryptography;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;

namespace HapSengVehicleMaster
{
    public class Global
    {
        #region Global Veriable Declaration
       

        #region SBO Releted Veriable
        public static SAPbouiCOM.Application SboApp;
        public static SAPbouiCOM.EventFilters oFilters;
        public static SAPbouiCOM.EventFilter oFilter;
        public static SAPbobsCOM.Company oCompany;
        #endregion

        #region Class veriable
        public static be1_UIConnection obe1sUi;
        public static MsgShow oMsgShow;
        public static Menu oMenu;
        
        #endregion
        public static System.Data.DataTable dt;
        
        public static int RITE = 0;
        public static System.Data.DataTable dtBatchAllocate;
        public static System.Data.DataTable dtCBU;
        public static bool batchExistFlag = false;
        public static Hashtable dtKDDocEntry = new Hashtable();
        public static bool SuccessFlag = false;
        public static Hashtable htValidate = new Hashtable();
        public static Hashtable htBatch = new Hashtable();
        public static Hashtable htIdentity = new Hashtable();
        public static Hashtable htSerial = new Hashtable();
        public static int DocEntry= 0;
        public static int DocEntryCBU = 0;
        public static string EngineNo = "";
        public static string ChassisNo = "";
        public static string ItemType = "";
        public static System.Data.DataTable dtSerialUpdate;
        public static List<string> list = new List<string>();
        public static List<string> listChassis = new List<string>();
        public static List<string> ListFGCommissionNo = new List<string>();
        public static SqlConnection connobj;
        public static bool MultiLine = false;
        public static string Sql;
        public static string AddOn_Name;
        public static string AddOn_Id;
        public static string AddOn_Company_Id; 
        public bool costbywhsflag;
        public static string ChildBaseForm;
        public static int ChildOpen;
        public static string AltSrcFG;
        public static string AltSrcitm, AltSrcwhs;
        public static string AltSrcItmNm;
        public static SAPbouiCOM.Form BOMform;
        public static string SepDec, MainCurr;
        public static int Pricedec;
        public static string[] FormArray;
        public static string[] StdMenuArray;
        public static string[] NoObjectMenuArray;
        public static string[] ModalFormArray;
        public static SAPbouiCOM.ProgressBar oPrg;
        public static string FormTypeFlag = "";

        public static SAPbouiCOM.Form oBForm;
        public static SAPbouiCOM.EditTextColumn oBTxt;
        public static FormOpenType FormType;
        public static SAPbouiCOM.Cell oBCell;
        public static SAPbouiCOM.EditText oBEditext;
        public static SAPbouiCOM.EditText oBEditext2;
        public static string CFLSelectCode = "";

        public static bool sts = false;
        public static int selectedRow = 0;
        public static string LastAddRecord = "";
        public static bool MoldalFormFlag = false;
        public static string MoldalFormName ;
        public static string SelectedCol;
        public static string MainSql;
        public static  string ThousSep;
        public static  string QtyDec;

        public static bool _changeMsg = false;
        public static string _newMsg = string.Empty;
        public static string _errorSAPAddonMessage = string.Empty;

        public static  string RateDec;
        public static  string SumDec;
        public static  int  Language;
        public static NumberFormatInfo prvider;

        public enum FormOpenType
        {
            WithMatrixCell,
            WithLargTextCell,
            WithGridCell,
            WithMatrixEditBox,
            WithLargTextEditBox,
            WithGridEditBox
        }
        #endregion
  
        #region "Global Method"

        public void init_getcompanydetail()
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;
            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            //BPN '
            try
            {
                //Check cost by warehouse flag

                Query = "select top 1 priceSys, DecSep, PriceDec, QtyDec, SumDec, RateDec ,ThousSep, MainCurncy from OADM";

                oRecordset.DoQuery(Query);
                if (oRecordset.RecordCount <= 0)
                {
                    return;
                }

                if ((oRecordset.Fields.Item("priceSys").Value.ToString() == "Y"))
                {
                    costbywhsflag = true;
                }
                else
                {
                    costbywhsflag = false;
                }

                if ((!string.IsNullOrEmpty(oRecordset.Fields.Item("DecSep").Value.ToString())))
                {
                    SepDec = oRecordset.Fields.Item("DecSep").Value.ToString().TrimEnd();
                }
                if ((!string.IsNullOrEmpty(oRecordset.Fields.Item("ThousSep").Value.ToString())))
                {
                    ThousSep = oRecordset.Fields.Item("ThousSep").Value.ToString().TrimEnd();
                }
                if ((!string.IsNullOrEmpty(oRecordset.Fields.Item("PriceDec").Value.ToString())))
                {
                    Pricedec = oRecordset.Fields.Item("PriceDec").Value.ToString().TrimEnd();
                }
                if ((!string.IsNullOrEmpty(oRecordset.Fields.Item("QtyDec").Value.ToString())))
                {
                    QtyDec = oRecordset.Fields.Item("QtyDec").Value.ToString().TrimEnd();
                }
                if ((!string.IsNullOrEmpty(oRecordset.Fields.Item("SumDec").Value.ToString())))
                {
                    SumDec = oRecordset.Fields.Item("SumDec").Value.ToString().TrimEnd();
                }
                if ((!string.IsNullOrEmpty(oRecordset.Fields.Item("RateDec").Value.ToString())))
                {
                    RateDec = oRecordset.Fields.Item("RateDec").Value.ToString().TrimEnd();
                }
                if ((!string.IsNullOrEmpty(oRecordset.Fields.Item("MainCurncy").Value.ToString())))
                {
                    MainCurr = oRecordset.Fields.Item("MainCurncy").Value.ToString().TrimEnd();
                }
                Language = (int) oCompany.language;

                prvider = new System.Globalization.NumberFormatInfo();
                prvider.NumberDecimalSeparator = SepDec;
                prvider.NumberGroupSeparator = ThousSep;


                
                }

            catch (Exception ex)
            {
                SboApp.SetStatusBarMessage("Init: " + ex.Message, BoMessageTime.bmt_Short, true);
            }

        }

        public void MatrixRowAdd(SAPbouiCOM.Form oForm, SAPbouiCOM.Matrix oMatrix)
        {
            try
            {
                if (oMatrix.VisualRowCount <= oMatrix.RowCount)
                {
                    oMatrix.AddRow();
                }
                    //oMatrix.ClearRowData(oMatrix.RowCount);
                    //oMatrix.FlushToDataSource();
                    //oMatrix.LoadFromDataSource();
                
                //for (int i = 1; i <= oMatrix.RowCount; i++)
                //{
                //    oMatrix.Columns.Item("#").Cells.Item(i).Specific.value = i;
                //    //if (i > 1)
                //    //{
                //    //    oMatrix.Columns.Item("C_0_1").Cells.Item(i).Click();
                //    //}
                //}


            }
            catch (Exception ex)
            { }
        }
        public void MatrixRowRemove(SAPbouiCOM.Form oForm, SAPbouiCOM.Matrix oMatrix, int RowNum)
        {
            try
            {

                oMatrix.FlushToDataSource();
                oMatrix.DeleteRow(RowNum);
                string DataSource = oMatrix.Columns.Item(1).DataBind.TableName;
                oForm.DataSources.DBDataSources.Item(DataSource).RemoveRecord(RowNum-1);
                
                //oMatrix.LoadFromDataSource();
                //for (int i = 1; i <= oMatrix.RowCount; i++)
                //{
                //    oMatrix.Columns.Item("V_1").Cells.Item(i).Specific.value = i;
                //}
                if (oForm.Mode ==BoFormMode.fm_OK_MODE)
                {
                    oForm.Mode = BoFormMode.fm_UPDATE_MODE;
                }
            }
            catch (Exception)
            {

            }
        }
        public long MaxCode(string TableName)
        {
            string strSQL = null;
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);

            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            strSQL = "SELECT TOP(1) CODE FROM [" + TableName + "] ORDER BY CAST(CODE AS BIGINT) DESC";

            oRecordset.DoQuery(strSQL);
            oRecordset.MoveFirst();

            if (string.IsNullOrEmpty(oRecordset.Fields.Item("Code").Value))
            {
                return 1;
            }
            else
            {
                return oRecordset.Fields.Item("Code").Value;
            }
        }
        public long GetNextDocEntry(string TableName)
        {
            string strSQL = null;
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);

            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            strSQL = "SELECT ISNULL(Max(DocEntry), 0)+1 FROM [" + TableName + "]";

            oRecordset.DoQuery(strSQL);
            oRecordset.MoveFirst();

            if (string.IsNullOrEmpty(Convert.ToString(oRecordset.Fields.Item(0).Value)))
            {
                return 1;
            }
            else
            {
                return oRecordset.Fields.Item(0).Value;
            }
        }
        public long MaxLine(string TableName, string Code)
        {
            string strSQL = null;
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);

            oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            strSQL = "SELECT TOP(1) LineId FROM [" + TableName + "] " + " WHERE Code = '" + Code + "'" + " ORDER BY LineId DESC";

            oRecordset.DoQuery(strSQL);
            oRecordset.MoveFirst();

            return oRecordset.Fields.Item(0).Value;
        }
        public string MonthName(int Month)
        {
            switch (Month)
            {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
            }

            return "";
        }
        public int MonthDays(int Month)
        {
            switch (Month)
            {
                case 1:
                    return 31;
                case 2:
                    return 28;
                case 3:
                    return 31;
                case 4:
                    return 30;
                case 5:
                    return 31;
                case 6:
                    return 30;
                case 7:
                    return 31;
                case 8:
                    return 31;
                case 9:
                    return 30;
                case 10:
                    return 31;
                case 11:
                    return 30;
                case 12:
                    return 31;
            }

            return 0;
        }
        public long NextDocNum(string ObjectCode, int Series)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                Query = "SELECT NextNumber FROM NNM1 WHERE ObjectCode = '" + ObjectCode + "' AND Series = " + Series;
                oRecordset.DoQuery(Query);

                return oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public long NextDocEntry(string ObjectCode)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                Query = "SELECT AutoKey FROM ONNM WHERE ObjectCode = '" + ObjectCode + "'";
                oRecordset.DoQuery(Query);

                return oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public string GetEmpName(int EmpId)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                Query = "SELECT IsNull(LastName, '') + ', ' + IsNull(FirstName, '') + ' ' + IsNull(MiddleName, '') " + " FROM OHEM " + " WHERE EmpId = " + EmpId;

                oRecordset.DoQuery(Query);

                return oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                SboApp.SetStatusBarMessage("GetEmpName: " + ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                return "";
            }
        }
        public int GetScalar(string Query, ref int myObject)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                oRecordset.DoQuery(Query);
                if (oRecordset.RecordCount <= 0)
                {
                    myObject = 0;
                    return 0;
                }
                else
                {
                    myObject = oRecordset.Fields.Item(0).Value;
                    return myObject;
                }
            }
            catch (Exception ex)
            {
                myObject = 0;
                return 0;
                //return ("Error, " + ex.Message);
            }
        }
        public string GetNonQuery(string Query)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordset.DoQuery(Query);
                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error, " + ex.Message;
            }
        }
        //public string GetItemRouting(string ItemCode)
        //{
        //    string Query = null;
        //    string Routing = null;

        //    Routing = "N";
        //    Query = "SELECT IsNull(U_Routing, 'N') FROM OITM WHERE ItemCode = '" + ItemCode + "'";

        //    GetScalar(Query, ref  Routing);
        //    return Routing;
        //}
        //public string GetItemDefaultRouting(string ItemCode)
        //{
        //    //string Query = null;
        //    //string Routing = null;

        //    //Routing = "";
        //    //Query = "SELECT IsNull(U_ProcessCode, '') FROM OITM WHERE ItemCode = '" + ItemCode + "'";

        //    //GetScalar(Query, ref Routing);
        //    //return Routing;
        //}
        public string GetItemName(string ItemCode)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Query = "SELECT ItemName FROM OITM WHERE ItemCode = '" + ItemCode + "'";

                oRecordset.DoQuery(Query);

                return oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string GetItemDfltWh(string ItemCode)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Query = "SELECT DfltWh FROM OITM WHERE ItemCode = '" + ItemCode + "' and isnull(DfltWh,'')<>''";

                oRecordset.DoQuery(Query);
                if (oRecordset.RecordCount > 0)
                {
                    return oRecordset.Fields.Item(0).Value;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string GetItmsGrpCod(string ItemCode)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Query = "SELECT ItmsGrpCod FROM OITM WHERE ItemCode = '" + ItemCode + "'";

                oRecordset.DoQuery(Query);

                return oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public string GetCardName(string CardCode)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Query = "SELECT CardName FROM OCRD WHERE CardCode = '" + CardCode + "'";

                oRecordset.DoQuery(Query);

                return oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        //public float GetAvailableQty(string ItemCode, string WhsCode)
        //{
        //    string Query = null;
        //    string Quantity = "0";

        //    Query = "SELECT OnHand FROM OITW " + " WHERE ItemCode = '" + ItemCode + "'" + " AND WhsCode = '" + WhsCode + "'";

        //    if (GetScalar(Query, ref Quantity) == "OK")
        //    {
        //        return float.Parse(Quantity);
        //    }
        //    else
        //    {
        //        return 0;
        //    }
        //}
        //public float GetBatchQty(string ItemCode, string WhsCode, string BatchNum)
        //{
        //    string Query = null;
        //    string Quantity = "0";

        //    Query = "SELECT T0.Quantity " + " FROM OBTQ T0 " + " INNER JOIN OBTN T1 ON T0.ItemCode = T1.ItemCode AND T0.SysNumber = T1.SysNumber " + " WHERE T0.ItemCode = '" + ItemCode + "'" + " AND T0.WhsCode = '" + WhsCode + "'" + " AND T1.DistNumber = '" + BatchNum + "'";

        //    if (GetScalar(Query, ref Quantity) == "OK")
        //    {
        //        return float.Parse(Quantity);
        //    }
        //    else
        //    {
        //        return 0;
        //    }
        //}
        public float FirstLoadQty(string ItemCode)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                Query = "SELECT U_FirstLoad FROM OITM WHERE ItemCode = '" + ItemCode + "'";
                oRecordset.DoQuery(Query);

                if (oRecordset.RecordCount <= 0)
                {
                    return -1;
                }
                else
                {
                    return oRecordset.Fields.Item(0).Value;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        //public string IsBatchManaged(string ItemCode)
        //{
        //    string Query = null;
        //    string Result = "";

        //    Query = "SELECT ManBtchNum FROM OITM WHERE ItemCode = '" + ItemCode + "' and ManBtchNum='Y'";
        //    if (GetScalar(Query, ref  Result) == "OK")
        //    {
        //        return Result;
        //    }
        //    else
        //    {
        //        return "-1";
        //    }
        //}
        //public object IsSerialManaged(string ItemCode)
        //{
        //    string Query = null;
        //    string Result = null;

        //    Result = "N";
        //    Query = "SELECT ManSerNum FROM OITM WHERE ItemCode = '" + ItemCode + "'";
        //    if (GetScalar(Query, ref Result) == "OK")
        //    {
        //        return Result;
        //    }
        //    else
        //    {
        //        return "N";
        //    }
        //}
        //public string IsExcisable(string ItemCode)
        //{
        //    string Query = null;
        //    string Result = "";

        //    Query = "SELECT Excisable FROM OITM WHERE ItemCode = '" + ItemCode + "'";
        //    if (GetScalar(Query, ref Result) == "OK")
        //    {
        //        return Result;
        //    }
        //    else
        //    {
        //        return "-1";
        //    }
        //}
        public object GetSeries(SAPbobsCOM.BoObjectTypes ObjType, string WhsPrefix, System.DateTime DocDate)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                if (WhsPrefix == "DTA")
                {
                    WhsPrefix = "DBS";
                }

                Query = "SELECT T0.Series " + " FROM NNM1 T0 " + " INNER JOIN OFPR T1 ON T0.Indicator = T1.Indicator " + " WHERE ObjectCode = '" + ObjType + "'" + " AND U_SegCode = '" + WhsPrefix + "'" + " AND '" + DocDate.ToString("MM-dd-yyyy") + "' >= T1.F_RefDate " + " AND '" + DocDate.ToString("MM-dd-yyyy") + "' <= T1.T_RefDate ";

                oRecordset.DoQuery(Query);
                return oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public object GetSeriesName(SAPbobsCOM.BoObjectTypes ObjType, string Series)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                Query = "SELECT SeriesName " + " FROM NNM1 " + " WHERE ObjectCode = '" + ObjType + "'" + " AND Series = '" + Series + "'";

                oRecordset.DoQuery(Query);
                return oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        //public string GetAcctCode(string Segment_0, string Segment_1)
        //{
        //    string Query = null;
        //    string Result = "";

        //    Query = "SELECT AcctCode " + " FROM OACT " + " WHERE Segment_0 = '" + Segment_0 + "'" + " AND Segment_1 = '" + Segment_1 + "'";
        //    if (GetScalar(Query, ref Result) == "OK")
        //    {
        //        return Result;
        //    }
        //    else
        //    {
        //        return "-1";
        //    }
        //}
        //public long LastBatchNum()
        //{
        //    string Query = null;
        //    string Result = "";

        //    Query = "SELECT TOP 1 U_LastBatchNum " + " FROM [@PSET] ";

        //    if (GetScalar(Query, ref Result) == "OK")
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return -1;
        //    }
        //}
        public void UpdateLastBatch(long BatchNum)
        {
            string Query = null;
            string Result = "";

            Query = "UPDATE [@PSET] " + " SET U_LastBatchNum = " + BatchNum;

            if (GetNonQuery(Query) == "Ok")
            {
                return;
            }
        }
        public void ShowCrystalReport(string ReportId, string[] Parameters)
        {
            string Query = null;
            string MenuId = "";

            SAPbouiCOM.Form oForm = default(SAPbouiCOM.Form);

            try
            {
                Query = "SELECT MenuUid " + " FROM OCMN " + " WHERE ObjectKey = '" + ReportId + "'" + " AND ObjectType = '232'";

                if (GetScalar(Query, ref MenuId) != "OK")
                {
                    throw new Exception("Menu Id not found for report.");
                }

                SboApp.ActivateMenuItem(MenuId);
                oForm = SboApp.Forms.ActiveForm;

                int PFlag = 0;
                for (int flag = 0; flag <= oForm.Items.Count - 1; flag++)
                {
                    if (oForm.Items.Item(flag).Type == BoFormItemTypes.it_EDIT & oForm.Items.Item(flag).Visible == true)
                    {
                        oForm.Items.Item(flag).Specific.Value = Parameters[PFlag];
                        PFlag += 1;
                    }
                }

                oForm.Items.Item(0).Click(BoCellClickType.ct_Regular);
            }
            catch (Exception ex)
            {
                SboApp.SetStatusBarMessage("ShCry: " + ex.Message, BoMessageTime.bmt_Short, true);
            }
        }

        private string GetScalar(string Query, ref string myObject)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                oRecordset.DoQuery(Query);
                if (oRecordset.RecordCount <= 0)
                {
                    myObject = "";
                    return "No Records";
                }
                else
                {
                    myObject = Convert.ToString( oRecordset.Fields.Item(0).Value);
                    return "OK";
                }
            }
            catch (Exception ex)
            {
                myObject = "";
                return ("Error, " + ex.Message);
            }
        }
        public float GetPackFactor(string PackingCode)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                Query = "SELECT IsNull(U_Factor, 1) " + " FROM OITT " + " WHERE Code = '" + PackingCode + "'";

                oRecordset.DoQuery(Query);
                return oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return 1;
            }
        }
        public float GetProcessTime(SAPbobsCOM.BoObjectTypes DocType, long DocEntry, int LineNum, string TimeFormula)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string TimeQuery = "";

            try
            {
                oRecordset = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);

                if (DocType == BoObjectTypes.oQuotations)
                {
                    TimeQuery = "SELECT " + TimeFormula + " FROM QUT1 WHERE DocEntry = " + DocEntry + " AND LineNum = " + LineNum;
                }
                else if (DocType == BoObjectTypes.oOrders)
                {
                    TimeQuery = "SELECT " + TimeFormula + " FROM RDR1 WHERE DocEntry = " + DocEntry + " AND LineNum = " + LineNum;
                }

                oRecordset.DoQuery(TimeQuery);
                return (oRecordset.Fields.Item(0).Value);
            }
            catch (Exception Ex)
            {
                return 0;
            }
        }
        public string GetUserCode(int UserSign)
        {
            string Query = null;
            string Name = null;
            string Result = null;

            Query = "SELECT User_Code " + " FROM OUSR " + " WHERE Internal_K = " + UserSign;
            Result = GetScalar(Query, ref Name);
            if (Result == "OK")
            {
                return Name;
            }
            else
            {
                return "";
            }
        }
        public int GetUserSign(string UserCode)
        {
            string Query = null;
            string Result = null;
            string Name = "0";

            Query = "SELECT UserID " + " FROM OUSR " + " WHERE User_Code = '" + UserCode + "'";
            Result = GetScalar(Query, ref Name);

            if (Result == "OK")
            {
                return int.Parse(Name);
            }
            else
            {
                return 1;
            }
        }
        public string IsSuperUser(int UserSign)
        {
            string Query = null;
            string Result = null;

            Query = "SELECT SuperUser FROM OUSR WHERE Internal_K = " + UserSign;
            GetScalar(Query, ref  Result);

            return Result;
        }
        public string ContractorWhs(string CardCode)
        {
            string Query = null;
            string WhsCode = null;

            Query = "SELECT U_ContractorWhs FROM OCRD WHERE CardCode = '" + CardCode + "'";

            GetScalar(Query, ref WhsCode);
            return WhsCode;
        }
        public string TreeType(string ItemCode)
        {
            string Query = null;
            string ItemTree = null;

            Query = "SELECT TreeType FROM OITM WHERE ItemCode = '" + ItemCode + "'";

            GetScalar(Query, ref ItemTree);
            return ItemTree;
        }
        public object LastPurPrice(string TableName, string ItemCode, string MatchCol, string MatchValue)
        {
            string Query = null;
            string Price = null;

            Query = "SELECT Price " + " FROM " + TableName + " WHERE ItemCode = '" + ItemCode + "'" + " AND " + MatchCol + " = '" + MatchValue + "'";

            Price = "0";
            GetScalar(Query, ref Price);

            return Price;
        }
        public string GetDimDesc(int DimCode)
        {
            string Query = null;
            string Result = null;

            Result = "";
            Query = "SELECT DimDesc FROM ODIM WHERE DimCode = " + DimCode;
            GetScalar(Query, ref Result);

            return Result;
        }
        public string GetDimActive(int DimCode)
        {
            string Query = null;
            string Result = null;

            Result = "";
            Query = "SELECT DimActive FROM ODIM WHERE DimCode = " + DimCode;
            GetScalar(Query, ref Result);

            return Result;
        }
        public int GetEmpId(int UserSign)
        {
            string Query = null;
            string Result = null;

            Result = "-1";
            Query = "SELECT EmpId FROM OHEM WHERE UserId = " + UserSign;
            GetScalar(Query, ref Result);

            return int.Parse(Result);
        }
        public int GetManager(int EmpId)
        {
            string Query = null;
            string Result = null;

            Result = "-1";
            Query = "SELECT Manager FROM OHEM WHERE EmpId = " + EmpId;
            GetScalar(Query, ref Result);

            return int.Parse(Result);
        }
        public int GetUserId(int EmpId)
        {
            string Query = null;
            string Result = null;

            Result = "-1";
            Query = "SELECT UserId FROM OHEM WHERE EmpId = " + EmpId;
            GetScalar(Query, ref Result);

            return int.Parse(Result);
        }
        public float GetPrice(string ItemCode)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;

            float Price = 0;

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                //first check last purchase price
                Query = "SELECT LastPurPrc FROM OITM WHERE ItemCode = '" + ItemCode + "'";
                oRecordset.DoQuery(Query);

                Price = oRecordset.Fields.Item(0).Value;
                if (Price != 0)
                {
                    return Price;
                }

                Query = "SELECT LstEvlPric FROM OITM WHERE ItemCode = '" + ItemCode + "'";
                oRecordset.DoQuery(Query);

                Price = oRecordset.Fields.Item(0).Value;
                if (Price != 0)
                {
                    return Price;
                }

                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public float GetPrice(string Father, string Child)
        {
            SAPbobsCOM.Recordset oRecordset = default(SAPbobsCOM.Recordset);
            string Query = null;

            float Price = 0;
            int PriceList = 0;

            try
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Query = "SELECT * FROM ITT1 " + " WHERE Father = '" + Father + "'" + " AND Code = '" + Child + "'";

                oRecordset.DoQuery(Query);
                if (oRecordset.RecordCount > 0)
                {
                    PriceList = oRecordset.Fields.Item("PriceList").Value;
                }
                else
                {
                    PriceList = 0;
                }

                Price = oRecordset.Fields.Item("Price").Value;
                if (Price != 0)
                {
                    return oRecordset.Fields.Item("Price").Value;
                }

                if (PriceList == 0)
                {
                    return 0;
                }
                else if (PriceList == -1)
                {
                    Query = "SELECT LastPurPrc FROM OITM WHERE ItemCode = '" + Child + "'";
                }
                else if (PriceList == -2)
                {
                    Query = "SELECT LstEvlPric FROM OITM WHERE ItemCode = '" + Child + "'";
                }
                else
                {
                    Query = "SELECT Price FROM ITM1 WHERE ItemCode = '" + Child + "' AND PriceList = " + PriceList;
                }

                oRecordset.DoQuery(Query);
                return oRecordset.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public float GetBomFactor(string ItemCode)
        {
            string Query = null;
            string Result = "1";

            try
            {
                Query = "SELECT IsNull(Qauntity, 1) FROM OITT WHERE Code = '" + ItemCode + "'";
                GetScalar(Query, ref Result);

                return float.Parse(Result);
            }
            catch (Exception Ex)
            {
                return (1);
            }
        }
        public string ImagePath()
        {
            string Query = null;
            string Result = null;

            Result = "";
            Query = "SELECT TOP 1 BitmapPath FROM OADP";
            GetScalar(Query, ref Result);

            return Result;
        }
        public string GetItemImage(string ItemCode)
        {
            string Query = null;
            string Result = null;

            Result = "";
            Query = "SELECT IsNull(PicturName, '') FROM OITM WHERE ItemCode = '" + ItemCode + "'";
            GetScalar(Query, ref Result);

            if (string.IsNullOrEmpty(Result))
            {
                return "";
            }
            else
            {
                return ImagePath() + "\\" + Result;
            }
        }
        public object GetBatchQtyBySys(string ItemCode, string WhsCode, string SysNumber)
        {
            string Query = null;
            string Result = "0";

            Query = "SELECT Quantity " + " FROM OBTQ " + " WHERE ItemCode = '" + ItemCode + "'" + " AND WhsCode = '" + WhsCode + "'" + " AND SysNumber = '" + SysNumber + "'";

            GetScalar(Query, ref Result);
            return Result;
        }
        public double GetGlobalDoubleValue(string str)
        {
            double price;
            double.TryParse(str,   out price);
            return price;
        }
        public int GetGlobalIntValueWithZeroDecimal(string str)
        {
            int price;
            int.TryParse(str, System.Globalization.NumberStyles.Number, System.Globalization.NumberFormatInfo.InvariantInfo, out price);
            return price;
        }
        public void loadform(string filename)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(System.Windows.Forms.Application.StartupPath + "\\" + filename);
                SboApp.LoadBatchActions(SetFormUId(xmldoc));
            }
            catch (Exception ex)
            {
            }

        }
        public string SetFormUId(XmlDocument oXMLDoc)
        {
            try
            {
                foreach (XmlNode node in oXMLDoc.DocumentElement.SelectNodes("*"))
                {
                    string StartNodePathe;
                    XmlNode Node1;
                    StartNodePathe = node.Name;
                    Node1 = node.ParentNode;
                    StartNodePathe = Node1.Name + "/" + StartNodePathe + "/action/form";
                    Node1 = oXMLDoc.SelectSingleNode(StartNodePathe);
                    string a = DateTime.Now.Ticks.ToString("X16");
                    Node1.Attributes["uid"].Value = DateTime.Now.Ticks.ToString("X16");
                }
                return oXMLDoc.InnerXml;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public void FillCombo11(SAPbouiCOM.ComboBoxColumn Ocombo, string SqlQuery, string ValFld, string DescFld)
        {
            SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
            oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRs.DoQuery(SqlQuery);
            for (int i = 0; i <= oRs.RecordCount - 1; i++)
            {
                Ocombo.ValidValues.Add(oRs.Fields.Item(ValFld).Value, oRs.Fields.Item(DescFld).Value);
                oRs.MoveNext();
            }
        }
        public bool WhsWithBin(string WhsCode)
        {
            SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
            Sql = "select * from OWHS where BinActivat='Y' and WhsCode ='" + WhsCode + "'";
            oRs = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRs.DoQuery(Sql);
            if (oRs.RecordCount > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public double RemoveThousandseparator(string Val)
        {

            SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
            Sql = "select ThousSep  from OADM";
            oRs = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRs.DoQuery(Sql);
            if ((oRs.Fields.Item(0).Value.tostring()) == false)
            {
                Val = System.Convert.ToDouble(Val.Replace(oRs.Fields.Item(0).Value, ""), CultureInfo.InvariantCulture);
            }
            else
            {
                Val = System.Convert.ToString(double.Parse(Val), CultureInfo.InvariantCulture);
            }
            return double.Parse(Val);
        }

        public double  GetResourcePrice(string ResouceCode)
        {
            try
            {
                SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
                Sql = "Select stdCost1+stdCost2+stdCost3+stdCost4+stdCost5+stdCost6+stdCost7+stdCost8+stdCost9+stdCost10 from ORSC where ResCode='" + ResouceCode  + "'";
            
                oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRs.DoQuery(Sql);
                if (oRs.RecordCount > 0)
                {
                    return (oRs.Fields.Item(0).Value);
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception)
            {
                return 0;                
            }

        }

        public double GetBasePrice(string ItemCode)
        {
            SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
            Sql = " select Price from ITM1 where ItemCode='" + ItemCode + "' and priceList=1";
            oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRs.DoQuery(Sql);
            if (oRs.RecordCount > 0)
            {
                return (oRs.Fields.Item(0).Value);
            }
            else
            {
                return 0;
            }
        }
        public double GetAvgPrice(string ItemCode, string WhsCode)
        {
            SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
            if (costbywhsflag == false)
            {
                Sql = "select avgprice from oitm where itemcode = '" + ItemCode + "' ";
            }
            else
            {
                Sql = "select b.AvgPrice from OITM a left outer join OITW b on a.Itemcode=b.itemcode where a.ItemCode='" + ItemCode + "' and b.WhsCode='" + WhsCode + "'";
            }

            oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRs.DoQuery(Sql);
            if (oRs.RecordCount > 0)
            {
                return (oRs.Fields.Item(0).Value);
            }
            else
            {
                return 0;
            }
        }
        public double GetAvgPrice2(string ItemCode, string WhsCode)
        {
            SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
            if (costbywhsflag == false)
            {
                Sql = "select avgprice from oitm where itemcode = '" + ItemCode + "' ";
            }
            else
            {
                Sql = "select b.AvgPrice from OITM a left outer join OITW b on a.Itemcode=b.itemcode where a.ItemCode='" + ItemCode + "' and b.WhsCode='" + WhsCode + "'";
            }

            oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRs.DoQuery(Sql);
            if (oRs.RecordCount > 0)
            {
                return System.Convert.ToDouble(oRs.Fields.Item(0).Value);
            }
            else
            {
                return 0;
            }
        }
        public bool UserGroupAuthorization(string UserId, string AuthId)
        {
            SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
            Sql = "select PERMISSION from UGR1 a inner join USr7 b on a.GroupLink =b.GroupId where a.PermId ='" + AuthId + "' and b.UserId ='" + UserId + "'";
            oRs = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRs.DoQuery(Sql);
            if (oRs.RecordCount > 0)
            {
                if (oRs.Fields.Item(0).Value == "F")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
        public double tryDoubleParse(string inputno)
        {
            try
            {
                inputno=inputno.Replace(SepDec, CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator);
                double qty = System.Convert.ToDouble(inputno, CultureInfo.InvariantCulture);
                //Double.TryParse(inputno, NumberStyles.Number, CultureInfo.CurrentCulture, qty)
                return qty;
            }
            catch (Exception ex)
            {
                return 0;
            }

        }
        public string decimalreturn(string inputno)
        {
            if ((inputno.Contains(",") == true))
            {
                return inputno.Replace(",", SepDec);
            }
            else if ((inputno.Contains(".") == true))
            {
                return inputno.Replace(".", SepDec);
            }
            else
            {
                return inputno;
            }
        }
        public string decimalreturn(string inputno, bool userfield)
        {
            //Dim output As String = inputno.ToString(CultureInfo.InvariantCulture)

            if ((inputno.Contains(",") == true))
            {
                return inputno.Replace(",", ".");
            }
            else
            {
                return inputno.ToString();
            }
        }
        public string decimalreturn1(double inputno)
        {

            return inputno.ToString(CultureInfo.InvariantCulture);
        }
        public void linktobatch(string batchid)
        {
            int LineNo = 0;

            try
            {
                SboApp.ActivateMenuItem("12290");
                LineNo = 1;
                //Dim batchform As SAPbouiCOM.Form = obe1.SboApp.Forms.ActiveForm
                SAPbouiCOM.Form batchform = SboApp.Forms.GetForm("65053", -1);
                LineNo = 2;

                if (batchform.Mode != SAPbouiCOM.BoFormMode.fm_FIND_MODE)
                {
                    LineNo = 3;
                    batchform.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE;
                    LineNo = 4;
                }
                SAPbouiCOM.EditText editext = batchform.Items.Item("62").Specific;
                LineNo = 5;
                editext.Value = batchid.ToString();
                LineNo = 6;
                batchform.Items.Item("37").Click();
                LineNo = 7;

            }
            catch (Exception ex)
            {
                oMsgShow.Messsage("Line No: " + (LineNo.ToString().Trim()) + " Active Batch Exception : " + ex.Message.ToString(), MsgShow.MsgType.Statusbar, MsgShow.MsgCat.Errors);

            }
            finally
            {
            }
        }
        public void FillCombo(SAPbouiCOM.ComboBox fillcombo, string sqltext, string field1, string field2, bool norefresh)
        {
            try
            {
                // BPN 07/23/2014
                // fill combo info by SQL statement. Combo will always have a empty empty as the first select to allow user select null value 
                if ((norefresh == true & fillcombo.ValidValues.Count > 0))
                {
                    return;
                }

                SAPbobsCOM.Recordset orecord = default(SAPbobsCOM.Recordset);
              
              
                orecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                orecord.DoQuery(sqltext);
                while ((fillcombo.ValidValues.Count-1 > 0))
                {
                    fillcombo.ValidValues.Remove(fillcombo.ValidValues.Count-1, SAPbouiCOM.BoSearchKey.psk_Index);
                }
                
                //fillcombo.ValidValues.Add("", "");
                for (int i = 0; i <= orecord.RecordCount - 1; i++)
                {
                    try
                    {
                        fillcombo.ValidValues.Add(orecord.Fields.Item(field1).Value.ToString(), orecord.Fields.Item(field2).Value.ToString());
                    }
                    catch (Exception)
                    {
                    }                    
                    orecord.MoveNext();
                }
                try
                {
                    fillcombo.Select("", BoSearchKey.psk_ByValue);
                }
                catch (Exception) { }


            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText(ex.Message.ToString(), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
            }
        }
        public void setitemds(SAPbouiCOM.Form oForm, string itemcode, string objtype, SAPbouiCOM.BoDataType otype, int size)
        {
            SAPbouiCOM.EditText oedit = default(SAPbouiCOM.EditText);
            SAPbouiCOM.OptionBtn oOptbtn = default(SAPbouiCOM.OptionBtn);
            SAPbouiCOM.ComboBox ocombo = default(SAPbouiCOM.ComboBox);
            SAPbouiCOM.CheckBox ocheck = default(SAPbouiCOM.CheckBox);
            SAPbouiCOM.ButtonCombo oCbutton = default(SAPbouiCOM.ButtonCombo);
            switch ((objtype))
            {

                case "Edit":
                    oForm.DataSources.UserDataSources.Add(itemcode + "ds", otype, size);
                    oedit = (oForm.Items.Item(itemcode).Specific);
                    oedit.DataBind.SetBound(true, "", itemcode + "ds");

                    break;
                case "Optbtn":
                    oForm.DataSources.UserDataSources.Add(itemcode + "ds", otype, size);
                    oOptbtn = (oForm.Items.Item(itemcode).Specific);
                    oOptbtn.DataBind.SetBound(true, "", itemcode + "ds");
                    break;
                case "Combo":
                    oForm.DataSources.UserDataSources.Add(itemcode + "ds", otype, size);
                    ocombo = (oForm.Items.Item(itemcode).Specific);
                    ocombo.DataBind.SetBound(true, "", itemcode + "ds");

                    break;
                case "Check":
                    oForm.DataSources.UserDataSources.Add(itemcode + "ds", otype, size);
                    ocheck = (oForm.Items.Item(itemcode).Specific);
                    ocheck.DataBind.SetBound(true, "", itemcode + "ds");

                    break;
                case "ComboButton":
                    oForm.DataSources.UserDataSources.Add(itemcode + "ds", otype, size);
                    oCbutton = (oForm.Items.Item(itemcode).Specific);
                    oCbutton.DataBind.SetBound(true, "", itemcode + "ds");

                    break;
            }

        }
        public System.Data.DataTable GetDataTable(string sql)
        {


            SqlCommand cmdMain;// = default(System.Data.SqlClient.SqlCommand);
            SqlDataAdapter dataAdapt;

            try
            {
                try
                {
                    if (connobj.State == ConnectionState.Closed)
                    {
                        ADO_Connection();
                    }
                }
                catch (Exception ex)
                {
                    ADO_Connection();
                }


                System.Data.DataTable odtMain = new System.Data.DataTable();
                dataAdapt = new SqlDataAdapter();
                cmdMain = new SqlCommand(sql, connobj);
                dataAdapt.SelectCommand = cmdMain;
                dataAdapt.Fill(odtMain);
                return odtMain;

            }
            catch (Exception ex)
            {

                return null;
            }

        }
        public int ADONonExeSql(string sql)
        {
            System.Data.SqlClient.SqlCommand cmdMain = default(System.Data.SqlClient.SqlCommand);
            try
            {
                try
                {
                    if (connobj.State == ConnectionState.Closed)
                    {
                        ADO_Connection();
                    }
                }
                catch (Exception ex)
                {
                    ADO_Connection();
                }

                cmdMain = new SqlCommand(sql, connobj);
                return cmdMain.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
            }
        }

        public void ADO_Connection()
        {
            SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
            // Dim oDtTbl As New DataTable
            try
            {
                connobj.Close();

            }
            catch (Exception ex)
            {
            }
            oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            Sql = "Select * from sys.Tables where Name ='B1S_MetaDataLog'";
            oRs.DoQuery(Sql);
            if (oRs.RecordCount > 0)
            {
                Sql = "Select top 1 usr, pass from B1S_MetaDataLog order by ID Desc";
                oRs.DoQuery(Sql);
                //oDtTbl = CCUtility.GetDataTable(Sql)
                if (oRs.RecordCount > 0)
                {
                    connobj = new SqlConnection();
                    connobj.ConnectionString = "server=" + oCompany.Server.ToString() + ";uid=" + DecryptString(oRs.Fields.Item(0).Value.ToString()) + ";pwd=" + DecryptString(oRs.Fields.Item(1).Value.ToString()) + ";database=" + oCompany.CompanyDB.ToString();
                    connobj.Open();

                    if ((connobj.State != ConnectionState.Open))
                    {

                        //oMsgShow.Messsage("[87 |Using ADO Cannot Connect to Database]: " + oCompany.CompanyDB.ToString(), MsgShow.MsgType.PopUp);
                        _B1SLogInfo oLogInfo = new _B1SLogInfo();
                        oLogInfo.populateEditText("Please Input SQL User Name & password", new string[] { "UserName", "Password" }, new string[] { "sa", "" }, 2);
                        
                    }
                }
                else
                {
                    _B1SLogInfo oLogInfo = new _B1SLogInfo();
                    oLogInfo.populateEditText("Please Input SQL user, password", new string[] { "UserName", "Password" }, new string[] { "sa", "" }, 2);
                    
                }
            }
            else
            {
                _B1SLogInfo oLogInfo = new _B1SLogInfo();
                oLogInfo.populateEditText("Please Input SQL user, password", new string[] { "UserName", "Password" }, new string[] { "sa", "" }, 2);
                
            }
        }

        public string EncryptData(string Message)
        {
            byte[] Results = null;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes("be1s"));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToEncrypt = UTF8.GetBytes(Message);
            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return Convert.ToBase64String(Results);
        }

        public string DecryptString(string Message)
        {

            byte[] Results = null;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes("be1s"));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToDecrypt = Convert.FromBase64String(Message);

            try
            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }
            catch (Exception ex)
            {
                oMsgShow.Messsage("License Decrypt :" + ex.Message, MsgShow.MsgType.Statusbar, MsgShow.MsgCat.Errors);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return UTF8.GetString(Results);
        }

        public static string DecryptStringForMeta(string Message)
        {

            byte[] Results = null;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes("be1s"));
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            byte[] DataToDecrypt = Convert.FromBase64String(Message);

            try
            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }
            catch (Exception ex)
            {
                oMsgShow.Messsage("License Decrypt :" + ex.Message, MsgShow.MsgType.Statusbar, MsgShow.MsgCat.Errors);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return UTF8.GetString(Results);
        }

        public string  GetMenuID(String TABLEID)
        {
            string  MenuId = "";
            SAPbouiCOM.Menus oMenus;
            SAPbouiCOM.MenuItem oMenuItem;
            oMenuItem = SboApp.Menus.Item("51200");
            oMenus = oMenuItem.SubMenus;
            for (int i = 0; i< oMenus.Count;i++)
            {
                if (oMenus.Item(i).String.Contains(TABLEID)==true )
                {
                    MenuId = oMenus.Item(i).UID;
                    i=oMenus .Count +2; 
                }
            }
            return MenuId;
        }

         [STAThread]
        public string showOpenFileDialog()
        {
            //OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = @"C:\";
            //openFileDialog1.Title = "Select a File";
            //openFileDialog1.Filter = "Excel File|*.xls";
            string selectedPath = "";
            var t = new Thread((ThreadStart)(() =>
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();

                openFileDialog1.InitialDirectory = @"C:\";
                openFileDialog1.Title = "Select a File";
                openFileDialog1.Filter = "Image Files|*.jpg;*.jpeg;*.png;*.gif;*.tif;...";

                DialogResult DlRs=openFileDialog1.ShowDialog();
                if (DlRs == DialogResult.Cancel)
                {
                    return;
                }
                selectedPath = openFileDialog1.FileName;
              
            }));

            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
            return selectedPath;

            //if (openFileDialog1.ShowDialog() != DialogResult.Cancel)
            //{
            //    return  openFileDialog1.FileName;
            //}
            //else
            //{
            //    return "";
            //}            
        }

         public static void PrintDocument(int p,string LayoutCode)
         {
             try
             {
                 string test = "";
                 bool printflag = true;
                 int printcopy = 1;
                 SAPbobsCOM.CompanyService oCmpSrv;
                 SAPbobsCOM.ReportLayoutsService oReportLayoutService;
                 SAPbobsCOM.ReportLayoutPrintParams oPrintParam;
                 oCmpSrv = oCompany.GetCompanyService();

                 oReportLayoutService = (SAPbobsCOM.ReportLayoutsService)oCmpSrv.GetBusinessService(SAPbobsCOM.ServiceTypes.ReportLayoutsService);
                 SAPbobsCOM .Recordset  orecord = (SAPbobsCOM.Recordset)(Global.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset));
                 orecord.DoQuery("select isnull(NumCopy,1) from RDOC where doccode = '" + LayoutCode + "'");
                 if (orecord.RecordCount > 0)
                 {
                     if (printflag == true)
                     {
                         try
                         {
                             test = orecord.Fields.Item(0).Value.ToString();
                             printcopy = System.Convert.ToInt16(test);
                         }
                         catch
                         { }

                         oPrintParam = (SAPbobsCOM.ReportLayoutPrintParams)oReportLayoutService.GetDataInterface(SAPbobsCOM.ReportLayoutsServiceDataInterfaces.rlsdiReportLayoutPrintParams);
                         oPrintParam.LayoutCode = LayoutCode;
                         oPrintParam.DocEntry = p;
                         for (int i = 0; i < printcopy; i++)
                             oReportLayoutService.Print(oPrintParam);

                        
                     }
                     //Global.printed = 1;
                 }
                 else
                 {
                     //Global.BPERR = Global.BPERR + "(" + DateTime.Now.ToString() + ") " + "Layout Code " + LayoutCode + " Doesn't exist in system";
                     //Global.printed = -1;
                 }
             }
             catch (Exception ex)
             {
                 //Global.BPERR = Global.BPERR + "(" + DateTime.Now.ToString() + ") " + "Exception : " + ex.Message.ToString();
                 //Global.printed = -1;

             }
         }
         public bool Check_Menu_Athorization(string MenuId)
         {
             try
             {
                 //return true;
                 SAPbobsCOM.Users oUser = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUsers);
                 oUser.GetByKey(oCompany.UserSignature);
                 
                 if ((oUser.Superuser == SAPbobsCOM.BoYesNoEnum.tYES))
                 {
                     return true; 
                 }
                 else
                 {
                     
                     for (int i = 0; i <= oUser.UserPermission.Count - 1; i++)
                     {
                         
                         oUser.UserPermission.SetCurrentLine(i);
                         
                         if ((oUser.UserPermission.PermissionID == MenuId))
                         {
                             if ((oUser.UserPermission.Permission == SAPbobsCOM.BoPermission.boper_Full || UserGroupAuthorization(oUser.InternalKey, MenuId) == true))
                             {
                                 return true;
                             }
                             else
                             {
                                 return false;
                             }
                         }
                     }
                     return true;
                 }
             }
             catch (Exception)
             {
                 return false;   
             }
         }
         public bool UserGroupAuthorization(int UserId, string AuthId)
         {
             SAPbobsCOM.Recordset oRs = default(SAPbobsCOM.Recordset);
             Sql = "select PERMISSION from UGR1 a inner join USr7 b on a.GroupLink =b.GroupId where a.PermId ='" + AuthId + "' and b.UserId ='" + UserId + "'";
             oRs = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
             oRs.DoQuery(Sql);
             if (oRs.RecordCount > 0)
             {
                 if (oRs.Fields.Item(0).Value == "F")
                 {
                     return true;
                 }
                 else
                 {
                     return false;
                 }
             }
             else
             {
                 return false;
             }

         }
         public string CheckCountry(string country)
         {
             SAPbobsCOM.Recordset oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
             Sql = "Select Code,Name from [OCRY] where code='" + country +"'";
             oRs.DoQuery(Sql);
                 if (oRs.RecordCount >0)
                 {
                     return oRs.Fields.Item (0).Value ;
                 }
             Sql = "Select Code,Name from [OCRY] where Name='" + country +"'";
             oRs.DoQuery (Sql);
                 if (oRs.RecordCount >0)
                 {
                     return oRs.Fields.Item (0).Value ;
                 }

             return "";
         }
         public string CheckState(string State,string Country)
         {
             SAPbobsCOM.Recordset oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
             Sql = "Select Code,Name from [OCST] where Country='" + Country + "' and code='" + State + "'";
             oRs.DoQuery(Sql);
             if (oRs.RecordCount > 0)
             {
                 return oRs.Fields.Item(0).Value;
             }
             Sql = "Select Code,Name from [OCST] where Country='" + Country + "' and Name='" + State + "'";
             oRs.DoQuery(Sql);
             if (oRs.RecordCount > 0)
             {
                 return oRs.Fields.Item(0).Value;
             }

             return "";
         }

         public static bool ValidateMetaData()
         {
             try
             {
                 string result;
                 SAPbobsCOM.Recordset oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                 oRs.DoQuery("select count(*) from sys.objects where type = 'U' and name = 'B1S_MetaDataLog'");
                 if (oRs.Fields.Item(0).Value > 0)
                 {
                     oRs.DoQuery("Select top 1  isnull(MetaStatus, ''), MetaMsg from  B1S_MetaDataLog order by ID desc");
                     if (oRs.RecordCount > 0)
                     {
                         result = oRs.Fields.Item(0).Value;
                         
                     }
                     else { result = ""; }

                     if (result != "C")
                     {
                         return true;                    
                     }
                     
                     else
                     {
                        SboApp.MessageBox("Setup failure : " + oRs.Fields.Item(0).Value + " , Addon will now disconnect, please follow user menu for setup process before restart add-on : ");
                         return false;
                     }
                    
                 }
                 else
                 {
                     SboApp.MessageBox("Setup failure : Addon will now disconnect, please follow user menu for setup process before restart add-on : ");
                     return false;                
                 }
             }
             catch (System.Exception ex)
             {
                 SboApp.MessageBox("ValidateMetaData : " + ex.Message.ToString());
                 return false;
             }
            
         }


        #endregion
    }
}


