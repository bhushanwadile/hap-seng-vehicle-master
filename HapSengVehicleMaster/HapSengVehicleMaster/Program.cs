﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HapSengVehicleMaster
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Global.obe1sUi = new be1_UIConnection(); // Create Pre Difine Class objec
            Global.obe1sUi.SetApplication("Vehicle Master"); // SetApplication Method set UI connection & we pass the Add on name for the massages refrancess 

            //bool Flag = Global.ValidateMetaData();
            bool Flag = true;
            if (Flag == false)
            {
                System.Windows.Forms.Application.Exit();
                return;
            }
            else
            {

                Global.oMsgShow = new MsgShow();

                // This is meny list we store into this array, which menu active or use with the specific for,
                // For the handled the menu event related to purtucal form. we use this array.
                // For example we required some fuctionality on Item master form with the find menu.
                // In this case what is the logic for this we impliment in the _150 class.
                // but SBO event call the methoed of this call on find menu click. with the help of his array.
                Global.StdMenuArray = new string[] { "1281", "1288", "1289", "1290", "1291", "1292", "1304", "1283", "1284", "1285", "1286", "1287", "1301", "1292", "1293", "1299", "1294", "1295", "1296", "4870", "4869", "5896" };

                // event filter 
                Global.FormArray = new string[] { "BE1SVM", "Details", "B1SLogInfo", "CreateSQ", "ShowDetails", "ShowDetails2", "ShowDetails_button", "167", "169" };
                Global.obe1sUi.eventFilter(Global.FormArray);
                // what form you use in the Add On this form you pass in the array for the set event filter for this class 

                //No Object Form Related User Define Menu
                Global.NoObjectMenuArray = new string[] { "BE1STRNSMODE" };

                Global.ModalFormArray = new string[] { "B1SPriceMstEntry" };

                // Menu Creations
                Global.oMenu = new Menu();
                Global.oMenu.AddAllMenu(); // This is menu creation using XML file (This File with sample you can find in Bin folder of this project.



                //be1_UIConnectio.n.SetEvenSboApp();
                System.Windows.Forms.Application.Run();
            }
        }
    }
}
