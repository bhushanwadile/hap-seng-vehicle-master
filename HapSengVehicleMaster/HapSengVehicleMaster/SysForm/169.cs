﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using HapSengVehicleMaster;

namespace Standex_Quote_Calc
{
    public class _169 :UIDesign 
    {
        
        public int add()
        {
            return 5;
        }
        public void MenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent)
        {
            try
            {
               
                if (pVal .BeforeAction ==false )
                {
                    
                
                    int startIndex=pVal.MenuUID .LastIndexOf ("_")+1;
                    string ClassName=pVal.MenuUID .Substring (startIndex,(pVal .MenuUID .Length -startIndex)); 
                    BubbleEvent=true;
                       try
                       {
                           if (pVal.MenuUID == "BOS_169_VM")
                           {
                              _BE1SVM vm = new _BE1SVM();
                              vm.ShowForm();
                           }
                           if (ClassName == "GF01" && pVal.BeforeAction == false)
                           {
                               //System.IO.File.Open(System.Windows.Forms.Application.StartupPath + "\\SQ_MetaData.exe", System.IO.FileMode.Open);
                               test();
                               return;
                           }
                           if (NoObjectMenuArray.Contains(ClassName) == true)
                           {
                               SboApp.ActivateMenuItem(GetMenuID(ClassName));

                               SAPbouiCOM.Form oForm1 = SboApp.Forms.ActiveForm;
                               string a = oForm1.BusinessObject .Type.ToString () ;
                               return;
                           }

                           SAPbouiCOM.Form oForm = SboApp.Forms.ActiveForm;
                          if (FormArray.Contains(oForm.TypeEx) == true)
                           {
                               var ocls = MagicallyCreateInstance("_" + ClassName);
                               object[] mParam = {};
                               InvokeClassMethod(ocls, ocls.GetType(), "ShowForm", mParam);
                               BubbleEvent = (bool)mParam[1];
                               ocls = null;
                               GC.Collect();
                           }
                           
                       }
                       catch (Exception e)
                       {

                       }
                }
            }
            catch (Exception e)
            { 
            
            }
        }

        private void test()
        {
            // Prepare the process to run
            ProcessStartInfo start = new ProcessStartInfo();
            // Enter in the command line arguments, everything you would enter after the executable name itself
            start.Arguments= "Meta Data"; 
            // Enter the executable to run, including the complete path
            start.FileName = System.Windows.Forms.Application.StartupPath + "\\MetaDataSetup.exe";
            // Do you want to show a console window?
            start.WindowStyle = ProcessWindowStyle.Normal; 
            start.CreateNoWindow = true;
            start.Verb = "runas";
            int exitCode;


            // Run the external process & wait for it to finish
            using (Process proc = Process.Start(start))
            {
                 proc.WaitForExit();

                 // Retrieve the app's exit code
                 exitCode = proc.ExitCode;
            }
        }
    }
}
