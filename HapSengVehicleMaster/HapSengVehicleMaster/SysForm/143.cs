﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HapSengVehicleMaster.SysForm
{
    class _143 : UIDesign
    {
        SAPbouiCOM.Form oForm;
        SAPbouiCOM.Matrix oMatrix;
        SAPbouiCOM.ComboBox oCombo;
        string cardcode;
        string NumAtCard;
        string LineNo;
        string ItemCode;
        string quantity;
        string NumPerMsr;
        string LotFrom;
        string LotTo;
        public void ShowForm()
        {

        }
        public void EventHandler(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            try
            {
                if (pVal.EventType != SAPbouiCOM.BoEventTypes.et_FORM_DEACTIVATE && pVal.EventType != SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
                {
                    string s = pVal.EventType.ToString();
                }

                if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
                {
                    FormLoad(pVal, BubbleEvent);
                }
                if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_CLICK)
                {
                    ItemClick(pVal,out BubbleEvent);
                }
                if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                {
                    ComboSelect(pVal, BubbleEvent);
                }
                if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
                {
                    FormClose(pVal, BubbleEvent);
                }

            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Event Handler. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        private void FormClose(SAPbouiCOM.ItemEvent pVal, bool BubbleEvent)
        {
            try
            {
                //dt.Clear();
            }
            catch (Exception ex)
            {
                //SboApp.StatusBar.SetText("Form Close Event. "+ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        public void MenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent)
        {
            try
            {
                //if (pVal.MenuUID == "1284" && pVal.BeforeAction == true)
                //{
                //    SboApp.StatusBar.SetText("GRPO Document cancellation not allowed : ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                //    BubbleEvent = false;
                //    return;
                //}

                if ((pVal.MenuUID == "5896" || pVal.MenuUID == "5894") && pVal.BeforeAction == true)
                {
                    oForm = SboApp.Forms.ActiveForm;
                    oMatrix = oForm.Items.Item("38").Specific;
                    CreateTempFromAndToTable(oCompany.UserName.ToString());

                    cardcode = oForm.Items.Item("4").Specific.Value;
                    NumAtCard = oForm.Items.Item("14").Specific.Value;
                    double LotCounter = 0;
                    htValidate.Clear();
                    for (int i = 0; i < oMatrix.RowCount - 1; i++)
                    {
                        LineNo = oMatrix.Columns.Item("0").Cells.Item(i + 1).Specific.Value;
                        ItemCode = oMatrix.Columns.Item("1").Cells.Item(i + 1).Specific.Value;
                        quantity = oMatrix.Columns.Item("11").Cells.Item(i + 1).Specific.Value;
                        NumPerMsr = oMatrix.Columns.Item("213").Cells.Item(i + 1).Specific.Value;


                        LotFrom = oMatrix.Columns.Item("U_LotFrom").Cells.Item(i + 1).Specific.Value;
                        LotTo = oMatrix.Columns.Item("U_LotTo").Cells.Item(i + 1).Specific.Value;

                        // added for the engine and chassis validation
                        double qty = 0;
                        double lotFr = 0;
                        double LotT = 0;
                        double.TryParse(quantity, out qty);
                        double.TryParse(LotFrom, out lotFr);
                        double.TryParse(LotTo, out LotT);

                        if (lotFr < 0 || LotT < 0)
                        {
                            SboApp.StatusBar.SetText("LotFrom and LotTo should be greater than zero. ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                            BubbleEvent = false;
                            return;
                        }


                        string s = CheckKDBatchManageItem(ItemCode);

                        if (s.Trim() == "")
                        {
                            s = CheckKDComponantManageItem(ItemCode);
                        }

                        if (s.Trim() != "")
                        {

                            if (!htValidate.Contains(ItemCode))
                            {
                                htValidate.Add(ItemCode, LotT);
                            }
                            else
                            {
                                LotCounter = lotFr + qty;

                                string cnt = htValidate[ItemCode].ToString();
                                htValidate[ItemCode] = LotT;
                                double counter = Convert.ToInt32(cnt);
                                if (lotFr - 1 != counter)
                                {
                                    SboApp.StatusBar.SetText("LotFrom and LotTo Overlap. ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                                    BubbleEvent = false;
                                    return;
                                }
                            }


                            double result = (LotT - lotFr) + 1;
                            if ((LotT - lotFr) + 1 != qty)
                            {
                                SboApp.StatusBar.SetText("LotFrom and LotTo difference not respective to the quantity. ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                                BubbleEvent = false;
                                return;
                            }
                        }
                        // end added for engine and chassis validation

                        string query = "insert into b1s_temp_" + oCompany.UserName.ToString() + "(Vendor, NumAtCard, LineID, ItemCode, Quantity, NumPerMsr, LotFrom, LotTo) "
                           + " values ('" + cardcode + "', '" + NumAtCard + "', " + LineNo + ", '" + ItemCode + "', " + quantity + ", " + NumPerMsr + ", '" + LotFrom + "', '" + LotTo + "') ";
                        ADONonExeSql(query);

                        DocEntry = 0;
                        DocEntryCBU = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Menu Click Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        public void ComboSelect(SAPbouiCOM.ItemEvent pVal, bool BubbleEvent)
        {
            try
            {
                oForm = SboApp.Forms.GetForm("143", pVal.FormTypeCount);
                oMatrix = oForm.Items.Item("38").Specific;
                if (pVal.ItemUID == "3" && pVal.BeforeAction == false)
                {
                    oCombo = oForm.Items.Item("3").Specific;

                    if (oCombo.Selected.Value == "I")
                    {
                        SAPbouiCOM.Column oItem = oMatrix.Columns.Item("213");
                        if (oItem.Visible == false || oItem.Editable == false)
                        {
                            SboApp.StatusBar.SetText("Please turn on Items Per Unit fields. before proceed.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Combo Select Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        private void ItemClick(SAPbouiCOM.ItemEvent pVal,out bool BubbleEvent)
        {
            try
            {
                
                oForm = SboApp.Forms.GetForm("143", pVal.FormTypeCount);
                SAPbouiCOM.Matrix oMatrix = oForm.Items.Item("38").Specific;

                if (pVal.ItemUID == "2" && pVal.BeforeAction == false)
                {
                    try
                    {
                        dt.Clear();
                    }
                    catch (Exception ex)
                    {
                        //SboApp.StatusBar.SetText("Item Click Event. "+ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                    }
                }

                if (pVal.ItemUID == "1" && pVal.BeforeAction == true)
                {
                    //string DocEntry = oForm.Items.Item("8").Specific.Value;
                    //string query = "select t0.CardCode, t0.NumAtCard, t1.LineNum, t1.ItemCode, t1.Quantity, t1.NumPerMsr "
                    //                + " into Be1s_Temp_From_" + oCompany.UserName.ToString() + " from OPDN t0 inner join PDN1 t1 on t0.DocEntry = t1.DocEntry"
                    //                + " where t0.DocEntry = " + DocEntry + "";

                    CreateTempFromAndToTable(oCompany.UserName.ToString());

                    cardcode = oForm.Items.Item("4").Specific.Value;
                    NumAtCard = oForm.Items.Item("14").Specific.Value;

                    double LotCounter = 0;
                    htValidate.Clear();
                    for (int i = 0; i < oMatrix.RowCount - 1; i++)
                    {
                        LineNo = oMatrix.Columns.Item("0").Cells.Item(i + 1).Specific.Value;
                        ItemCode = oMatrix.Columns.Item("1").Cells.Item(i + 1).Specific.Value;
                        quantity = oMatrix.Columns.Item("11").Cells.Item(i + 1).Specific.Value;
                        NumPerMsr = oMatrix.Columns.Item("213").Cells.Item(i + 1).Specific.Value;

                        LotFrom = oMatrix.Columns.Item("U_LotFrom").Cells.Item(i + 1).Specific.Value;
                        LotTo = oMatrix.Columns.Item("U_LotTo").Cells.Item(i + 1).Specific.Value;


                        double qty = 0;
                        double lotFr = 0;
                        double LotT= 0;
                          double.TryParse(quantity,out qty);
                          double.TryParse(LotFrom, out lotFr);
                          double.TryParse(LotTo, out LotT);

                          if (lotFr < 0 || LotT < 0)
                          {
                              SboApp.StatusBar.SetText("LotFrom and LotTo should be greater than zero. ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                              BubbleEvent = false;
                              return;    
                          }


                       string s = CheckKDBatchManageItem(ItemCode);

                       if (s.Trim() == "")
                       {
                           s = CheckKDComponantManageItem(ItemCode);
                       }

                        if(s.Trim() != "")
                        {
                             
                            if (!htValidate.Contains(ItemCode))
                            {
                                htValidate.Add(ItemCode, LotT);
                            }
                            else
                            {
                                LotCounter = lotFr + qty;
                                
                                string cnt = htValidate[ItemCode].ToString();
                                htValidate[ItemCode] = LotT;
                                double counter = Convert.ToInt32(cnt);
                                if (lotFr -1 != counter )
                                {
                                    SboApp.StatusBar.SetText("LotFrom and LotTo Overlap. ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                                    BubbleEvent = false;
                                    return;                  
                                }
                            }


                            double result = (LotT - lotFr) + 1;
                        if ((LotT - lotFr) +1 != qty)
                        {
                            SboApp.StatusBar.SetText("LotFrom and LotTo difference not respective to the quantity. ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                            BubbleEvent = false;
                            return;
                        }
                        }
                        string query = "insert into b1s_temp_" + oCompany.UserName.ToString() + "(Vendor, NumAtCard, LineID, ItemCode, Quantity, NumPerMsr, LotFrom, LotTo) "
                        + " values ('" + cardcode + "', '" + NumAtCard + "', " + LineNo + ", '" + ItemCode + "', " + quantity + ", " + NumPerMsr + ", '" + LotFrom + "', '" + LotTo + "') ";
                        ADONonExeSql(query);
                      
                        DocEntry = 0;
                        DocEntryCBU = 0;
                    }
                  
                }
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Item Click Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
            BubbleEvent = true;
        }

        public string CheckKDBatchManageItem(string ItemCode)
        {
            string q = " select U_KDitemCode, U_remark from [@KDVEHICLEMAPPING] where U_KDitemCode = '" + ItemCode + "'  and U_KDitemCode in (select Itemcode from OITM where ManBtchNum = 'Y')";
            SAPbobsCOM.Recordset oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRs.DoQuery(q);
            return oRs.Fields.Item(0).Value;
        }

        public string CheckKDComponantManageItem(string ItemCode)
        {
            string q = " select U_ComponentCode, U_remark from [@KDVEHICLEMAPPING] where U_ComponentCode = '" + ItemCode + "' and U_ComponentCode in (select Itemcode from OITM where ManBtchNum = 'Y')";
            SAPbobsCOM.Recordset oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRs.DoQuery(q);
            return oRs.Fields.Item(0).Value;
        }

        public int GetMaxID()
        {
            try
            {
                SAPbobsCOM.Recordset oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                string Query = "select max(cast(code as int)) from [@B1S_COMMCOUNTER]";
                oRs.DoQuery(Query);
                return oRs.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("GetMaxID method. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                return 0;
            }

        }
        private void CreateTempFromAndToTable(string User)
        {
            try
            {

                string str = "exec sp_DropTable 'B1s_Temp_" + User + "'";

                str = "CREATE TABLE [dbo].[b1s_temp_" + User + "]( "
                      + "       [Vendor] [nvarchar](50) NULL, "
                         + "         [NumAtCard] [nvarchar](50) NULL, "
                        + "          [LineID] [int] NULL, "
                        + "          [ItemCode] [nvarchar](50) NULL, "
                        + "          [Quantity] [numeric](19, 6) NULL, "
                        + "          [NumPerMsr] [numeric](19, 6) NULL, LotFrom [nvarchar](50) NULL, LotTo [nvarchar](50) NULL "
                       + "       ) ON [PRIMARY] ";
                ADONonExeSql(str);
                str = "delete From b1s_temp_" + User + "";
                ADONonExeSql(str);

            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Create Table Exception. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }

        }
        public void FormLoad(SAPbouiCOM.ItemEvent pVal, bool BubbleEvent)
        {
            try
            {
                oForm = SboApp.Forms.GetForm("143", pVal.FormTypeCount);
                oMatrix = oForm.Items.Item("38").Specific;
                SAPbouiCOM.Column oItem = oMatrix.Columns.Item("213");
                if (pVal.BeforeAction == false)
                {
                    if (oItem.Visible == false || oItem.Editable == false)
                    {
                        SboApp.StatusBar.SetText("Please turn on Items Per Unit fields. before proceed.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);

                    }
                }

                //htValidate.Clear();

            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Form Load Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        public void FormDataLoadEvent(ref SAPbouiCOM.BusinessObjectInfo pVal, ref bool BubbleEvent)
        {
            try
            {
                if ((pVal.BeforeAction == false))
                {
                    try
                    {
                        oForm = SboApp.Forms.ActiveForm;

                        if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD && pVal.ActionSuccess == true)
                        {
                            try
                            {

                                //if (dt != null)
                                //{
                                //    for (int i = 0; i < dt.Rows.Count; i++)
                                //    {
                                //        if (dt.Rows[i]["Iden"].ToString().Trim() != "0" || dt.Rows[i]["Iden"].ToString().Trim() != "")
                                //        {
                                //            SAPbobsCOM.Recordset oRsDetails = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                                //            oRsDetails.DoQuery("select Code from [@B1S_COMMCOUNTER] where U_TYPE = '" + dt.Rows[i]["Iden"].ToString() + "' and U_Year = '" + DateTime.Now.Year.ToString() + "'");
                                //            if (oRsDetails.RecordCount > 0)
                                //            {
                                //                oRsDetails.DoQuery("update [@B1S_COMMCOUNTER] SET U_COUNTER = " + dt.Rows[i]["Doc"].ToString() + " where code = '" + oRsDetails.Fields.Item(0).Value + "'");
                                //            }
                                //            else
                                //            {
                                //                string code = (GetMaxID() + 1).ToString();
                                //                string q = "insert into  [@B1S_COMMCOUNTER] (Code, Name, U_TYPE, U_Year, U_COUNTER ) values "
                                //                + " ('" + code + "','" + code + "', '" + dt.Rows[i]["Iden"].ToString() + "',"
                                //                + "'" + DateTime.Now.Year.ToString() + "', " + dt.Rows[i]["Doc"].ToString() + ") ";
                                //                oRsDetails.DoQuery(q);
                                //            }
                                //        }
                                //    }
                                dt.Clear();
                                //}


                                //if (dtSerialUpdate != null)
                                //{
                                //    for (int i = 0; i < dtSerialUpdate.Rows.Count; i++)
                                //    {
                                //        string Itemcode = dtSerialUpdate.Rows[i]["ItemCode"].ToString();
                                //        Itemcode = Itemcode.Substring(0, 5);
                                //        //Sql = "update B1S_COMMIMPORTCBU set Matched = 'Y' , CommissionNo = '" + dtSerialUpdate.Rows[i]["CommissionNo"].ToString() + "' "
                                //        //+ " where I_INVNO = '" + dtSerialUpdate.Rows[i]["Inv"].ToString() + "' and I_Model = '" + Itemcode + "' "
                                //        //+ " and I_ENGINENO = '" + dtSerialUpdate.Rows[i]["Engine"].ToString() + "' and R_CHASSIS = '" + dtSerialUpdate.Rows[i]["Chassis"].ToString() + "'";

                                //        Sql = "update B1S_COMMIMPORTCBU set Matched = 'Y' , CommissionNo = '" + dtSerialUpdate.Rows[i]["CommissionNo"].ToString() + "' "
                                //       + " where I_INVNO = '" + dtSerialUpdate.Rows[i]["Inv"].ToString() + "' and I_Model like SUBSTRING('" + Itemcode + "',  0, 5)+ '%'"
                                //       + " and I_ENGINENO = '" + dtSerialUpdate.Rows[i]["Engine"].ToString() + "' and R_CHASSIS = '" + dtSerialUpdate.Rows[i]["Chassis"].ToString() + "'";


                                //        ADONonExeSql(Sql);
                                //    }
                                dtSerialUpdate.Clear();
                                htSerial.Clear();
                                //}


                                //foreach (var list in ListFGCommissionNo)
                                //{
                                //    Sql = "Update [@SALEDKD] set U_status = 'Y' where U_COmmissionNo = '" + list + "'";
                                //    ADONonExeSql(Sql);
                                //}
                                ListFGCommissionNo.Clear();
                            }
                            catch (Exception ex)
                            {
                                //SboApp.StatusBar.SetText("Item Click Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                            }
                        }
                        if (pVal.ActionSuccess == false)
                        {
                            //try
                            //{
                            //    dt.Clear();
                            //}
                            //catch (Exception)
                            //{
                            //}
                        }



                    }
                    catch (Exception ex)
                    {
                        oMsgShow.Messsage("Form Data Load Exception : " + ex.Message.ToString(), MsgShow.MsgType.Statusbar, MsgShow.MsgCat.Errors);
                    }
                }
                if ((pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD || pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE) && pVal.ActionSuccess == true)
                {
                    try
                    {
                        htBatch.Clear();
                        DocEntry = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            catch (Exception e)
            {
                oMsgShow.Messsage("Form Data Load Exception : " + e.Message.ToString(), MsgShow.MsgType.Statusbar, MsgShow.MsgCat.Errors);
            }

        }
    }
}
