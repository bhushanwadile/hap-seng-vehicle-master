﻿using SAPbouiCOM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HapSengVehicleMaster.SysForm
{
    class _41 : UIDesign
    {
        DataSet ds = new DataSet();
        // new use code
        public void EventHandler(ref SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            try
            {
                if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_CLICK)
                {
                    ItemClick(pVal, ref BubbleEvent);
                }
                if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_CLOSE)
                {
                    FormCloseEvent(pVal, BubbleEvent);
                }
                if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
                {
                    FormLoadEvent(pVal, BubbleEvent);
                }
                if (pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_UNLOAD)
                {
                    FormUnLoadEvent(pVal, BubbleEvent);
                }


            }
            catch (Exception e)
            {

            }
        }

        public void FormUnLoadEvent(ItemEvent pVal, bool BubbleEvent)
        {

        }
        public void MenuEvent(ref SAPbouiCOM.MenuEvent pVal, ref bool BubbleEvent)
        {
            try
            {
                if (pVal.MenuUID == "1293" && pVal.BeforeAction == false)
                {
                    DocEntry = 0;
                }
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Menu Click Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        private void FormCloseEvent(ItemEvent pVal, bool BubbleEvent)
        {
            try
            {
                if (pVal.ActionSuccess == false)
                {
                    try
                    {
                        if (SuccessFlag == false)
                        {
                            dt.Clear();
                            htBatch.Clear();
                            htIdentity.Clear();
                            //dtKDDocEntry.Clear();
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Form Data load Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        public void FormLoadEvent(SAPbouiCOM.ItemEvent pVal, bool BubbleEvent)
        {
            try 
	        {
                if (pVal.BeforeAction == false)
                {
                    SAPbouiCOM.Form oForm = SboApp.Forms.GetForm("41", pVal.FormTypeCount);
                    SAPbouiCOM.Matrix oMatrix = oForm.Items.Item("3").Specific;
                    //ds.ReadXml("ConFigFile.xml");
                    //System.Data.DataTable dt1 = ds.Tables[0];
                    Sql = "select EnableFlag from B1S_Config";
                    System.Data.DataTable dt12 = GetDataTable(Sql);
                    if (dt12.Rows[0][0].ToString().Trim() == "Y")
                    {
                        try
                        {
                            oMatrix.Columns.Item("20").Cells.Item(1).Click();
                            oMatrix.Columns.Item("2").Editable = false;
                            oMatrix.Columns.Item("5").Editable = false;
                            oMatrix.Columns.Item("7").Editable = false;
                            oMatrix.Columns.Item("8").Editable = false;
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
	        }
	        catch (Exception)
	        {
		
		        
	        }
        }


        public void ItemClick(SAPbouiCOM.ItemEvent pVal, ref bool BubbleEvent)
        {
            try
            {
                SAPbouiCOM.Form oForm = SboApp.Forms.GetForm("41", pVal.FormTypeCount);



                if (pVal.ItemUID == "1" && pVal.BeforeAction == false)
                {

                }

                if (FormTypeFlag == "143")
                {
                    selectedRow = 1;
                    oForm = SboApp.Forms.GetForm("41", pVal.FormTypeCount);
                    SAPbouiCOM.Matrix oMatrix = oForm.Items.Item("3").Specific;
                    SAPbouiCOM.Matrix oMatrixUp = oForm.Items.Item("35").Specific;
                    if (pVal.ItemUID == "1" && pVal.BeforeAction == false)
                    {
                        SuccessFlag = true;
                    }

                    if (pVal.ItemUID == "2" && pVal.BeforeAction == false)
                    {
                        SuccessFlag = false;
                        try
                        {
                            dt.Clear();
                            htBatch.Clear();
                            htIdentity.Clear();
                            dtKDDocEntry.Clear();
                        }
                        catch (Exception)
                        {

                        }


                    }
                }

                    if (pVal.ItemUID == "36" && pVal.BeforeAction == true)
                    {
                        oForm = SboApp.Forms.GetForm("41", pVal.FormTypeCount);
                        //oForm.Freeze(true);
                        try
                        {
                             
                    SAPbouiCOM.Item oItem = oForm.Items.Item("36");
                    if (oItem.Enabled == true)
                    {
                        UpdateCounter();
                        KDProcessNewDemo(out  BubbleEvent);
                        ComponantProcessNew(out  BubbleEvent);
                        batchExistFlag = false;
                        if (BubbleEvent == false)
                        {
                            //BubbleEvent = false;
                        }
                        else
                        {
                            BubbleEvent = true;
                        }
                        //BubbleEvent = false;
                    }
                        }
                        catch (Exception ex)
                        {
                            SboApp.StatusBar.SetText("Item Click Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                        }
                        finally
                        {
                            //oForm.Freeze(false);
                        }
                    }

                }
            

                        //try{
            //KDProcess(out  BubbleEvent);
            //ComponantProcess(out  BubbleEvent);
            //if (BubbleEvent == false)
            //{
            //    //BubbleEvent = false;
            //}
            //else
            //{
            //    BubbleEvent = true;
            //}

                        //}
            //catch (Exception ex)
            //{
            //    SboApp.StatusBar.SetText("Item Click Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            //}
            //finally
            //{
            //    oForm.Freeze(false);
            //}
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Item Click Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
            finally
            {
            }
            //oForm.Freeze(false);

        }
        public void UpdateCounter()
        {
            try
            {
                Sql = "exec BE1S_UpdateCounter";
                ADONonExeSql(Sql);
            }
            catch (Exception)
            {
                SboApp.StatusBar.SetText("Counter update exception ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        public void KDProcessNewDemo(out bool BubbleEvent)
        {
            BubbleEvent = true;
            SAPbouiCOM.Form oForm = SboApp.Forms.ActiveForm;
            SAPbouiCOM.Matrix oMatrixUp = oForm.Items.Item("35").Specific;
            SAPbouiCOM.Matrix oMatrix = oForm.Items.Item("3").Specific;
            string vendor = "";
            string totalneeded = "";
            string totalcreated = "";
            double needed;
            double created;
            int Counter = 1;
            string ItemCode;
            //bool batchExistFlag = false;
            //int DocEntry = 0;
            MultiLine = false;
            string Identifier = "";
            System.Data.DataTable dtoRs;
            System.Data.DataTable dtoRsVendor;
            System.Data.DataTable dtoRsChecKDI;
            htBatch.Clear();
            htIdentity.Clear();
            
            Sql = "select Vendor, NumAtCard, LineID, ItemCode, Quantity, NumPerMsr, LotFrom, LotTo from b1s_temp_" + oCompany.UserName.ToString() + " where ItemCode IN (select ItemCode from OITM where ManBtchNum = 'Y' )";
            dtoRs = GetDataTable(Sql);

            if (dtBatchAllocate == null)
            {
                dtBatchAllocate = new System.Data.DataTable();

                dtBatchAllocate.Columns.Add("Item", typeof(string));
                dtBatchAllocate.Columns.Add("Batch", typeof(string));
                dtBatchAllocate.Columns.Add("LineiD", typeof(int));
            }
            SboApp.StatusBar.SetText("Automatic batch creation process started. Please wait...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

            //for (int b = 1; b <= oMatrixUp.RowCount; b++)
            //{
            //     oMatrixUp.Columns.Item("0").Cells.Item(b).Click(SAPbouiCOM.BoCellClickType.ct_Regular);

            //    for (int c = oMatrix.RowCount-1; c >= 1 ; c--)
            //    {

            //        SAPbouiCOM.EditText oEdit = oMatrixUp.Columns.Item("38").Cells.Item(b).Specific;
            //        oEdit.Item.Enabled = true;
            //        oEdit.Value = "0";

            //        //oMatrix.Columns.Item("0").Cells.Item(c).Click();
            //        //oMatrix.SelectRow(c, true, false); 
            //        //oMatrixUp.SetCellFocus(b, 4);
            //        //oMatrixUp.Columns.Item("38").Cells.Item(b).Specific.Value = 0;
            //        //oForm.EnableMenu("1293", true);
            //        //SboApp.ActivateMenuItem("1293");
            //        //oMatrix.Columns.Item("5").Cells.Item(c).Specific.Value = 0;
            //        //oMatrix.Columns.Item("7").Cells.Item(c).Specific.Value = "";
            //        //oMatrix.Columns.Item("8").Cells.Item(c).Specific.Value = "";
            //        //oMatrix.DeleteRow(c);
            //    }

            //oMatrixUp.Columns.Item("38").Cells.Item(1).Specific.Value = "60";
            //oForm.DataSources.DBDataSources.Item("SBDR").SetValue("TotalCreat", b - 1,"0");
            //    oForm.Mode = BoFormMode.fm_UPDATE_MODE;
            //    oForm.Items.Item("1").Click(BoCellClickType.ct_Regular);

            //}


            oForm.Freeze(true);
            for (int ii = 0; ii < dtoRs.Rows.Count; ii++)
            {

                try
                {
                    oMatrixUp.Columns.Item("0").Cells.Item(Counter).Click(SAPbouiCOM.BoCellClickType.ct_Regular);


                }
                catch (Exception)
                {
                }
                vendor = dtoRs.Rows[ii][0].ToString();
                ItemCode = dtoRs.Rows[ii][3].ToString();
                Sql = "select top 1 Vendor, Identifier, TaiwanCode from B1S_VendorConfig where Vendor ='" + dtoRs.Rows[ii][0].ToString() + "'";
                dtoRsVendor = GetDataTable(Sql);
                Sql = "select top 1 U_KDitemCode, U_remark from [@KDVEHICLEMAPPING] where U_KDitemCode = '" + dtoRs.Rows[ii][3].ToString() + "'";
                dtoRsChecKDI = GetDataTable(Sql);

                if (dtoRsChecKDI.Rows.Count > 0)
                {
                    if (dtoRs.Rows[ii][6].ToString().Trim() != "" && dtoRs.Rows[ii][7].ToString().Trim() != "")
                    {
                        double TotalBatches = Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString()) / Convert.ToDouble(dtoRs.Rows[ii][5].ToString());
                        int TotalBatch = (int)Math.Ceiling(TotalBatches);
                        double TotalQty = Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString());
                        double qty = (Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString())) / TotalBatch;
                        string yr = Convert.ToString(DateTime.Now.Year % 100);
                        string TaiwanCode = "886";
                        Identifier = dtoRsChecKDI.Rows[0][1].ToString();
                        Identifier = Identifier.Trim().Substring(0, 1);
                        if (DocEntry == 0 )
                        {
                                DocEntry = GetMaxCode(Identifier, DateTime.Now.Year.ToString());
                            
                        }
                        if (htIdentity.ContainsKey(Identifier))
                        {
                            DocEntry = Convert.ToInt32(htIdentity[Identifier]);
                        }
                        else
                        {
                            DocEntry = GetMaxCode(Identifier, DateTime.Now.Year.ToString());
                        }
                        //if (htBatch.ContainsKey(dtoRs.Rows[ii][3].ToString()))
                        //{
                        //    DocEntry = Convert.ToInt32(htBatch[dtoRs.Rows[ii][3].ToString()]);
                        //}

                        if (DocEntry == 0)
                            DocEntry = 1;

                        int BatchID, UpdateBatchID = 0;
                        BatchID = Convert.ToInt32(dtoRs.Rows[ii][6]);
                        UpdateBatchID = BatchID;
                        int LotTo = Convert.ToInt32(dtoRs.Rows[ii][7]);

                        string result = CheckBatchExists(dtoRs.Rows[ii][3].ToString(), BatchID, LotTo);
                        if (result.Trim() != "")
                        {
                            SboApp.StatusBar.SetText("Item : '" + dtoRs.Rows[ii][3].ToString() + "' Batches : " + result + " already exist in the system. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                            BubbleEvent = false;
                            batchExistFlag = true;
                            //return;
                        }
                        else
                        {

                            
                            int i = 0;
                            int j;
                            j = 1;
                           oForm.Freeze(true);
                            oMatrix.Columns.Item("2").Editable = true;
                            oMatrix.Columns.Item("5").Editable = true;
                            oMatrix.Columns.Item("7").Editable = true;
                            oMatrix.Columns.Item("8").Editable = true;
                            for (i = BatchID; i < BatchID + TotalBatches; i++)
                            {

                                batchExistFlag = false;
                                string str1 = DocEntry.ToString().PadLeft(5, '0').ToString();
                                string CommissionFrom = yr + TaiwanCode + Identifier + str1;
                                DocEntry = DocEntry + Convert.ToInt32(qty) - 1;
                                string str2 = DocEntry.ToString().PadLeft(5, '0').ToString();
                                string CommissionTo = yr + TaiwanCode + Identifier + str2;
                                //oForm.Freeze(true);
                                try
                                {

                                    //for (int a = 0; a < dtBatchAllocate.Rows.Count; a++)
                                    //{
                                    //    if(dtBatchAllocate.Rows[a][0].ToString() == ItemCode)
                                    //    {
                                    //        if (dtBatchAllocate.Rows[a][1].ToString() == i.ToString() && Convert.ToInt32(dtBatchAllocate.Rows[a][2]) != Counter -1)
                                    //        {
                                    //            SboApp.StatusBar.SetText("Item : '" + dtoRs.Rows[ii][3].ToString() + "' Batch : " + i.ToString() + " already exist in the document. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                                    //            BubbleEvent = false;
                                    //            batchExistFlag = true;
                                    //            return;
                                    //        }
                                    //    }
                                    //}

                                    oMatrix.Columns.Item("2").Cells.Item(j).Specific.Value = i;

                                    //dtBatchAllocate.Rows.Add(ItemCode, i, Counter);
                                }
                                catch (Exception)
                                {
                                    oForm.Freeze(false);
                                    SboApp.MessageBox("Please delete all batch allocation manually and create batches automatically.", 1);
                                    BubbleEvent = false;
                                    return;
                                }
                                oForm.Freeze(true);
                                oMatrix.Columns.Item("5").Cells.Item(j).Specific.Value = qty;
                                oMatrix.Columns.Item("7").Cells.Item(j).Specific.Value = CommissionFrom;
                                oMatrix.Columns.Item("8").Cells.Item(j).Specific.Value = CommissionTo;
                                oMatrix.Columns.Item("11").Cells.Item(j).Click();
                                //oMatrix.AddRow(1, -1);
                                oForm.Freeze(false);
                                DocEntry++;
                                j++;
                                UpdateBatchID++;


                            }


                            if (!htIdentity.ContainsKey(Identifier))
                            {
                                htIdentity.Add(Identifier, DocEntry);
                            }
                            else
                            {
                                htIdentity[Identifier] = DocEntry;
                            }

                            if (!htBatch.ContainsKey(dtoRs.Rows[ii][3].ToString()))
                            {
                                htBatch.Add(dtoRs.Rows[ii][3].ToString(), DocEntry);
                            }
                            else
                            {
                                htBatch[dtoRs.Rows[ii][3].ToString()] = DocEntry;
                            }

                            oForm.Freeze(false);
                            UpdateBatchID = 0;

                            
                        }
                    }

                    else
                    {
                        SboApp.StatusBar.SetText("Please fill LotFrom and LotTo on the GRPO line", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                        BubbleEvent = false;
                        return;
                    }
                    BubbleEvent = false;
                }
                Counter++;
            }
            Sql = "select EnableFlag from B1S_Config";
            System.Data.DataTable dt12 = GetDataTable(Sql);
            if (dt12.Rows[0][0].ToString().Trim() == "Y")
            {
                try
                {
                 
                    oMatrix.Columns.Item("2").Editable = false;
                    oMatrix.Columns.Item("5").Editable = false;
                    oMatrix.Columns.Item("7").Editable = false;
                    oMatrix.Columns.Item("8").Editable = false;
                }
                catch (Exception)
                {
                }
            }
            if (batchExistFlag == false)
            {
                SboApp.StatusBar.SetText("Automatic batch creation process completed successfully.", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success);

            }
            //BubbleEvent = true;
            oForm.Freeze(false);
        }

        public void KDProcessNew(out bool BubbleEvent)
        {
            SAPbouiCOM.Form oForm = SboApp.Forms.ActiveForm;
            SAPbouiCOM.Matrix oMatrixUp = oForm.Items.Item("35").Specific;
            SAPbouiCOM.Matrix oMatrix = oForm.Items.Item("3").Specific;
            string vendor = "";
            string totalneeded = "";
            string totalcreated = "";
            double needed;
            double created;
            int Counter = 1;
            //int DocEntry = 0;
            MultiLine = false;
            string Identifier = "";
            System.Data.DataTable dtoRs;
            System.Data.DataTable dtoRsVendor;
            System.Data.DataTable dtoRsChecKDI;

            Sql = "select Vendor, NumAtCard, LineID, ItemCode, Quantity, NumPerMsr, LotFrom, LotTo from b1s_temp_" + oCompany.UserName.ToString() + " where ItemCode IN (select ItemCode from OITM where ManBtchNum = 'Y' )";
            dtoRs = GetDataTable(Sql);

            if (dt == null)
            {
                dt = new System.Data.DataTable();
                dt.Columns.Add("Doc", typeof(int));
                dt.Columns.Add("Iden", typeof(string));
            }
            SboApp.StatusBar.SetText("Automatic batch creation process started. Please wait...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);

            for (int ii = 0; ii < dtoRs.Rows.Count; ii++)
            {
                if (dtoRs.Rows[ii][6].ToString().Trim() != "" && dtoRs.Rows[ii][7].ToString().Trim() != "")
                {
                    try
                    {
                        oMatrixUp.Columns.Item("0").Cells.Item(Counter).Click(SAPbouiCOM.BoCellClickType.ct_Regular);
                    }
                    catch (Exception)
                    {
                    }

                    totalneeded = Convert.ToString(oMatrixUp.Columns.Item("39").Cells.Item(Counter).Specific.Value);
                    totalcreated = Convert.ToString(oMatrixUp.Columns.Item("38").Cells.Item(Counter).Specific.Value);

                    string CommisionNo = "";


                    if (totalneeded == "")
                        needed = 0;
                    else
                        needed = Convert.ToDouble(totalneeded);

                    if (totalcreated == "")
                        created = 0;
                    else
                        created = Convert.ToDouble(totalcreated);

                    vendor = dtoRs.Rows[ii][0].ToString();
                    Sql = "select top 1 Vendor, Identifier, TaiwanCode from B1S_VendorConfig where Vendor ='" + dtoRs.Rows[ii][0].ToString() + "'";
                    dtoRsVendor = GetDataTable(Sql);

                    //if (dtoRsVendor.Rows.Count > 0)
                    //{

                    Sql = "select top 1 U_KDitemCode, U_remark from [@KDVEHICLEMAPPING] where U_KDitemCode = '" + dtoRs.Rows[ii][3].ToString() + "'";
                    dtoRsChecKDI = GetDataTable(Sql);

                    if (dtoRsChecKDI.Rows.Count > 0)
                    {
                        double TotalBatches = Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString()) / Convert.ToDouble(dtoRs.Rows[ii][5].ToString());
                        int TotalBatch = (int)Math.Ceiling(TotalBatches);
                        double TotalQty = Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString());
                        double qty = (Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString())) / TotalBatch;
                        double qtyremaining = 0;
                        string yr = Convert.ToString(DateTime.Now.Year % 100);
                        string TaiwanCode = "886";
                        Identifier = dtoRsChecKDI.Rows[0][1].ToString();
                        Identifier = Identifier.Trim().Substring(0, 1);
                        if (MultiLine == false)
                        {
                            while (DocEntry == 0)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    DocEntry = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString());
                                }
                                else
                                {
                                    DocEntry = GetMaxCode(Identifier, DateTime.Now.Year.ToString());
                                }
                            }
                            if (DocEntry == 0)
                                DocEntry = 1;
                        }
                        else
                        {


                        }
                        if (dtKDDocEntry.Contains(Identifier) != true)
                        {
                            DocEntry = GetMaxCode(Identifier, DateTime.Now.Year.ToString());
                        }
                        else
                        {
                            DocEntry = Convert.ToInt32(dtKDDocEntry[Identifier]);
                        }
                        int BatchID, UpdateBatchID = 0;
                        if (!htBatch.ContainsKey(dtoRs.Rows[ii][3].ToString()))
                        {
                            BatchID = GetMaxBatchID(dtoRs.Rows[ii][3].ToString());
                            UpdateBatchID = BatchID;
                            //htBatch.Add(dtoRs.Rows[ii][3].ToString(), BatchID);
                            MultiLine = false;
                        }
                        else
                        {
                            BatchID = Convert.ToInt32(htBatch[dtoRs.Rows[ii][3].ToString()]);
                            UpdateBatchID = BatchID;
                            //BatchID = BatchID + 1;
                            MultiLine = true;
                        }
                        BatchID = Convert.ToInt32(dtoRs.Rows[ii][6]);
                        BatchID = BatchID - 1;

                        int i = 0;
                        int j;
                        if (created > 0)
                        {
                            string asd;
                            j = oMatrix.RowCount;
                            string getmaxBatch;
                            if (oMatrix.Columns.Item("2").Cells.Item(oMatrix.RowCount).Specific.Value == "")
                            {
                                getmaxBatch = oMatrix.Columns.Item("2").Cells.Item(oMatrix.RowCount - 1).Specific.Value;
                                CommisionNo = oMatrix.Columns.Item("8").Cells.Item(oMatrix.RowCount - 1).Specific.Value;
                                asd = CommisionNo.Substring(CommisionNo.Length - 5, 5);
                                DocEntry = Convert.ToInt32(asd);
                                DocEntry = DocEntry + 1;
                            }
                            else
                            {
                                getmaxBatch = oMatrix.Columns.Item("2").Cells.Item(oMatrix.RowCount).Specific.Value;
                                CommisionNo = oMatrix.Columns.Item("8").Cells.Item(oMatrix.RowCount).Specific.Value;
                                asd = CommisionNo.Substring(CommisionNo.Length - 5, 5);
                                DocEntry = Convert.ToInt32(asd);
                            }
                            //BatchID = Convert.ToInt32(getmaxBatch);
                            BatchID = Convert.ToInt32(getmaxBatch);
                            UpdateBatchID = BatchID;
                            //string asd  = CommisionNo.Substring(CommisionNo.Length - 5, 5);
                            //    DocEntry = Convert.ToInt32(asd);
                            //DocEntry = DocEntry + 1;
                            //BatchID = BatchID - 1;
                        }
                        else
                        {
                            j = 1;
                        }
                        oForm.Freeze(true);
                        oMatrix.Columns.Item("2").Editable = true;
                        oMatrix.Columns.Item("5").Editable = true;
                        oMatrix.Columns.Item("7").Editable = true;
                        oMatrix.Columns.Item("8").Editable = true;
                        for (i = BatchID; i < BatchID + TotalBatches; i++)
                        {
                            if (created < needed)
                            {
                                //if (TotalQty - qtyremaining > 0)
                                //{
                                qtyremaining = qtyremaining + qty;
                                string str1 = DocEntry.ToString().PadLeft(5, '0').ToString();
                                string CommissionFrom = yr + TaiwanCode + Identifier + str1;
                                DocEntry = DocEntry + Convert.ToInt32(qty) - 1;
                                string str2 = DocEntry.ToString().PadLeft(5, '0').ToString();
                                string CommissionTo = yr + TaiwanCode + Identifier + str2;
                                oForm.Freeze(true);
                                //if (CheckBatchExists(dtoRs.Rows[ii][3].ToString(), (i + 1).ToString()))
                                //{
                                //    SboApp.StatusBar.SetText("Batch already exist : "+ i + 1 , BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                                //    BubbleEvent = false;
                                //    return;
                                //}
                                //else
                                //{
                                try
                                {
                                    oMatrix.Columns.Item("2").Cells.Item(j).Specific.Value = i + 1;
                                }
                                catch (Exception)
                                {
                                    BubbleEvent = true;
                                }

                                oMatrix.Columns.Item("5").Cells.Item(j).Specific.Value = qty;
                                oMatrix.Columns.Item("7").Cells.Item(j).Specific.Value = CommissionFrom;
                                oMatrix.Columns.Item("8").Cells.Item(j).Specific.Value = CommissionTo;
                                oMatrix.Columns.Item("10").Cells.Item(j).Click();
                                oForm.Freeze(false);
                                DocEntry++;
                                j++;
                                UpdateBatchID++;
                                created = created + qty;
                                //}
                            }
                            else
                            {
                                DocEntry = DocEntry + Convert.ToInt32(qty) - 1;
                            }
                        }

                        //oMatrix.Columns.Item("2").Editable = false;
                        //oMatrix.Columns.Item("5").Editable = false;
                        //oMatrix.Columns.Item("7").Editable = false;
                        //oMatrix.Columns.Item("8").Editable = false;
                        oForm.Freeze(false);
                        if (!htBatch.ContainsKey(dtoRs.Rows[ii][3].ToString()))
                        {
                            htBatch.Add(dtoRs.Rows[ii][3].ToString(), i);
                        }
                        else
                        {
                            htBatch[dtoRs.Rows[ii][3].ToString()] = UpdateBatchID;
                        }
                        //dt.Rows.Add(DocEntry, Identifier);
                        UpdateBatchID = 0;
                    }
                    //}
                    //oRs.MoveNext();
                    Counter++;

                    //MultiLine = true;
                    if (!dtKDDocEntry.Contains(Identifier) == true)
                    {
                        dtKDDocEntry.Add(Identifier, DocEntry);
                    }
                    else
                    {
                        dtKDDocEntry[Identifier] = DocEntry;
                    }
                }
                else
                {
                    SboApp.StatusBar.SetText("Please fill LotFrom and LotTo on the GRPO line", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                    BubbleEvent = false;
                    return;
                }
            }

            //oForm.Freeze(false);
            foreach (DictionaryEntry item in dtKDDocEntry)
            {
                dt.Rows.Add(item.Value, item.Key);
            }



            SboApp.StatusBar.SetText("Automatic batch creation process completed successfully.", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success);

            BubbleEvent = false;
        }

        private string CheckBatchExists(string ItemCode, int LotFrom, int LotTO)
        {
            try
            {
                        Sql = "select ISNULL(STUFF((SELECT ', ' + CAST(Distnumber AS VARCHAR(10)) [text()] "
                         + "  from OBTN where ItemCode ='" + ItemCode + "' and DistNumber >= " + LotFrom + " and DistNumber <=" + LotTO + " "
                         + "  FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' '), '')";
                        System.Data.DataTable dt2 = GetDataTable(Sql);
                        return (dt2.Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Check Batch Exists Method : " + ex.Message.ToString(), BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                return "";
            }         
        }

        public void ComponantProcessNew(out bool BubbleEvent)
        {
            BubbleEvent = true;
            SAPbouiCOM.Form oForm = SboApp.Forms.ActiveForm;
            SAPbouiCOM.Matrix oMatrixUp = oForm.Items.Item("35").Specific;
            SAPbouiCOM.Matrix oMatrix = oForm.Items.Item("3").Specific;
            string vendor = "";
            string totalneeded = "";
            string totalcreated = "";
            double needed;
            double created;
            int Counter = 1;
            //int DocEntry = 0;
            MultiLine = false;
            string Identifier = "";
            System.Data.DataTable dtoRs;
            System.Data.DataTable dtoRsVendor;
            System.Data.DataTable dtoRsChecKDI;

            Sql = "select Vendor, NumAtCard, LineID, ItemCode, Quantity, NumPerMsr, LotFrom, LotTo from b1s_temp_" + oCompany.UserName.ToString() + " where ItemCode IN (select ItemCode from OITM where ManBtchNum = 'Y' )";
            dtoRs = GetDataTable(Sql);

            if (dt == null)
            {
                dt = new System.Data.DataTable();
                dt.Columns.Add("Doc", typeof(int));
                dt.Columns.Add("Iden", typeof(string));
            }
            if (batchExistFlag == false)
            {
                SboApp.StatusBar.SetText("Automatic batch creation process started. Please wait...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
            }
            oForm.Freeze(true);
            for (int ii = 0; ii < dtoRs.Rows.Count; ii++)
            {

                try
                {
                    oMatrixUp.Columns.Item("0").Cells.Item(Counter).Click(SAPbouiCOM.BoCellClickType.ct_Regular);
                }
                catch (Exception)
                {
                }

                totalneeded = Convert.ToString(oMatrixUp.Columns.Item("39").Cells.Item(Counter).Specific.Value);
                totalcreated = Convert.ToString(oMatrixUp.Columns.Item("38").Cells.Item(Counter).Specific.Value);

                string CommisionNo = "";

                vendor = dtoRs.Rows[ii][0].ToString();
                Sql = "select top 1 Vendor, Identifier, TaiwanCode from B1S_VendorConfig where Vendor ='" + dtoRs.Rows[ii][0].ToString() + "'";
                dtoRsVendor = GetDataTable(Sql);

                Sql = "select top 1 U_ComponentCode, U_remark from [@KDVEHICLEMAPPING] where U_ComponentCode = '" + dtoRs.Rows[ii][3].ToString() + "' and U_ComponentCode IN (select Itemcode from OITM where isnull(ManBtchNum, '') = 'Y')";
                dtoRsChecKDI = GetDataTable(Sql);

                if (dtoRsChecKDI.Rows.Count > 0)
                {
                    if (dtoRs.Rows[ii][6].ToString().Trim() != "" && dtoRs.Rows[ii][7].ToString().Trim() != "")
                    {
                        double TotalBatches = Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString()) / Convert.ToDouble(dtoRs.Rows[ii][5].ToString());
                        int TotalBatch = (int)Math.Ceiling(TotalBatches);
                        double TotalQty = Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString());
                        double qty = (Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString())) / TotalBatch;
                        double qtyremaining = 0;
                        string yr = Convert.ToString(DateTime.Now.Year % 100);
                        string TaiwanCode = "886";
                        Identifier = dtoRsChecKDI.Rows[0][1].ToString();
                        Identifier = Identifier.Trim().Substring(0, 1);

                        int BatchID, UpdateBatchID = 0;

                        BatchID = Convert.ToInt32(dtoRs.Rows[ii][6]);
                        UpdateBatchID = BatchID;
                        int LotTo = Convert.ToInt32(dtoRs.Rows[ii][7]);

                        string result = CheckBatchExists(dtoRs.Rows[ii][3].ToString(), BatchID, LotTo);
                        if (result.Trim() != "")
                        {
                            SboApp.StatusBar.SetText("Item : '" + dtoRs.Rows[ii][3].ToString() + "' Batches : " + result + " already exist in the system. ", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                            BubbleEvent = false;
                            batchExistFlag = true;
                            //return;
                        }
                        else
                        {



                            int j = 1;

                            oForm.Freeze(true);
                            oMatrix.Columns.Item("2").Editable = true;
                            oMatrix.Columns.Item("5").Editable = true;
                            oMatrix.Columns.Item("7").Editable = true;
                            oMatrix.Columns.Item("8").Editable = true;
                            for (int i = BatchID; i < BatchID + TotalBatches; i++)
                            {
                                Sql = "Select isnull(Distnumber, '') from OBTN where Distnumber = '" + i.ToString() + "'";
                                System.Data.DataTable dt2 = GetDataTable(Sql);
                                if (dt2.Rows[0][0].ToString().Trim() != "")
                                {


                                    qtyremaining = qtyremaining + qty;

                                    oForm.Freeze(true);
                                    try
                                    {
                                        oMatrix.Columns.Item("2").Cells.Item(j).Specific.Value = i;
                                    }
                                    catch (Exception)
                                    {
                                        BubbleEvent = true;
                                        oForm.Freeze(false);
                                        return;
                                    }
                                    oMatrix.Columns.Item("5").Cells.Item(j).Specific.Value = qty;
                                    oMatrix.Columns.Item("10").Cells.Item(j).Click();
                                    oForm.Freeze(false);
                                    j++;
                                    UpdateBatchID++;


                                }
                                else
                                {
                                    SboApp.MessageBox("Batch '" + i.ToString() + "' Already exist in system.", 1);
                                }
                            }
                            //oMatrix.Columns.Item("2").Editable = false;
                            //oMatrix.Columns.Item("5").Editable = false;
                            //oMatrix.Columns.Item("7").Editable = false;
                            //oMatrix.Columns.Item("8").Editable = false;
                            oForm.Freeze(false);
                            //dt.Rows.Add(DocEntry, Identifier);
                            UpdateBatchID = 0;
                        }
                    }
                    else
                    {
                        SboApp.StatusBar.SetText("Please LotFrom and LotTo on the GRPO line", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                        BubbleEvent = false;
                        return;
                    }

                    Counter++;
                    BubbleEvent = false;
                }
                Sql = "select EnableFlag from B1S_Config";
                System.Data.DataTable dt12 = GetDataTable(Sql);
                if (dt12.Rows[0][0].ToString().Trim() == "Y")
                {
                    try
                    {
                        oMatrix.Columns.Item("2").Editable = false;
                        oMatrix.Columns.Item("5").Editable = false;
                        oMatrix.Columns.Item("7").Editable = false;
                        oMatrix.Columns.Item("8").Editable = false;
                    }
                    catch (Exception)
                    {
                    }
                }
                Counter++;
            }
            if (batchExistFlag == false)
            {
                SboApp.StatusBar.SetText("Automatic batch creation process completed successfully.", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
            }
            BubbleEvent = false;
            oForm.Freeze(false);
        }

        public void ComponantProcess(out bool BubbleEvent)
        {
            BubbleEvent = true;
            SAPbouiCOM.Form oForm = SboApp.Forms.ActiveForm;
            SAPbouiCOM.Matrix oMatrix = oForm.Items.Item("3").Specific;
            SAPbouiCOM.Matrix oMatrixUp = oForm.Items.Item("35").Specific;
            oForm = SboApp.Forms.ActiveForm;
            oForm.Freeze(true);
            try
            {
                string vendor = "";
                string totalneeded = "";
                string totalcreated = "";
                double needed;
                double created;
                int Counter = 1;
                //int DocEntry = 0;
                MultiLine = false;
                string Identifier = "";
                System.Data.DataTable dtoRs;
                System.Data.DataTable dtoRsVendor;
                System.Data.DataTable dtoRsChecKDI;

                Sql = "select Vendor, NumAtCard, LineID, ItemCode, Quantity, NumPerMsr, LotFrom, LotTo from b1s_temp_" + oCompany.UserName.ToString() + " where ItemCode IN (select ItemCode from OITM where ManBtchNum = 'Y' )";
                dtoRs = GetDataTable(Sql);

                if (dt == null)
                {
                    dt = new System.Data.DataTable();
                    dt.Columns.Add("Doc", typeof(int));
                    dt.Columns.Add("Iden", typeof(string));
                }
                SboApp.StatusBar.SetText("Automatic batch creation process started. Please wait...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);


                for (int ii = 0; ii < dtoRs.Rows.Count; ii++)
                {
                    if (dtoRs.Rows[ii][6].ToString().Trim() != "" && dtoRs.Rows[ii][7].ToString().Trim() != "")
                    {


                        try
                        {
                            oMatrixUp.Columns.Item("0").Cells.Item(Counter).Click(SAPbouiCOM.BoCellClickType.ct_Regular);
                        }
                        catch (Exception)
                        {
                        }

                        totalneeded = Convert.ToString(oMatrixUp.Columns.Item("39").Cells.Item(Counter).Specific.Value);
                        totalcreated = Convert.ToString(oMatrixUp.Columns.Item("38").Cells.Item(Counter).Specific.Value);

                        string CommisionNo = "";


                        if (totalneeded == "")
                            needed = 0;
                        else
                            needed = Convert.ToDouble(totalneeded);

                        if (totalcreated == "")
                            created = 0;
                        else
                            created = Convert.ToDouble(totalcreated);

                        vendor = dtoRs.Rows[ii][0].ToString();
                        Sql = "select top 1 Vendor, Identifier, TaiwanCode from B1S_VendorConfig where Vendor ='" + dtoRs.Rows[ii][0].ToString() + "'";
                        dtoRsVendor = GetDataTable(Sql);

                        //if (dtoRsVendor.Rows.Count > 0)
                        //{

                        Sql = "select top 1 U_ComponentCode, U_remark from [@KDVEHICLEMAPPING] where U_ComponentCode = '" + dtoRs.Rows[ii][3].ToString() + "'";
                        dtoRsChecKDI = GetDataTable(Sql);













                        if (dtoRsChecKDI.Rows.Count > 0)
                        {
                            double TotalBatches = Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString()) / Convert.ToDouble(dtoRs.Rows[ii][5].ToString());
                            int TotalBatch = (int)Math.Ceiling(TotalBatches);
                            double TotalQty = Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString());
                            double qty = (Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString())) / TotalBatch;
                            double qtyremaining = 0;

                            int BatchID, UpdateBatchID = 0;



                            BatchID = Convert.ToInt32(dtoRs.Rows[ii][6]);
                            BatchID = BatchID - 1;
                            int i = 0;
                            int j;
                            if (created > 0)
                            {
                                string asd;
                                j = oMatrix.RowCount;
                                string getmaxBatch;
                                if (oMatrix.Columns.Item("2").Cells.Item(oMatrix.RowCount).Specific.Value == "")
                                {
                                    getmaxBatch = oMatrix.Columns.Item("2").Cells.Item(oMatrix.RowCount - 1).Specific.Value;
                                }
                                else
                                {
                                    getmaxBatch = oMatrix.Columns.Item("2").Cells.Item(oMatrix.RowCount).Specific.Value;
                                }
                                BatchID = Convert.ToInt32(getmaxBatch);
                                UpdateBatchID = BatchID;
                            }
                            else
                            {
                                j = 1;



                            }
                            oForm.Freeze(true);
                            oMatrix.Columns.Item("2").Editable = true;
                            oMatrix.Columns.Item("5").Editable = true;
                            oMatrix.Columns.Item("7").Editable = true;
                            oMatrix.Columns.Item("8").Editable = true;
                            for (i = BatchID; i < BatchID + TotalBatches; i++)
                            {
                                if (created < needed)
                                {
                                    //if (TotalQty - qtyremaining > 0)
                                    //{
                                    qtyremaining = qtyremaining + qty;

                                    oForm.Freeze(true);
                                    oMatrix.Columns.Item("2").Cells.Item(j).Specific.Value = i + 1;
                                    oMatrix.Columns.Item("5").Cells.Item(j).Specific.Value = qty;

                                    oMatrix.Columns.Item("11").Cells.Item(j).Click();
                                    oForm.Freeze(false);

                                    j++;
                                    UpdateBatchID++;
                                    created = created + qty;
                                    //}

                                }
                                else
                                {

                                }
                            }

                            //oMatrix.Columns.Item("2").Editable = false;
                            //oMatrix.Columns.Item("5").Editable = false;
                            //oMatrix.Columns.Item("7").Editable = false;
                            //oMatrix.Columns.Item("8").Editable = false;
                            oForm.Freeze(false);
                            if (!htBatch.ContainsKey(dtoRs.Rows[ii][3].ToString()))
                            {
                                htBatch.Add(dtoRs.Rows[ii][3].ToString(), i);
                            }
                            else
                            {
                                htBatch[dtoRs.Rows[ii][3].ToString()] = UpdateBatchID;
                            }
                            //dt.Rows.Add(DocEntry, Identifier);
                            UpdateBatchID = 0;
                        }
                        Counter++;
                    }
                    else
                    {
                        SboApp.StatusBar.SetText("Please LotFrom and LotTo on the GRPO line", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                        BubbleEvent = false;
                        return;
                    }
                }

                //oForm.Freeze(false);

                SboApp.StatusBar.SetText("Automatic batch creation process completed successfully.", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success);



                BubbleEvent = false;
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Item Click Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
            finally
            {
                oForm.Freeze(false);
            }
        }

        public void KDProcess(out bool BubbleEvent)
        {
            BubbleEvent = true;
            SAPbouiCOM.Form oForm = SboApp.Forms.ActiveForm;
            SAPbouiCOM.Matrix oMatrix = oForm.Items.Item("3").Specific;
            SAPbouiCOM.Matrix oMatrixUp = oForm.Items.Item("35").Specific;
            oForm = SboApp.Forms.ActiveForm;
            oForm.Freeze(true);
            try
            {
                string vendor = "";
                string totalneeded = "";
                string totalcreated = "";
                double needed;
                double created;
                int Counter = 1;
                //int DocEntry = 0;
                MultiLine = false;
                string Identifier = "";
                System.Data.DataTable dtoRs;
                System.Data.DataTable dtoRsVendor;
                System.Data.DataTable dtoRsChecKDI;

                Sql = "select Vendor, NumAtCard, LineID, ItemCode, Quantity, NumPerMsr, LotFrom, LotTo from b1s_temp_" + oCompany.UserName.ToString() + " where ItemCode IN (select ItemCode from OITM where ManBtchNum = 'Y' )";
                dtoRs = GetDataTable(Sql);

                if (dt == null)
                {
                    dt = new System.Data.DataTable();
                    dt.Columns.Add("Doc", typeof(int));
                    dt.Columns.Add("Iden", typeof(string));
                }
                SboApp.StatusBar.SetText("Automatic batch creation process started. Please wait...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);


                for (int ii = 0; ii < dtoRs.Rows.Count; ii++)
                {
                    if (dtoRs.Rows[ii][6].ToString().Trim() != "" && dtoRs.Rows[ii][7].ToString().Trim() != "")
                    {
                        try
                        {
                            oMatrixUp.Columns.Item("0").Cells.Item(Counter).Click(SAPbouiCOM.BoCellClickType.ct_Regular);
                        }
                        catch (Exception)
                        {
                        }


                        totalneeded = Convert.ToString(oMatrixUp.Columns.Item("39").Cells.Item(Counter).Specific.Value);
                        totalcreated = Convert.ToString(oMatrixUp.Columns.Item("38").Cells.Item(Counter).Specific.Value);

                        string CommisionNo = "";


                        if (totalneeded == "")
                            needed = 0;
                        else
                            needed = Convert.ToDouble(totalneeded);

                        if (totalcreated == "")
                            created = 0;
                        else
                            created = Convert.ToDouble(totalcreated);

                        vendor = dtoRs.Rows[ii][0].ToString();
                        Sql = "select top 1 Vendor, Identifier, TaiwanCode from B1S_VendorConfig where Vendor ='" + dtoRs.Rows[ii][0].ToString() + "'";
                        dtoRsVendor = GetDataTable(Sql);

                        //if (dtoRsVendor.Rows.Count > 0)
                        //{

                        Sql = "select top 1 U_KDitemCode, U_remark from [@KDVEHICLEMAPPING] where U_KDitemCode = '" + dtoRs.Rows[ii][3].ToString() + "'";
                        dtoRsChecKDI = GetDataTable(Sql);








                        if (dtoRsChecKDI.Rows.Count > 0)
                        {
                            double TotalBatches = Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString()) / Convert.ToDouble(dtoRs.Rows[ii][5].ToString());
                            int TotalBatch = (int)Math.Ceiling(TotalBatches);
                            double TotalQty = Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString());
                            double qty = (Convert.ToDouble(dtoRs.Rows[ii][4].ToString()) * Convert.ToDouble(dtoRs.Rows[ii][5].ToString())) / TotalBatch;
                            double qtyremaining = 0;
                            string yr = Convert.ToString(DateTime.Now.Year % 100);
                            string TaiwanCode = "886";
                            Identifier = dtoRsChecKDI.Rows[0][1].ToString();
                            Identifier = Identifier.Trim().Substring(0, 1);









                            DocEntry = GetMaxCode(Identifier, DateTime.Now.Year.ToString());






                            if (DocEntry == 0)
                                DocEntry = 1;
































                            //if (dtKDDocEntry.Contains(Identifier) != true)
                            //{

                            //    DocEntry = GetMaxCode(Identifier, DateTime.Now.Year.ToString());
                            //}
                            //else
                            //{



                            //    DocEntry = Convert.ToInt32(dtKDDocEntry[Identifier]);
                            //}
                            int BatchID, UpdateBatchID = 0;










































































































                            BatchID = Convert.ToInt32(dtoRs.Rows[ii][6]);
                            BatchID = BatchID - 1;
                            int i = 0;
                            int j;
                            if (created > 0)
                            {
                                string asd;
                                j = oMatrix.RowCount;
                                string getmaxBatch;
                                if (oMatrix.Columns.Item("2").Cells.Item(oMatrix.RowCount).Specific.Value == "")
                                {
                                    getmaxBatch = oMatrix.Columns.Item("2").Cells.Item(oMatrix.RowCount - 1).Specific.Value;
                                }
                                else
                                {
                                    getmaxBatch = oMatrix.Columns.Item("2").Cells.Item(oMatrix.RowCount).Specific.Value;
                                }
                                BatchID = Convert.ToInt32(getmaxBatch);
                                UpdateBatchID = BatchID;
                            }
                            else
                            {
                                j = 1;
                            }
                            oForm.Freeze(true);
                            oMatrix.Columns.Item("2").Editable = true;
                            oMatrix.Columns.Item("5").Editable = true;
                            oMatrix.Columns.Item("7").Editable = true;
                            oMatrix.Columns.Item("8").Editable = true;
                            for (i = BatchID; i < BatchID + TotalBatches; i++)
                            {
                                if (created < needed)
                                {
                                    //if (TotalQty - qtyremaining > 0)
                                    //{
                                    qtyremaining = qtyremaining + qty;
                                    string str1 = DocEntry.ToString().PadLeft(5, '0').ToString();
                                    string CommissionFrom = yr + TaiwanCode + Identifier + str1;
                                    DocEntry = DocEntry + Convert.ToInt32(qty) - 1;
                                    string str2 = DocEntry.ToString().PadLeft(5, '0').ToString();
                                    string CommissionTo = yr + TaiwanCode + Identifier + str2;
                                    oForm.Freeze(true);
                                    oMatrix.Columns.Item("2").Cells.Item(j).Specific.Value = i + 1;
                                    oMatrix.Columns.Item("5").Cells.Item(j).Specific.Value = qty;
                                    oMatrix.Columns.Item("7").Cells.Item(j).Specific.Value = CommissionFrom;
                                    oMatrix.Columns.Item("8").Cells.Item(j).Specific.Value = CommissionTo;
                                    oMatrix.Columns.Item("10").Cells.Item(j).Click();
                                    oForm.Freeze(false);
                                    DocEntry++;
                                    j++;
                                    UpdateBatchID++;
                                    created = created + qty;
                                    //}

                                }
                                else
                                {
                                    DocEntry = DocEntry + Convert.ToInt32(qty) - 1;

                                }
                            }

                            //oMatrix.Columns.Item("2").Editable = false;
                            //oMatrix.Columns.Item("5").Editable = false;
                            //oMatrix.Columns.Item("7").Editable = false;
                            //oMatrix.Columns.Item("8").Editable = false;
                            oForm.Freeze(false);
                            if (!htBatch.ContainsKey(dtoRs.Rows[ii][3].ToString()))
                            {
                                htBatch.Add(dtoRs.Rows[ii][3].ToString(), i);
                            }
                            else
                            {
                                htBatch[dtoRs.Rows[ii][3].ToString()] = UpdateBatchID;
                            }
                            //dt.Rows.Add(DocEntry, Identifier);
                            UpdateBatchID = 0;
                        }
                        //}
                        //oRs.MoveNext();
                        Counter++;









                        //MultiLine = true;
                        //if (!dtKDDocEntry.Contains(Identifier) == true)
                        //{
                        //    dtKDDocEntry.Add(Identifier, DocEntry);
                        //}
                        //else
                        //{
                        //    dtKDDocEntry[Identifier] = DocEntry;
                        //}
                        if (DocEntry != 0)
                        {

                            Sql = "update [@B1S_COMMCOUNTER] SET U_COUNTER = " + DocEntry + " where U_Type = '" + Identifier + "'  and U_Year ='" + DateTime.Now.Year.ToString() + "' ";
                            ADONonExeSql(Sql); //dt.Rows.Add(DocEntryCBU, IdentifierFOrCBU);

                        }
                    }
                    else
                    {
                        SboApp.StatusBar.SetText("Please fill LotFrom and LotTo on the GRPO line", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error);
                        BubbleEvent = false;
                        return;




                    }
                }

                //oForm.Freeze(false);

                SboApp.StatusBar.SetText("Automatic batch creation process completed successfully.", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success);



                BubbleEvent = false;

            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Item Click Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
            finally
            {
                oForm.Freeze(false);
            }
        }
        //public void ItemClick_old(SAPbouiCOM.ItemEvent pVal, bool BubbleEvent)
        //{
        //    try
        //    {
        //        if (FormTypeFlag == "143")
        //        {
        //            selectedRow = 1;
        //            SAPbouiCOM.Form oForm = SboApp.Forms.ActiveForm;
        //            SAPbouiCOM.Matrix oMatrix = oForm.Items.Item("3").Specific;
        //            SAPbouiCOM.Matrix oMatrixUp = oForm.Items.Item("35").Specific;



        //            if (pVal.ItemUID == "1" && pVal.BeforeAction == false)
        //            {

        //                //try
        //                //{

        //                //    for (int i = 0; i < dt.Rows.Count; i++)
        //                //    {
        //                //        SAPbobsCOM.Recordset oRsDetails = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
        //                //        oRsDetails.DoQuery("select Code from [@B1S_COMMCOUNTER] where U_TYPE = '" + dt.Rows[i]["Iden"].ToString() + "' and U_Year = '" + DateTime.Now.Year.ToString() + "'");
        //                //        if (oRsDetails.RecordCount > 0)
        //                //        {
        //                //            oRsDetails.DoQuery("update [@B1S_COMMCOUNTER] SET U_COUNTER = " + dt.Rows[i]["Doc"].ToString() + " where code = '" + oRsDetails.Fields.Item(0).Value + "'");
        //                //        }
        //                //        else
        //                //        {
        //                //            string code = (GetMaxID() + 1).ToString();
        //                //            string q = "insert into  [@B1S_COMMCOUNTER] (Code, Name, U_TYPE, U_Year, U_COUNTER ) values "
        //                //            + " ('" + code + "','" + code + "', '" + dt.Rows[i]["Iden"].ToString() + "',"
        //                //            + "'" + DateTime.Now.Year.ToString() + "', " + dt.Rows[i]["Doc"].ToString() + ") ";
        //                //            oRsDetails.DoQuery(q);
        //                //        }
        //                //    }
        //                //    dt.Clear();
        //                //}
        //                //catch (Exception ex)
        //                //{
        //                //    SboApp.StatusBar.SetText("Item Click Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
        //                //}
        //            }

        //            if (pVal.ItemUID == "36" && pVal.BeforeAction == true)
        //            {


        //                Hashtable ht = new Hashtable();                        
        //                oForm = SboApp.Forms.GetForm("143", -1);
        //                oForm.Freeze(true);
        //                try
        //                {
        //                    string vendor = "";
        //                    int Counter = 1;
        //                    int DocEntry = 0;
        //                    MultiLine = false;
        //                    string Identifier = "";

        //                    //System.Data.DataTable dt = GetDataTable(sql);

        //                    SAPbobsCOM.Recordset oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
        //                    SAPbobsCOM.Recordset oRsVendorConfig = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
        //                    SAPbobsCOM.Recordset oRsCheckKDI = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
        //                    oRs.DoQuery("select * from b1s_Temp_" + oCompany.UserName.ToString() + " where ItemCode IN (select ItemCode from OITM where ManBtchNum = 'Y' )");

        //                    dt = new System.Data.DataTable();
        //                    dt.Columns.Add("Doc", typeof(int));
        //                    dt.Columns.Add("Iden", typeof(string));

        //                    while (oRs.EoF != true)
        //                    {
        //                        oMatrixUp.Columns.Item("0").Cells.Item(Counter).Click(SAPbouiCOM.BoCellClickType.ct_Regular);

        //                        vendor = oRs.Fields.Item(0).Value;
        //                        oRsVendorConfig.DoQuery("select Vendor, Identifier, TaiwanCode from B1S_VendorConfig where vendor = '" + vendor.Trim() + "'");
        //                        if (oRsVendorConfig.RecordCount > 0)
        //                        {
        //                            oRsCheckKDI.DoQuery("select U_KDitemCode from [@B1S_KDVACHMAPPING] where U_KDitemCode = '" + oRs.Fields.Item(3).Value + "'");
        //                            if (oRsCheckKDI.RecordCount > 0)
        //                            {
        //                                double TotalBatches = oRs.Fields.Item(4).Value / oRs.Fields.Item(5).Value;

        //                                //int TotalBatch = Convert.ToInt32(TotalBatches);
        //                                int TotalBatch = (int)Math.Ceiling(TotalBatches);
        //                                //int abc =  (int)Math.Ceiling(TotalBatches);

        //                                double TotalQty = oRs.Fields.Item(4).Value * oRs.Fields.Item(5).Value;
        //                                double qty = (oRs.Fields.Item(4).Value * oRs.Fields.Item(5).Value) / TotalBatch;

        //                                double qtyremaining = 0;


        //                                string yr = Convert.ToString(DateTime.Now.Year % 100);
        //                                string TaiwanCode = oRsVendorConfig.Fields.Item(2).Value;

        //                                Identifier = oRsVendorConfig.Fields.Item(1).Value;
        //                                //if(vendor == "MFC")
        //                                //    Identifier = "M";
        //                                //else
        //                                //    Identifier = "C";

        //                                if (MultiLine == false)
        //                                {
        //                                    DocEntry = GetMaxCode(Identifier, DateTime.Now.Year.ToString());
        //                                    if (DocEntry == 0)
        //                                        DocEntry = 1;
        //                                }
        //                                int j = 1;

        //                                //SAPbouiCOM.CellPosition oPos = oMatrixUp.GetCellFocus();

        //                                //string ItemCode = oMatrixUp.Columns.Item("5").Cells.Item(oPos.rowIndex).Specific.Value;
        //                                //int BatchID = GetMaxBatchID(ItemCode);
        //                                int BatchID;

        //                                if (!ht.ContainsKey(oRs.Fields.Item(3).Value))
        //                                {
        //                                     BatchID = GetMaxBatchID(oRs.Fields.Item(3).Value);
        //                                    //ht.Add(oRs.Fields.Item(3).Value, BatchID);
        //                                }
        //                                else
        //                                {
        //                                    BatchID = Convert.ToInt32(ht[oRs.Fields.Item(3).Value].ToString());
        //                                }
        //                                //int BatchID = GetMaxBatchID(oRs.Fields.Item(3).Value);


        //                                SboApp.StatusBar.SetText("Automatic batch creation process started. Please wait...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
        //                                oForm.Freeze(true);
        //                                int i = 0;
        //                                for ( i = BatchID; i < BatchID + TotalBatch; i++)
        //                                {


        //                                    if (TotalQty - qtyremaining > 0)
        //                                    {
        //                                        qtyremaining = qtyremaining + qty;

        //                                        string str1 = DocEntry.ToString().PadLeft(6, '0').ToString();
        //                                        string CommissionFrom = yr + TaiwanCode + Identifier + str1;
        //                                        DocEntry = DocEntry + Convert.ToInt32(qty) - 1;
        //                                        string str2 = DocEntry.ToString().PadLeft(6, '0').ToString();
        //                                        string CommissionTo = yr + TaiwanCode + Identifier + str2;
        //                                        oForm.Freeze(true);
        //                                        oMatrix.Columns.Item("2").Cells.Item(j).Specific.Value = i + 1;
        //                                        if (TotalQty - qtyremaining < qty)
        //                                        {
        //                                            //qty = TotalQty - qtyremaining;
        //                                        }
        //                                        else
        //                                        {
        //                                            oMatrix.Columns.Item("5").Cells.Item(j).Specific.Value = qty;
        //                                        }
        //                                        oMatrix.Columns.Item("7").Cells.Item(j).Specific.Value = CommissionFrom;
        //                                        oMatrix.Columns.Item("8").Cells.Item(j).Specific.Value = CommissionTo;
        //                                        oForm.Freeze(false);
        //                                        DocEntry++;
        //                                        j++;

        //                                    }

        //                                }
        //                                oForm.Freeze(false);
        //                                if (!ht.ContainsKey(oRs.Fields.Item(3).Value))
        //                                {
        //                                    ht.Add(oRs.Fields.Item(3).Value, i);
        //                                }
        //                                else
        //                                {
        //                                    ht[oRs.Fields.Item(3).Value] = i;
        //                                }
        //                                //dt.Rows.Add(DocEntry, Identifier);

        //                            }
        //                        }
        //                        oRs.MoveNext();
        //                        Counter++;

        //                        MultiLine = true;

        //                    }
        //                    dt.Rows.Add(DocEntry, Identifier);
        //                    SboApp.StatusBar.SetText("Automatic batch creation process completed successfully.", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success);

        //                    BubbleEvent = false;
        //                }
        //                catch (Exception ex)
        //                {
        //                    SboApp.StatusBar.SetText("Item Click Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
        //                }
        //                finally
        //                {
        //                    oForm.Freeze(false);
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SboApp.StatusBar.SetText("Item Click Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
        //    }
        //    finally
        //    { 
        //    }
        //}

        public int GetMaxBatchID(string ItemCode)
        {
            try
            {
                SAPbobsCOM.Recordset oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                string Query = "select isnull(max(cast(DistNumber as int)),0) from OBTN where ItemCode ='" + ItemCode + "' and DistNumber <> 'B1' ";
                oRs.DoQuery(Query);
                return oRs.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("Item Click Event. " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                return 0;
            }

        }
        public int GetMaxCode(string Typ, string Year)
        {
            try
            {
                SAPbobsCOM.Recordset oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                string Query = "select isnull(U_COUNTER, 1)  as code "
                  + " from [@B1S_COMMCOUNTER] where U_TYPE = '" + Typ + "'   and U_Year ='" + Year + "' ";

                oRs.DoQuery(Query);

                return oRs.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("GetMaxCode Method.  " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                return 0;
            }

        }
        public int GetMaxID()
        {
            try
            {
                SAPbobsCOM.Recordset oRs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                string Query = "select max(cast(code as int)) from [@B1S_COMMCOUNTER]";
                oRs.DoQuery(Query);
                return oRs.Fields.Item(0).Value;
            }
            catch (Exception ex)
            {
                SboApp.StatusBar.SetText("GetMaxId Method.  " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                return 0;
            }

        }

    }
}
