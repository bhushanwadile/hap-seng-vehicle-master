      if @object_type = '13' and @transaction_type in('A', 'U')
      begin
      declare @ItemCode  nvarchar(50)
      declare @serial  nvarchar(50)
	  --end
				if exists (select t0.linenum
					from INV1 t0 inner join OINV t1 on t0.DocEntry = t1.DocEntry
					inner join SRI1 t2 on t2.BaseEntry = t0.DocEntry and t2.BaseLinNum = t0.LineNum and t2.BaseType = 13 and Direction = 1
					inner join OSRN t5	on t5.ItemCode = t2.ItemCode and t5.SysNumber = t2.SysSerial 
					inner join B1SVMHEADER a on t5.DistNumber = a.B1S_CommNo and a.B1S_VehModelVar = (select  isnull(U_B1S_VehModelVar, '') from OITM where Itemcode = t0.Itemcode) 
					inner join b1svmline b on a.vmentry = b.vmentry 
					 where t1.DocEntry = @list_of_cols_val_tab_del and b.objtype  IN ('999004') and transtype in ( 'Stop') 
					 and isnull(b.creationdate, '') = '' and a.B1S_CommNo = t5.DistNumber
					 and (select U_B1S_VehType from OITM where ItemCode = t0.ItemCode) IN ('BU', 'FG')
					 and t1.U_B1S_VMType = 'CKD') 
					 begin

					 set @error = '200'
					 set @error_message = 'PDI Stop not exeuted for the line : '+ STUFF((select distinct ',' + cast(t0.linenum  as nvarchar(10))
						from INV1 t0 inner join OINV t1 on t0.DocEntry = t1.DocEntry
					inner join SRI1 t2 on t2.BaseEntry = t0.DocEntry and t2.BaseLinNum = t0.LineNum and t2.BaseType = 13 and Direction = 1
					inner join OSRN t5	on t5.ItemCode = t2.ItemCode and t5.SysNumber = t2.SysSerial 
					inner join B1SVMHEADER a on t5.DistNumber = a.B1S_CommNo and a.B1S_VehModelVar = (select  isnull(U_B1S_VehModelVar, '') from OITM where Itemcode = t0.Itemcode) 
					inner join b1svmline b on a.vmentry = b.vmentry 
					 where t1.DocEntry = @list_of_cols_val_tab_del and b.objtype  IN ('999004') and transtype in ( 'Stop') 
					 and isnull(b.creationdate, '') = '' and a.B1S_CommNo = t5.DistNumber
					 and (select U_B1S_VehType from OITM where ItemCode = t0.ItemCode) IN ('BU', 'FG')
					 and t1.U_B1S_VMType = 'CKD' FOR XML PATH ('')), 1, 1, '')

					 end
					 end